import sys
import math 
import glob
import numpy as np
import cv2
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

IMG_FEATURES = 100

orb = cv2.ORB_create(nfeatures=IMG_FEATURES)

var1 = 1
var2 = 1
var3 = 13
kernel = np.ones((3,3), np.uint8) 



images = [cv2.imread(file) for file in sorted(glob.glob(sys.argv[2]  + "*.png"))]  

blank  = np.zeros((images[0].shape[0],images[0].shape[1],3), np.uint8)

print("imgaes number : ",len(images))

in_train_mat = [np.zeros((IMG_FEATURES,2), np.float32)] * len(images) 
in_train = [np.zeros((IMG_FEATURES*2,1), np.float32)] * len(images) 

for cnt,pd in enumerate(images):
    blur = cv2.medianBlur(images[cnt],13)
    img_h = cv2.Sobel(blur,cv2.CV_8U,int(var1),int(var2),ksize=int(var3))
    img_erosion = cv2.erode(img_h, kernel, iterations=2) 
    keypoints_orb, descriptors = orb.detectAndCompute(img_erosion, None)
    for cntin,pd in enumerate(keypoints_orb):
        in_train_mat[cnt][cntin,:] = pd.pt
        # print("points : ", pd.pt)
    in_train_mat[cnt] = in_train_mat[cnt][in_train_mat[cnt][:,0].argsort()]
    in_train[cnt]     = np.ravel(in_train_mat[cnt])


class NN_vis(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim):
        super(NN_vis, self).__init__()
        self.fc1 = nn.Linear(input_dim, hidden_dim) 
        self.sigmoid1 = nn.ReLU()
        self.fc2 = nn.Linear(hidden_dim, hidden_dim)  
        self.sigmoid2 = nn.ReLU()
        self.fc3 = nn.Linear(hidden_dim, hidden_dim)  
        self.sigmoid3 = nn.ReLU()
        self.fc4 = nn.Linear(hidden_dim, output_dim)  
    def forward(self, x):
        out = self.fc1(x)
        out = self.sigmoid1(out)
        out = self.fc2(out)
        out = self.sigmoid2(out)
        out = self.fc3(out)
        out = self.sigmoid3(out)
        out = self.fc4(out)
        # return F.softmax(out, dim=0)
        return out

model = NN_vis(200,2000,1)
model.load_state_dict(torch.load(str(sys.argv[1])))
model.eval()
model.to('cuda')

for elem in in_train:
    inp_c = torch.from_numpy(elem).float().to('cuda')
    pred = model(inp_c).cpu()
    torch.cuda.synchronize()
    print(pred.data.numpy())
