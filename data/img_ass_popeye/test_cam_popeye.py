#!/usr/bin/env python
import sys
import math 
import glob
import numpy as np
import cv2
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable


if sys.argv[1] == "merge":
    images = [cv2.imread(file) for file in sorted(glob.glob(sys.argv[2]  + "*.png"))]
    print(images)
    for i in range(len(images)/2):
        vis = np.concatenate((images[i * 2],images[(i * 2) + 1]), axis=1)
        # cv2.imshow('image_' + str(i),vis)
        cv2.imwrite(sys.argv[3] + str(i) + ".png",vis)
    sys.exit()

IMG_FEATURES = 100

orb = cv2.ORB_create(nfeatures=IMG_FEATURES)

var1 = 1
var2 = 1
var3 = 13
kernel = np.ones((3,3), np.uint8) 


images = [cv2.imread(file) for file in sorted(glob.glob(sys.argv[2]  + "*.png"))]  

in_train_mat = [np.zeros((IMG_FEATURES,2), np.float32)] * len(images) 
in_train = [np.zeros((IMG_FEATURES*2,1), np.float32)] * len(images)

blank  = np.zeros((images[0].shape[0],images[0].shape[1],3), np.uint8)

print("images number : ",len(images))

for cnt,pd in enumerate(images):
    blur = cv2.medianBlur(images[cnt],13)
    img_h = cv2.Sobel(blur,cv2.CV_8U,int(var1),int(var2),ksize=int(var3))
    img_erosion = cv2.erode(img_h, kernel, iterations=2) 
    keypoints_orb, descriptors = orb.detectAndCompute(img_erosion, None)
    for cntin,pd in enumerate(keypoints_orb):
        in_train_mat[cnt][cntin,0] = pd.pt[0]/1280.0
        in_train_mat[cnt][cntin,1] = pd.pt[1]/480.0
        # print("points : ", pd.pt)
    in_train_mat[cnt] = in_train_mat[cnt][in_train_mat[cnt][:,0].argsort()]
    in_train[cnt]     = np.ravel(in_train_mat[cnt])
#     blank = cv2.drawKeypoints(blank, keypoints_orb,None,color = (cnt * 40,255 - (cnt * 40),255 - (cnt * 40)))
#     cv2.imshow('image_' + str(cnt),img_erosion)
# cv2.imshow('image_' + str(cnt + 1),blank)
# k = cv2.waitKey(200000) & 0xFF
# print ("input:" ,in_train)
# var1, var2, var3 = raw_input("Enter 3 numbers here: ").split()
 

class NN_vis(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim):
        super(NN_vis, self).__init__()
        self.fc1 = nn.Linear(input_dim, hidden_dim) 
        self.sigmoid1 = nn.ReLU()
        self.fc2 = nn.Linear(hidden_dim, hidden_dim)  
        self.sigmoid2 = nn.ReLU()
        self.fc3 = nn.Linear(hidden_dim, hidden_dim)  
        self.sigmoid3 = nn.ReLU()
        self.fc4 = nn.Linear(hidden_dim, output_dim)  
    def forward(self, x):
        out = self.fc1(x)
        out = self.sigmoid1(out)
        out = self.fc2(out)
        out = self.sigmoid2(out)
        out = self.fc3(out)
        out = self.sigmoid3(out)
        out = self.fc4(out)
        # return F.softmax(out, dim=0)
        return out   
    
if sys.argv[1] == "train":
    model = NN_vis(200,1000,1)
    model.to('cuda')


    # criterion = nn.CrossEntropyLoss()
    criterion = nn.MSELoss(reduction='sum')
    learning_rate = 0.0000025
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)  

    data_y = np.ones((len(images)*2,1),dtype=np.float32)
    label = Variable(torch.from_numpy(data_y)).cuda()

    for epoch in range(5000):
        for i, inp in enumerate(in_train):
            optimizer.zero_grad()
            inp_c = torch.from_numpy(inp).float().to('cuda')
            outputs = model(inp_c)
            loss = criterion(outputs, label[i])
            # print(loss)
            loss.backward()
            optimizer.step()
        print('Iteration: {}. Loss: {}.'.format(iter, loss.item()))
                
    torch.save(model.state_dict(),"NN_file")
    
if sys.argv[1] == "test":
    model = NN_vis(200,1000,1)
    model.load_state_dict(torch.load(str(sys.argv[3])))
    model.eval()

    for elem in in_train:
        inp_c = torch.from_numpy(elem).float()
        pred = model(inp_c)
        print(pred.data.numpy())