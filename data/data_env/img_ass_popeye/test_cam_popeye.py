#!/usr/bin/env python
import sys
import math 
import glob
import numpy as np
import cv2


def auto_canny(image, sigma=0.25):
    v = np.median(image)
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
    return edged


# if sys.argv[1] == "merge":
#     images = [cv2.imread(file) for file in sorted(glob.glob(sys.argv[2]  + "*.png"))]
#     print(images)
#     for i in range(len(images)/2):
#         vis = np.concatenate((images[i * 2],images[(i * 2) + 1]), axis=1)
#         # cv2.imshow('image_' + str(i),vis)
#         cv2.imwrite("train_merged/" + str(i) + ".png",vis)
#     sys.exit()
    

orb = cv2.ORB_create(nfeatures=200)

var1 = 1
var2 = 1
var3 = 13
kernel = np.ones((3,3), np.uint8) 


images = [cv2.imread(file) for file in sorted(glob.glob(sys.argv[1]  + "*.png"))]  

blank_image = np.zeros((images[0].shape[0],images[0].shape[1],3), np.uint8)

while(1):
    for cnt,pd in enumerate(images[:4]):
        # blur = cv2.GaussianBlur(images[cnt],(5,5), 1) 
        blur = cv2.medianBlur(images[cnt],5)
        img_h = cv2.Sobel(blur,cv2.CV_8U,int(var1),int(var2),ksize=int(var3))
        img_erosion = cv2.erode(img_h, kernel, iterations=2) 
        keypoints_orb, descriptors = orb.detectAndCompute(img_erosion, None)
        blank_image = cv2.drawKeypoints(blank_image, keypoints_orb,None,color = (cnt * 10,255 - (cnt * 10),0))
        cv2.imshow('image_' + str(cnt),img_erosion)
    k = cv2.waitKey(20) & 0xFF
    # var1, var2, var3 = raw_input("Enter 3 numbers here: ").split()
    if k == 27:
        break
