<launch>  
  <arg name="debug"             default="false" />
  <arg name="launch_prefix"     value="gdb -ex run --args"    if="$(arg debug)" />
  <arg name="launch_prefix"     value=""                      unless="$(arg debug)" />

  <arg name="fake_execution"    default="true" />
  <arg name="clamper"           default="only_fix_element" />
  <arg name="static_scene"      default="false" />
  <arg name="rviz"              default="true" />
  <arg name="sim_sweepee"       default="true" />

  <arg name="db" default="false" />
  <arg name="db_path" default="$(find popeye_arm_moveit_config)/default_warehouse_mongo_db" />




<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- planning -->
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
  <include file="$(find popeye_arm_moveit_config)/launch/move_group.launch">
    <arg name="allow_trajectory_execution" value="true"/>
    <arg name="fake_execution" value="false"/>
    <arg name="info" value="true"/>
    <arg name="debug" value="$(arg debug)"/>
  </include>

  <!--include file="$(find popeye_arm_moveit_config)/launch/moveit_rviz.launch" if="$(arg rviz)" >
    <arg name="config" value="true"/>
    <arg name="debug" value="$(arg debug)"/>
  </include-->

  <include file="$(find popeye_arm_moveit_config)/launch/default_warehouse_db.launch" if="$(arg db)">
    <arg name="moveit_warehouse_database_path" value="$(arg db_path)"/>
  </include>

  <!-- node that load the objects inside the scene, and make easier the connection/attchment of the hatrack during for the planning -->
  <node  name="context_manager" pkg="popeye_assembly" type="context_manager_node" output="screen">
    <rosparam>
      hatbox_in_habox_cart_frame/xyz:         [-0.05         , 0.0,  0.770]
      hatbox_in_habox_cart_frame/rpy:         [-0.78539816339, 0.0, -1.57079632679]

      hatbox_approach_A_in_hatbox_frame/xyz:  [ 0.00, 0.030,  -0.090  ]
      hatbox_approach_A_in_hatbox_frame/rpy:  [ 0.12, 0.00 ,  0.000  ]

      hatbox_approach_B_in_hatbox_frame/xyz:  [ 0.000, 0.005, -0.050  ]
      hatbox_approach_B_in_hatbox_frame/rpy:  [ 0.100, 0.00,  0.00    ]

      hatbox_bracket_in_hatbox_frame/xyz:     [ 0.000, -0.240, 0.555 ]
      hatbox_bracket_in_hatbox_frame/rpy:     [ 0.175, -0.000, 0.000 ]

      hatbox_bracket_1_in_fuselage/xyz:       [ 4.150, -0.870, 1.950 ]
      hatbox_bracket_1_in_fuselage/rpy:       [ 0.175, -0.000, 3.1415]

      hatbox_approach_1_A_in_bracket_1/xyz:   [ 0.0000, -0.035, 0.050 ]
      hatbox_approach_1_A_in_bracket_1/rpy:   [-0.1756, -0.000, 0.000 ]

      hatbox_approach_1_B_in_bracket_1/xyz:   [ 0.0000, -0.000, 0.010 ]
      hatbox_approach_1_B_in_bracket_1/rpy:   [ 0.0000, -0.000, 0.000 ]

      hatbox_final_1_in_bracket_1/xyz:        [ 0.0000, -0.000, 0.010 ]
      hatbox_final_1_in_bracket_1/rpy:        [-0.1309, -0.000, 0.000 ]

      window_1_in_fuselage/xyz:               [ 4.150 , -2.000, 0.000 ]
      window_1_in_fuselage/rpy:               [ 0.0000, -0.000, 0.000 ]

      hatbox_post_grasping_z_displacement:  0.1
      group_name                         :  popeye_arm
      robot_description                  :  /robot_description
    </rosparam>
  </node>
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->






<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- execution fsm module -->
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<node launch-prefix="$(arg launch_prefix)"  name="eureca_popeye_fsm" pkg="popeye_assembly" type="assembly" output="screen" >
  <rosparam>
    group_name                         : popeye_arm
    robot_description                  : /robot_description
    motion_planner_action_ns           : /hatrack_assembly_action

    approaching/linear_speed          : 0.02
    approaching/angular_speed         : 0.01
    approaching/goal                  : [0.0, 0.00, 0.070, 0.0, 0.0, 0.0, 0.0]
    approaching/linear_tollerance     : 0.001
    approaching/angular_tollerance    : 0.001
    approaching/goal_link             : "hatbox_clamper"
    approaching/goal_reference_frame  : "hatbox_clamper"

    touching/goal_twist               : [0.0,  -0.01, 0.0, 0.0, 0.0, 0.0 ]
    touching/target_wrench            : [0.0, -25.00, 0.0, 0.0, 0.0, 0.0 ]
    touching/wrench_toll              : [5,5,5,5,5,5]
    touching/wrench_deadband          : [10,10,10,10,10,10]
    touching/target_wrench_frame      : "hatbox_clamper"
    touching/goal_twist_frame         : "hatbox_clamper"

    coupling/goal_twist               : [0.0,  0.001,    0.01, 0.0, 0.0, 0.0]
    coupling/target_wrench            : [0.0, -25.000,  25.00, 0.0, 0.0, 0.0]
    coupling/wrench_toll              : [5,5,5,5,5,5]
    coupling/wrench_deadband          : [10,10,10,10,10,10]
    coupling/target_wrench_frame      : "hatbox_clamper"
    coupling/goal_twist_frame         : "hatbox_clamper"

    alignment/goal_quaternion         : [ 0.0, 0.0, -0.435, 0.9 ]
    alignment/goal                    : [ 0.0, 0.02, 0.02, 0.0, 0.0, 0.0, 1.0 ]

    lifting_goal/goal                 : [-0.175, 0.0, 0.325, 0.0, -0.065, 0.0, 0.998]

    correct/linear_speed              : 0.01
    correct/goal_link                 : "hatbox_clamper"
    correct/goal_reference_frame      : "base"
    correct/goal                      : [0.06, 0.0, 0.025, 0.0, 0.0, 0.0, 1.0 ]

    probing/goal_twist               : [0.000, 0.0,0.01,0,0.0 ]
    probing/target_wrench            : [ 30 ]
    probing/wrench_toll              : [5,5,5,5,5,5]
    probing/wrench_deadband          : [10,10,10,10,10,10]
    probing/target_wrench_frame      : "hatbox_clamper_bracket"
    probing/goal_twist_frame         : "hatbox_clamper_bracket"

    detach/goal                      : [.0, 0.0, -0.005, 0.0, 0.0, 0.0, 1]

    insert_x/goal_twist           : [ 0.010, 0.0, 0.0,  0.,0.0, 0.0, 1.0 ]
    insert_x/target_wrench        : 30
    insert_x/target_wrench_frame  : hatbox_clamper_bracket
    insert_x/goal_twist_frame     : base

    insert_z/goal_twist           : [ 0.005, 0.0, -0.010,  0.,0.0, 0.0, 1.0 ]
    insert_z/target_wrench        : 30
    insert_z/target_wrench_frame  : hatbox_clamper_bracket
    insert_z/goal_twist_frame     : base

    insert_i/goal                 : [ 0.005, 0.0, 0.00, 0.0, 0.0, 0.0, 1.0]

    insert_ii/goal                : [ 0.000, 0.0, -0.06, 0.0, 0.0, 0.0, 1.0]


  </rosparam>
  <remap from="~attach_object" to="/context_manager/attach_object" />
</node>
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->







<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Configuration as needed -->
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!--group if="$(arg sim_sweepee)" >
  <node  pkg="rosservice"
        name="sim_vision_system"
        type="rosservice"
        args="call \-\-wait  /context_manager/configure_scene
              '{ hatbox_pose: { position: { x:  0.000, y: -0.890, z:  0.908},  orientation: {x:  0.0, y: -0.358, z:  0.935, w:  0.0} },
                 window_pose: { position: { x:  0.000, y: -2.000, z:  0.000},  orientation: {x:  0.0, y: 0.0000, z:  0.000, w:  1.0} } }'" />

</group-->

<group if="$(arg sim_sweepee)" >
  <node  pkg="rosservice"
        name="sim_vision_system"
        type="rosservice"
        args="call --wait  /context_manager/configure_scene
              '{ hatbox_pose: { position: { x:  0.013, y: -0.826, z:  0.860},  orientation: {x:  0.0, y: -0.344, z:  0.955896, w:  0.0} },
                 window_pose: { position: { x:  0.000, y: -1.890, z:  0.000},  orientation: {x:  0.0, y: 0.0000, z:  0.000, w:  1.0} } }'" />

</group>


<!--group if="$(arg rviz)" >
  <node name="rviz" pkg="rviz" type="rviz" respawn="false" output="screen" />
</group-->
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


<!-- include file="$(find teleop_twist_joy)/launch/teleop.launch" / -->

</launch>
