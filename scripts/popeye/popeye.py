import rospy
from eureca_hosts import eureca_hosts
import actionlib
from popeye_assembly_msgs.msg import ExecuteAction,ExecuteGoal
import roslaunch
import os
from pexpect import pxssh
from std_msgs.msg import Bool
import socket
import time
import traceback


os.environ["ROSLAUNCH_SSH_UNKNOWN"] = "1"

class remote_popeye:
    def __init__(self):
        self.s = pxssh.pxssh(maxread=10000)
        # self.s.SSH_OPTS = ("-X")
        hostname = "192.169.0.180"
        username = "volta"
        password = "volta"
        self.s.login (hostname, username, password)
        self.s.sendline ('export DISPLAY=:0')
        self.s.sendline ("xterm -e 'python /home/volta/eureca_ws/src/popeye_brain/scripts/popeye_brain_tcp_server.py 2>&1 | tee pd_log.txt' ")
        # self.s.sendline ('python /home/volta/eureca_ws/src/popeye_brain/scripts/popeye_brain_tcp_server.py')
        self.s.prompt()            
        print(self.s.before)
        time.sleep(0.2)

    # def align(self):
    #     self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)        
    #     t_end = time.time() + 0.1
    #     while time.time() < t_end:
    #         try:
    #             self.sock.connect(('192.169.0.180', 10000))
    #         except Exception as e: print(e)
    #     try:
    #         self.sock.sendall(str(3)) 
    #     except Exception as e: print(e)
        
    #     rospy.wait_for_message("/popeye_align_completed",Bool)

    def start(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        t_end = time.time() + 0.1
        while time.time() < t_end:
            try:
                self.sock.connect(('192.169.0.180', 10000))
            except Exception as e: print(e)
        try:
            self.sock.sendall(str(1)) 
        except Exception as e: print(e)
                
        

    def stop(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        t_end = time.time() + 0.1
        while time.time() < t_end:
            try:
                self.sock.connect(('192.169.0.180', 10000))
            except Exception as e: print(e)
        try:
            self.sock.sendall(str(5)) 
        except Exception as e: print(e)
        time.sleep(5)
        
    def close(self):
        self.s.logout()




class popeyeClass:
    
    def __init__(self):
        rospy.logwarn("Creating popeye class")
        # self.__HOST_UP  = eureca_hosts.check_host(eureca_hosts.CAMERA_POPEYE_IP) and eureca_hosts.check_host(eureca_hosts.NUC_POPEYE_IP) 
        # self.__delay = eureca_hosts.check_time_host(eureca_hosts.CAMERA_POPEYE_IP) and eureca_hosts.check_time_host(eureca_hosts.NUC_POPEYE_IP)
        
        self.docking_position = rospy.get_param("/popeye/status/docking_position_init") if rospy.has_param("/popeye/status/docking_position_init") else rospy.get_param("/popeye/status/position_init")
        self.position         = rospy.get_param("/popeye/status/position"    ) if rospy.has_param("/popeye/status/position"    ) else rospy.get_param("/popeye/status/position_init")
        self.docked           = rospy.get_param("/popeye/status/docked"      ) if rospy.has_param("/popeye/status/docked"      ) else False
        self.ready            = rospy.get_param("/popeye/status/ready"       ) if rospy.has_param("/popeye/status/ready"       ) else False
        self.empty            = rospy.get_param("/popeye/status/empty"       ) if rospy.has_param("/popeye/status/empty"       ) else True
        self.floor            = rospy.get_param("/popeye/status/floor"       ) if rospy.has_param("/popeye/status/floor"       ) else 0
        self.is_moving        = rospy.get_param("/popeye/status/is_moving"   ) if rospy.has_param("/popeye/status/is_moving"   ) else False
        self.emergency        = rospy.get_param("/popeye/status/emergency"   ) if rospy.has_param("/popeye/status/emergency"   ) else False
        self.status_update( )
        
        rospy.logwarn("finish creating popeye class")
        
    def status_update(self, floor =  [], position = [], docked = [], ready = [] , docking_position = []):
        if (floor or floor==0):
            self.floor              = floor
        if position             :    self.position            = position
        if docking_position     :    self.docking_position    = docking_position
        if docked               :    self.docked              = docked
        if ready                :    self.ready  = ready
        rospy.set_param("/popeye/status/floor",self.floor)
        rospy.set_param("/popeye/status/position",self.position)
        rospy.set_param("/popeye/status/ready",self.ready)
        rospy.set_param("/popeye/status/empty",self.empty)
        rospy.set_param("/popeye/status/docked",self.docked)
        rospy.set_param("/popeye/status/is_moving", self.is_moving )
        rospy.set_param("/popeye/status/emergency", self.emergency )
        rospy.set_param("/popeye/status/docking_position", self.docking_position )
        
        
    def get_status( self ):
        return rospy.get_param("/popeye/status" )
    
    def hatrack_assembly(self,request):
        client = actionlib.SimpleActionClient('/hatrack_assembly_action', ExecuteAction)
        client.wait_for_server()
        goal = ExecuteGoal(request)
        client.send_goal(goal)
        client.wait_for_result()
        return client.get_result()
    
    
    
    
