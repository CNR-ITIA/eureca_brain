#!/usr/bin/env python
import time
from pexpect import pxssh
import rospy

rospy.init_node('start_popeye', anonymous=True,disable_signals=False)  

s = pxssh.pxssh(maxread=10000)
s.SSH_OPTS = ("-X")
hostname = "192.169.0.180"
username = "volta"
password = "volta"
s.login (hostname, username, password)
s.sendline ('export DISPLAY=localhost:10.0')
s.sendline ("xterm -e 'python /home/volta/eureca_ws/src/popeye_brain/scripts/popeye_brain_tcp_server.py 2>&1 | tee pd_log.txt' ")
s.prompt()            
print(s.before)
time.sleep(1)

rospy.spin()


