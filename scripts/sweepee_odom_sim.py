#!/usr/bin/env python
import rospy
import tf
from nav_msgs.msg import Odometry
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose,Point,Quaternion
import numpy as np
import quaternion
import math

rospy.init_node('sweepee_odom_simfal', anonymous=True,disable_signals=False)  

rospy.sleep(5)

pub_odom        = rospy.Publisher('/sweepee/odom_simfal', Odometry, queue_size=1)  
pub_odom_olivia = rospy.Publisher('/olivia/odom_simfal', Odometry, queue_size=1,latch=True)  
pub_odom_popeye = rospy.Publisher('/popeye/odom_simfal', Odometry, queue_size=1,latch=True)  
pub_odom_bluto  = rospy.Publisher('/bluto/odom_simfal', Odometry, queue_size=1,latch=True) 
pub_jnt_olivia  = rospy.Publisher('/iiwa_joints', JointState, queue_size=1,latch=True)
pub_jnt_popeye  = rospy.Publisher('/popeye/joint_states_simfal', JointState, queue_size=1,latch=True)

pos_init_olivia = rospy.get_param("/olivia/status/position_init")
pos_init_popeye = rospy.get_param("/popeye/status/position_init")
pos_init_bluto  = rospy.get_param("/bluto/status/position_init")

odom_olivia = Odometry()
odom_olivia.header.frame_id = "map_sweepee"
odom_olivia.header.stamp    =  rospy.Time.now()
odom_olivia.pose.pose       =  Pose(Point(pos_init_olivia[0],pos_init_olivia[1],0),Quaternion(0.0,0.0,pos_init_olivia[2],pos_init_olivia[3])) 
pub_odom_olivia.publish(odom_olivia)

odom_popeye = Odometry()
odom_popeye.header.frame_id = "map_sweepee"
odom_popeye.header.stamp    =  rospy.Time.now()
baseqp = np.quaternion(0.0,0.0,pos_init_popeye[2],pos_init_popeye[3])
rot_qp = np.quaternion(0.707,0.707,0,0)
pd    = baseqp * rot_qp
odom_popeye.pose.pose       =  Pose(Point(pos_init_popeye[0],pos_init_popeye[1],0),Quaternion(pd.components[0],pd.components[1],pd.components[2],pd.components[3])) 
pub_odom_popeye.publish(odom_popeye)


odom_bluto = Odometry()
odom_bluto.header.frame_id = "map_sweepee"
odom_bluto.header.stamp    =  rospy.Time.now()
odom_bluto.pose.pose       =  Pose(Point(pos_init_bluto[0],pos_init_bluto[1],0),Quaternion(0.0,0.0,pos_init_bluto[2],pos_init_bluto[3])) 
pub_odom_bluto.publish(odom_bluto)

jnt_olivia = JointState()
jnt_olivia.header.stamp    =  rospy.Time.now()
jnt_olivia.name = ['iiwa_joint_1', 'iiwa_joint_2', 'iiwa_joint_3', 'iiwa_joint_4', 'iiwa_joint_5', 'iiwa_joint_6', 'iiwa_joint_7']
jnt_olivia.position = [-0.28, -0.75, 0.32, 1.0, -0.19, -1.4, 1.58]
pub_jnt_olivia.publish(jnt_olivia)

jnt_popeye = JointState()
jnt_popeye.header.stamp    =  rospy.Time.now()
jnt_popeye.name = ['popeye_frontal', 'popeye_shoulder', 'popeye_elbow', 'popeye_wrist']
jnt_popeye.position = [0.0, 0.055, 2.62, -0.50]
pub_jnt_popeye.publish(jnt_popeye)

listener = tf.TransformListener()
rate_sim = rospy.Rate(10) 

rotw   = []
transw = []
rot    = []
trans  = []

x_shift = [0,  5.95 , 11.60]
y_shift = [0, -0.4  , -3.0]

docked_obj = "none"
odom_sim = Odometry()
odom_sim.header.frame_id = "map_sweepee"

while True and not rospy.is_shutdown():
    try:
        (trans,rot) = listener.lookupTransform('/map_sweepee','/sweepee/base_footprint', rospy.Time(0))
    except Exception as e:
        rospy.logwarn_throttle(5,"exc 76")
        continue
    
    floor = rospy.get_param("/sweepee/status/floor")
        
    odom_sim.header.stamp    =  rospy.Time.now()
    
    if floor == 0:
        odom_sim.pose.pose       =  Pose(Point(trans[0],trans[1],0),Quaternion(rot[0],rot[1],rot[2],rot[3]))    
    elif floor == 1:
        baseq = np.quaternion(rot[0],rot[1],rot[2],rot[3])
        rot_q = np.quaternion(0.707,-0.707,0,0)
        pd    = baseq * rot_q
        odom_sim.pose.pose       =  Pose(Point(trans[1] + x_shift[floor] ,-trans[0]+ y_shift[floor] , 1.7),Quaternion(pd.components[0],pd.components[1],pd.components[2],pd.components[3]))    
    elif floor == 2:
        baseq = np.quaternion(rot[0],rot[1],rot[2],rot[3])
        rot_q = np.quaternion(0.707,0.707,0,0)
        pd    = baseq * rot_q
        odom_sim.pose.pose       =  Pose(Point(trans[1] + x_shift[floor] ,-trans[0]+ y_shift[floor] , 3.1),Quaternion(pd.components[0],pd.components[1],pd.components[2],pd.components[3])) 
        
    pub_odom.publish(odom_sim)
    
    if rospy.has_param("/sweepee/status/docked_obj"):
        docked_obj = rospy.get_param("/sweepee/status/docked_obj")
    
    if docked_obj == "popeye":
        pop_sim = Odometry()
        pop_sim = odom_sim
        if floor == 0:
            baseq = np.quaternion(rot[0],rot[1],rot[2],rot[3])
            rot_q = np.quaternion(0.707,0.707,0,0)
            pd    = baseq * rot_q
            pop_sim.pose.pose       =  Pose(Point(trans[0],trans[1],0),Quaternion(pd.components[0],pd.components[1],pd.components[2],pd.components[3]))    
        pub_odom_popeye.publish(pop_sim)
        
    elif docked_obj == "olivia":
        ol_sim = Odometry()
        ol_sim = odom_sim
        if floor == 2:
            baseq = np.quaternion(rot[0],rot[1],rot[2],rot[3])
            rot_q = np.quaternion(0.707,-0.707,0,0)
            pd    = baseq * rot_q
            ol_sim.pose.pose       =  Pose(Point(trans[1] + x_shift[floor] ,-trans[0]+ y_shift[floor] ,3.1),Quaternion(pd.components[0],pd.components[1],pd.components[2],pd.components[3]))    
        pub_odom_olivia.publish(ol_sim)
        
    elif docked_obj == "bluto":
        bl_sim = Odometry()
        bl_sim = odom_sim
        if floor == 2:
            baseq = np.quaternion(rot[0],rot[1],rot[2],rot[3])
            rot_q = np.quaternion(0.707,-0.707,0,0)
            pd    = baseq * rot_q
            bl_sim.pose.pose       =  Pose(Point(trans[1] + x_shift[floor] ,-trans[0]+ y_shift[floor] ,3.1),Quaternion(pd.components[0],pd.components[1],pd.components[2],pd.components[3]))    
        pub_odom_bluto.publish(bl_sim)
    
    rate_sim.sleep()
    
