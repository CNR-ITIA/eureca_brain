import rospy
import roslaunch
from roslaunch import remoteprocess,remote
import actionlib
import time, math
import os
import signal
import sys
from eureca_hosts import eureca_hosts
from transitions import Machine
from transitions.extensions import MachineFactory
from geometry_msgs.msg import Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from actionlib_msgs.msg import GoalStatus
from map_server.srv import LoadMap 
from laser_line_extraction.msg import LineSegmentList, LineSegment
from threading import Thread, Lock
from sensor_msgs.msg import LaserScan
from eureca_brain.srv import CartAttach,CartAttachResponse
from eureca_brain.msg import DockingAction, DockingGoal
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseWithCovarianceStamped
from std_srvs.srv import Empty
import tf
import dynamic_reconfigure.client
import traceback

os.environ["ROSLAUNCH_SSH_UNKNOWN"] = "1"



class cart_docking: #For carts docking

    def start(self,cart_name,forward_dist):
        try:
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(self.uuid)
            self.master_re = roslaunch.core.Master(type_="rosmaster",uri="http://"+str(eureca_hosts.DISPATCHER_IP)+":11311/")
            self.sweepee_machine = roslaunch.core.Machine("sweepee", str(eureca_hosts.SWEEPEE_IP),env_loader="/home/abaco/env.sh" , user="abaco", password="abaco")
            self.node1 = roslaunch.core.Node("eureca_docking",
                                     "eureca_docking_node",
                                     name="docking",
                                     machine_name="sweepee",
                                     output="log",
                                     args="_scan_topic:=sweepee/scan/front _camera_frame:=/usb_cam _tag_frame:=/" + str(cart_name)  + 
                                          " _base_frame:=sweepee/base_footprint _odom_frame:=sweepee/odom _forward_dist:=" + str(forward_dist) +
                                          " _linear_tolerance:=0.002 _angle_tolerance:=0.002 _alignment_distance:=0.7 _cmd_vel:=/cmd_vel_mux/input/docking",
                                     remap_args=[("cmd_vel","/cmd_vel_mux/input/docking")])
            self.launch = roslaunch.scriptapi.ROSLaunch()
            self.parent = roslaunch.parent.ROSLaunchParent(self.uuid, [], is_core=False)
            self.base_conf = roslaunch.config.ROSLaunchConfig();
            self.base_conf.add_machine(self.sweepee_machine)
            self.base_conf.add_node(self.node1)
            self.base_conf.assign_machines()
            self.base_conf.set_master(self.master_re)
            self.parent.config = self.base_conf
            self.launch.parent = self.parent
            rospy.sleep(3)
            self.launch.start()
            return True
        except Exception as e: print(e,traceback.format_exc())
        
    def stop(self):
        self.launch.stop()
        return True


class usb_tag_dock: #For carts docking
    def __init__(self):
        self.launch = None
        
    def start(self,up=False):
        try:
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(self.uuid)
            if not up:
                self.launch = roslaunch.parent.ROSLaunchParent(self.uuid, ["/home/stade/dry_run_ws/src/eureca_brain/launch/remotes_lauch/docking.launch"])
            else:
                self.launch = roslaunch.parent.ROSLaunchParent(self.uuid, ["/home/stade/dry_run_ws/src/eureca_brain/launch/remotes_lauch/docking_up.launch"])
            self.launch.start()
            return True
        except Exception as e: print(e)

        
    def stop(self):
        if self.launch is not None:
            try:
                self.launch.shutdown()
                return True
            except Exception as e: print(e)



class remote_line_both: #For cargo docking on the lateral blinder
    def __init__(self):
        try:
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, True)
            roslaunch.configure_logging(self.uuid)
            self.master_re = roslaunch.core.Master(type_="rosmaster",uri="http://"+str(eureca_hosts.DISPATCHER_IP)+":11311/")
            self.sweepee_machine = roslaunch.core.Machine("sweepee", str(eureca_hosts.SWEEPEE_IP),env_loader="/home/abaco/env.sh" , user="abaco", password="abaco")
            self.node1 = roslaunch.core.Node("laser_line_extraction",
                                             "line_extraction_node",
                                             name="laser_line_cargo",
                                             machine_name="sweepee",
                                             output="screen",
                                             args="_frequency:=10.0 _frame_id:=sweepee/base_footprint _scan_topic:=/sweepee/scan_multi _publish_markers:=true \
                                                   _bearing_std_dev:=0.001 _range_std_dev:=0.012 _least_sq_angle_thresh:=0.0001 _least_sq_radius_thresh:=0.0001  \
                                                   _max_line_gap:=0.4 _min_line_length:=0.2 _min_range:=0.2 _min_split_dist:=0.1 _outlier_dist:=0.02 _min_line_points:=8 ")                                              
            self.launch = roslaunch.scriptapi.ROSLaunch()
            self.parent = roslaunch.parent.ROSLaunchParent(self.uuid, [], is_core=False)
            self.base_conf = roslaunch.config.ROSLaunchConfig();
            self.base_conf.add_machine(self.sweepee_machine)
            self.base_conf.add_node(self.node1)
            self.base_conf.assign_machines()
            self.base_conf.set_master(self.master_re)
            self.parent.config = self.base_conf
            self.launch.parent = self.parent
        except Exception as e: print(e)


    def start(self):
        try:
            self.launch.start()
        except Exception as e: print(e)
        return True
        
    def stop(self):
        try:
            self.launch.stop()
        except Exception as e: print(e)
        return True


class remote_line_front: #For door docking on 2nd floor
    def __init__(self):
        self.launch = None
    def start(self):
        try:
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, True)
            roslaunch.configure_logging(self.uuid)
            self.master_re = roslaunch.core.Master(type_="rosmaster",uri="http://"+str(eureca_hosts.DISPATCHER_IP)+":11311/")
            self.sweepee_machine = roslaunch.core.Machine("sweepee", str(eureca_hosts.SWEEPEE_IP),env_loader="/home/abaco/env.sh" , user="abaco", password="abaco")
            self.node1 = roslaunch.core.Node("laser_line_extraction",
                                             "line_extraction_node",
                                             name="laser_line_door_dock",
                                             machine_name="sweepee",
                                             output="screen",
                                             args="_frequency:=10.0 _frame_id:=sweepee/front_laser _scan_topic:=/sweepee/scan/front _publish_markers:=true \
                                                   _bearing_std_dev:=0.001 _range_std_dev:=0.012 _least_sq_angle_thresh:=0.0001 _least_sq_radius_thresh:=0.0001  \
                                                   _max_line_gap:=0.4 _min_line_length:=0.2 _min_range:=0.2 _min_split_dist:=0.1 _outlier_dist:=0.02 _min_line_points:=8 ")
            self.launch = roslaunch.scriptapi.ROSLaunch()
            self.parent = roslaunch.parent.ROSLaunchParent(self.uuid, [], is_core=False)
            self.base_conf = roslaunch.config.ROSLaunchConfig();
            self.base_conf.add_machine(self.sweepee_machine)
            self.base_conf.add_node(self.node1)
            self.base_conf.assign_machines()
            self.base_conf.set_master(self.master_re)
            self.parent.config = self.base_conf
            self.launch.parent = self.parent
            rospy.sleep(1)
            self.launch.start()
            return True
        except Exception as e: print(e)

        
    def stop(self):
        if self.launch is not None:
            try:
                self.launch.stop()
            except Exception as e: print(e)
        return True


def ccc(cf):
    print("callback:")

class docking_popeye: 
    
    def __init__(self):
        try:
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, True)
            roslaunch.configure_logging(self.uuid)
            self.master_re = roslaunch.core.Master(type_="rosmaster",uri="http://"+str(eureca_hosts.DISPATCHER_IP)+":11311/")
            self.sweepee_machine = roslaunch.core.Machine("sweepee", str(eureca_hosts.SWEEPEE_IP),env_loader="/home/abaco/env.sh" , user="abaco", password="abaco")
            self.node1 = roslaunch.core.Node("docking_popeye",
                                             "docking_popeye_node",
                                             name="docking_popeye_app",
                                             machine_name="sweepee",
                                             output="screen",
                                             args="" )
            rospy.set_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/param/fixed_laser_topic", "/sweepee/scan/rear")
            rospy.set_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/param/laser_scan_topic" , "/sweepee/scan/rear")
            rospy.set_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/param/frame_name"       , "/sweepee/rear_laser")
            self.launch = roslaunch.scriptapi.ROSLaunch()
            self.parent = roslaunch.parent.ROSLaunchParent(self.uuid, [], is_core=False)
            self.base_conf = roslaunch.config.ROSLaunchConfig();
            self.base_conf.add_machine(self.sweepee_machine)
            self.base_conf.add_node(self.node1)
            self.base_conf.assign_machines()
            self.base_conf.set_master(self.master_re)
            self.parent.config = self.base_conf
            self.launch.parent = self.parent
        except Exception as e: print(e)


    def start(self):
        try:
            rospy.set_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/status"       , "not_done")
            self.launch.start()
            return True
        except Exception as e: print(e)

        
    def stop(self):
        try:
            self.launch.stop()
        except Exception as e: print(e)
        return True


class sweepeeClass:
  
    def __init__(self):

        rospy.logwarn("Creating sweepee class")
        
        self._HOST_UP  = True if eureca_hosts.check_host(eureca_hosts.SWEEPEE_IP) else False
        
        if not self._HOST_UP:
            rospy.logerr("SWEEPEE IS NOT UP")
            return
        # self.__delay    = eureca_hosts.check_time_host(eureca_hosts.SWEEPEE_IP)
        # rospy.logwarn("Time sweepee %s:   ",str(self.__delay))
        

        self.__w_vel_sweepee  = 0.0
        self.__x_odom_sweepee = 0.0
        self.__y_odom_sweepee = 0.0
        self.__coeffM_cargo   = 0.0
        self.__midPx          = 0.0
        self.__midPy          = 0.0
        self.__coeffM         = 0.0
        self.__rot_dock_ex    = 0.0
        self.__gap_dock_ex    = 0.0
        

        ####  ROS DATA
        self.__rate             = rospy.Rate(50)
        self.move_base_action   = actionlib.SimpleActionClient('/sweepee/move_base', MoveBaseAction)
        self.__client_cart_dock = actionlib.SimpleActionClient('/docking', DockingAction) #TODO noma cambia con carrello
        self.__pub_vel          = rospy.Publisher('/agv/command/velocity', Twist, queue_size=1)
        self.__odom_sub         = None 
        self.__change_map       = rospy.ServiceProxy('/sweepee/change_map', LoadMap)
        self._relocalize_amcl   = rospy.ServiceProxy("/sweepee/request_nomotion_update", Empty)
        self.__cart_attach      = rospy.ServiceProxy('/sweepee/cart_attach', CartAttach)
        self.__clearmap         = rospy.ServiceProxy('/sweepee/move_base/clear_costmaps', Empty)
        print("ciao")
        try:
            self.dynrec             = dynamic_reconfigure.client.Client("/sweepee/move_base/TebLocalPlannerROS", timeout=30, config_callback=ccc)
        except Exception as e: print("exc:",e)
        print("ciao2")
        
        ####  ROSLAUNCH CALLS
        self.dock_popeye_call   = docking_popeye()
        self.line_both_caller   = remote_line_both()
        self.usb_tag_dock       = usb_tag_dock()
        self.door_dock_launch   = remote_line_front()
        
        self.listener = tf.TransformListener()

        self.__mutex   = Lock()

        #### FSM data
        self.position         = rospy.get_param("/sweepee/status/position"    ) if rospy.has_param("/sweepee/status/position"    ) else rospy.get_param("/sweepee/status/position_init_0")
        self.docked_obj       = rospy.get_param("/sweepee/status/docked_obj"  ) if rospy.has_param("/sweepee/status/docked_obj"  ) else "none"
        self.ready            = rospy.get_param("/sweepee/status/ready"       ) if rospy.has_param("/sweepee/status/ready"       ) else False
        self.empty            = rospy.get_param("/sweepee/status/empty"       ) if rospy.has_param("/sweepee/status/empty"       ) else True
        self.floor            = rospy.get_param("/sweepee/status/floor"       ) if rospy.has_param("/sweepee/status/floor"       ) else 0
        self.is_moving        = rospy.get_param("/sweepee/status/is_moving"   ) if rospy.has_param("/sweepee/status/is_moving"   ) else False
        self.emergency        = rospy.get_param("/sweepee/status/emergency"   ) if rospy.has_param("/sweepee/status/emergency"   ) else False
        self.status_update()   
        
        self.emergency              = False
        
        # self.__level_zero_task_start_begin() #TODO
        
        rospy.logwarn("Finished creating sweepee class")

#TODO posizione iniziale che vede i carrelli

    def initial_pose_sweepee_0(self):
        init_pose = PoseWithCovarianceStamped()
        init_pose.header.frame_id = "map_sweepee"
        init_pose.header.stamp    =  rospy.Time.now()
        pos_goal = rospy.get_param("/sweepee/status/position_init_0")
        init_pose.pose.pose.position.x    = pos_goal[0]
        init_pose.pose.pose.position.y    = pos_goal[1]
        init_pose.pose.pose.orientation.z = pos_goal[2]
        init_pose.pose.pose.orientation.w = pos_goal[3]
        pub = rospy.Publisher('/sweepee/initialpose', PoseWithCovarianceStamped, queue_size=1)
        t_end = time.time() + 1.5
        while time.time() < t_end:
            pub.publish(init_pose)
            self.__rate.sleep()
        pub.unregister()
        return True

    def initial_pose_sweepee_0_return(self):
        init_pose = PoseWithCovarianceStamped()
        init_pose.header.frame_id = "map_sweepee"
        init_pose.header.stamp    =  rospy.Time.now()
        pos_goal = rospy.get_param("/sweepee/status/position_init_exit_0")
        init_pose.pose.pose.position.x    = pos_goal[0]
        init_pose.pose.pose.position.y    = pos_goal[1]
        init_pose.pose.pose.orientation.z = pos_goal[2]
        init_pose.pose.pose.orientation.w = pos_goal[3]
        pub = rospy.Publisher('/sweepee/initialpose', PoseWithCovarianceStamped, queue_size=1)
        t_end = time.time() + 1.5
        while time.time() < t_end:
            pub.publish(init_pose)
            self.__rate.sleep()
        pub.unregister()
        return True

    def initial_pose_sweepee_1(self):
        init_pose = PoseWithCovarianceStamped()
        init_pose.header.frame_id = "map_sweepee"
        init_pose.header.stamp    =  rospy.Time.now()
        pos_goal = rospy.get_param("/sweepee/status/position_init_1")
        init_pose.pose.pose.position.x    = pos_goal[0]
        init_pose.pose.pose.position.y    = pos_goal[1]
        init_pose.pose.pose.orientation.z = pos_goal[2]
        init_pose.pose.pose.orientation.w = pos_goal[3]
        pub = rospy.Publisher('/sweepee/initialpose', PoseWithCovarianceStamped, queue_size=1)
        t_end = time.time() + 1.5
        while time.time() < t_end:
            pub.publish(init_pose)
            self.__rate.sleep()
        pub.unregister()
        return True

    def initial_pose_sweepee_2(self):
        init_pose = PoseWithCovarianceStamped()
        init_pose.header.frame_id = "map_sweepee"
        init_pose.header.stamp    =  rospy.Time.now()
        pos_goal = rospy.get_param("/sweepee/status/position_init_2")
        init_pose.pose.pose.position.x    = pos_goal[0]
        init_pose.pose.pose.position.y    = pos_goal[1]
        init_pose.pose.pose.orientation.z = pos_goal[2]
        init_pose.pose.pose.orientation.w = pos_goal[3]
        pub = rospy.Publisher('/sweepee/initialpose', PoseWithCovarianceStamped, queue_size=1)
        t_end = time.time() + 1.5
        while time.time() < t_end:
            pub.publish(init_pose)
            self.__rate.sleep()
        pub.unregister()
        return True

    def relocalize_amcl_srv(self):
        resp = True
        try:
            resp = self._relocalize_amcl()
            rospy.sleep(0.5)
            return resp
        except:
            print("Service call failed")
        finally:
            print (resp)


    def _sweepee_callback_odom(self,data):
            self.__x_odom_sweepee = data.twist.twist.linear.x
            self.__y_odom_sweepee = data.twist.twist.linear.y
            self.__w_vel_sweepee  = data.twist.twist.angular.z

    def sweepee_move_forw(self,dist, vel):
        self.__odom_sub = rospy.Subscriber("/sweepee/odom", Odometry, self._sweepee_callback_odom)
        try:
            rospy.wait_for_message("/sweepee/odom", Odometry)
            dist_made = 0.0
            vel_msg = Twist()
            if dist < 0.0:
                vel = -1.0 * vel
            vel_msg.linear.x = vel
            time_now = time.time()
            while (math.fabs(dist_made) < math.fabs(dist)) and not rospy.is_shutdown() and not self.emergency:
                self.__pub_vel.publish(vel_msg)
                dist_made = dist_made + self.__x_odom_sweepee * (time.time() - time_now)
                time_now = time.time()
                self.__rate.sleep()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                vel_msg.linear.x = 0.0
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
        except :
            print("Exception")
            vel_msg = Twist()
            t_end = time.time() + 0.5
            while time.time() < t_end:
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
            return False
        finally:
            vel_msg = Twist()
            t_end = time.time() + 0.5
            while time.time() < t_end:
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
            self.__odom_sub.unregister()
            self.__odom_sub = None
        return True

    def sweepee_move_lat(self,dist, vel):
        self.__odom_sub = rospy.Subscriber("/sweepee/odom", Odometry, self._sweepee_callback_odom)
        try:
            rospy.wait_for_message("/sweepee/odom", Odometry)
            dist_made = 0.0
            vel_msg = Twist()
            if dist < 0.0:
                vel = -1.0 * vel
            vel_msg.linear.y = vel
            time_now = time.time()
            while (math.fabs(dist_made) < math.fabs(dist)) and not rospy.is_shutdown() and not self.emergency:
                self.__pub_vel.publish(vel_msg)
                dist_made = dist_made + self.__y_odom_sweepee * (time.time() - time_now)
                time_now = time.time()
                self.__rate.sleep()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                vel_msg.linear.y = 0.0
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
        except :
            print("Exception")
            vel_msg = Twist()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
            return False
        finally:
            vel_msg = Twist()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
            self.__odom_sub.unregister()
            self.__odom_sub = None
        return True

    def sweepee_move_yaw(self,dist, vel):
        self.__odom_sub = rospy.Subscriber("/sweepee/odom", Odometry, self._sweepee_callback_odom)
        try:
            rospy.wait_for_message("/sweepee/odom", Odometry)
            dist_made = 0.0
            vel_msg = Twist()
            if dist < 0.0:
                vel = -1.0 * vel
            vel_msg.angular.z = vel
            time_now = time.time()
            while (math.fabs(dist_made) < math.fabs(dist)) and not rospy.is_shutdown() and not self.emergency:
                self.__pub_vel.publish(vel_msg)
                dist_made = dist_made + self.__w_vel_sweepee * (time.time() - time_now)
                time_now = time.time()
                self.__rate.sleep()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                vel_msg.angular.z = 0.0
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
        except :
            vel_msg = Twist()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
            return False
        finally:
            vel_msg = Twist()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
            self.__odom_sub.unregister()
            self.__odom_sub = None
        return True

    def pos_err(self,pos_in,trans):
        return math.sqrt(((pos_in[0]-trans[0])**2)+((pos_in[1]-trans[1])**2))

    def sweepee_movebase_client(self, pos_in):
        
        ret = False
        try:
            self.move_base_action.wait_for_server()
            goal = MoveBaseGoal()
            goal.target_pose.header.frame_id = "map_sweepee"
            print("moving to : ",pos_in[0], pos_in[1], pos_in[2], pos_in[3])
            goal.target_pose.header.stamp = rospy.Time.now()
            goal.target_pose.pose.position.x    = pos_in[0]
            goal.target_pose.pose.position.y    = pos_in[1]
            goal.target_pose.pose.orientation.z = pos_in[2]
            goal.target_pose.pose.orientation.w = pos_in[3]

            self.move_base_action.send_goal(goal)
            wait = self.move_base_action.wait_for_result()
            print("Action finished: ",wait)
            listener = tf.TransformListener()
            rot   = []
            trans = []
            while rot == [] and not rospy.is_shutdown():
                self.__rate.sleep()
                try:
                    (trans,rot) = listener.lookupTransform('/map_sweepee','/sweepee/base_footprint', rospy.Time(0))
                except Exception as e: print(e)
                
            err_move = self.pos_err(pos_in,trans)
            print("pos_err:",err_move)
        
            if not wait:
                rospy.logerr("Action server interrupted!")
                ret = False
            else:
                resin = self.move_base_action.get_state()
                ret   = resin == GoalStatus.SUCCEEDED
                
        except Exception as e: print(e)
        
        finally:
            if not ret:
                self.move_base_action.cancel_all_goals()
            return ret
        

    def change_map_srv(self,map_number):
        if map_number==0:
            path = "/home/stade/dry_run_ws/src/eureca_brain/maps/zero.yaml"
        elif map_number==1:
            path = "/home/stade/dry_run_ws/src/eureca_brain/maps/first.yaml"
        elif map_number==2:
            path = "/home/stade/dry_run_ws/src/eureca_brain/maps/second.yaml"
        else:
            return False
        
        try:
            resp1 = self.__change_map(path)
            return resp1.success
        except rospy.ROSInterruptException:
            rospy.loginfo("Maps switch failure.")
            return False

    def _range_lis(self,list1):
        lowest = list1[0]
        lowest2 = None
        for item in list1[1:]:
            if item < lowest:
                lowest2 = lowest
                lowest = item
            elif lowest2 == None or lowest2 > item:
                lowest2 = item
        return lowest, lowest2

    def _Lascallback_cargo(self,data):
        Points = []
        Plen = []
        for lines in data.line_segments:
            Points.append(lines.start)
            Points.append(lines.end)
            Plen.append(math.sqrt(lines.start[0] * lines.start[0] + lines.start[1] * lines.start[1]))
            Plen.append(math.sqrt(lines.end[0] * lines.end[0] + lines.end[1] * lines.end[1]))
        max1, max2 = self._range_lis(Plen)
        point1 = Points[Plen.index(max1)]
        point2 = Points[Plen.index(max2)]
        self.__mutex.acquire()
        self.__coeffM_cargo = (point2[1] - point1[1])/(point2[0] - point1[0])
        self.__mutex.release()

    def dock_cargo(self):
        try:
            sub_line = rospy.Subscriber("/line_segments", LineSegmentList, self._Lascallback_cargo, queue_size=1)
            self.line_both_caller.start()
            rospy.sleep(3)
            rospy.wait_for_message("/line_segments",LineSegmentList)
            vel_msg = Twist()
            while math.fabs(self.__coeffM_cargo) > 0.005 and not rospy.is_shutdown():
                self.__mutex.acquire()
                vel_msg.angular.z = min(max(0.4 * self.__coeffM_cargo, -0.2), 0.2)
                self.__mutex.release()
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
            return True
        except KeyboardInterrupt:
            return False
        finally:
            self.line_both_caller.stop()
            sub_line.unregister()
            vel_end = Twist()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                self.__pub_vel.publish(vel_end)
                self.__rate.sleep()
            print("sweepee cargo dock completed")

    def _Lascallback_door(self,data):
        Points = []
        Plen = []
        for lines in data.line_segments:
            if abs(lines.angle) > 0.3:
                continue
            Points.append(lines.start)
            Points.append(lines.end)
            Plen.append(math.sqrt(lines.start[0] * lines.start[0] + lines.start[1] * lines.start[1]))
            Plen.append(math.sqrt(lines.end[0] * lines.end[0] + lines.end[1] * lines.end[1]))
        if len(Plen) > 0 :
            max1, max2 = self._range_lis(Plen)
        point1 = Points[Plen.index(max1)]
        point2 = Points[Plen.index(max2)]
        self.__mutex.acquire()
        self.__midPx = (point2[0] + point1[0])/2.0
        self.__midPy = (point2[1] + point1[1])/2.0
        self.__coeffM = -1/((point2[1] - point1[1])/(point2[0] - point1[0]))
        self.__mutex.release()

    def door_docking_entry(self):
        try:
            print("STARTING DOOR DOCK")
            self.door_dock_launch.start()
            rospy.sleep(3)
            linesub = rospy.Subscriber("/line_segments", LineSegmentList, self._Lascallback_door)
            rospy.wait_for_message("/line_segments", LineSegmentList)
            vel_msg = Twist()
            while (math.fabs(self.__midPy) > 0.01 or math.fabs(self.__coeffM) > 0.02) and not rospy.is_shutdown():
                self.__mutex.acquire()
                vel_msg.linear.x = min(max(0.25 * (self.__midPx - 0.8), -0.25), 0.25)
                vel_msg.linear.y = min(max(0.25 * self.__midPy , -0.25), 0.25)
                vel_msg.angular.z = min(max(0.25 * self.__coeffM, -0.25), 0.25)
                self.__mutex.release()
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
            rospy.loginfo("Door docking finished")
            return True
        finally:
            self.door_dock_launch.stop()
            linesub.unregister()
            vel_end = Twist()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                self.__pub_vel.publish(vel_end)
                self.__rate.sleep()

    def _ScanBack_door(self,data):
        # pd_msg = data
        aperture = 0.174533 * 4 # 10 deg * tot
        mid_p = int(((data.angle_max * 2)/data.angle_increment)/2)
        p_low = int(mid_p - aperture/data.angle_increment)
        p_hi  = int(mid_p + aperture/data.angle_increment)
        new_data   = data.ranges[p_low:mid_p]
        new_data2  = data.ranges[mid_p:p_hi]
        new_data_b = [0 if (x < 3.0 and x > 0.1) else 1 for x in new_data  ]
        new_data_b2= [0 if (x < 3.0 and x > 0.1) else 1 for x in new_data2  ]
        r_dist = sum(data.ranges[p_low - 15 : p_low])
        l_dist = sum(data.ranges[p_hi  : p_hi + 15]) + 0.5 # empirico
        # new_data_d = [x if x < 2.0 else 0 for x in new_data  ]
        # pd_msg.ranges = [0 for x in pd_msg.ranges]
        # pd_msg.ranges[p_low:mid_p] = new_data_b
        # pd_msg.ranges[mid_p:p_hi]  = new_data_b2
        # self.__pub_test.publish(pd_msg)
        self.__rot_dock_ex = sum(new_data_b2) - sum(new_data_b)
        self.__gap_dock_ex = l_dist-r_dist
        # print(sum(new_data_b) - sum(new_data_b2))
        # print(r_dist-l_dist)

    def door_docking_exit(self):
        try:
            linesub = rospy.Subscriber("sweepee/scan/front", LaserScan, self._ScanBack_door,queue_size=1)
            rospy.wait_for_message("sweepee/scan/front", LaserScan)
            vel_msg = Twist()
            while (math.fabs(self.__gap_dock_ex) > 0.03 or math.fabs(self.__rot_dock_ex) > 1) and not rospy.is_shutdown():
                    vel_msg.angular.z = min(max(0.01 * self.__rot_dock_ex , -0.05), 0.05)
                    vel_msg.linear.y  = min(max(0.02 * self.__gap_dock_ex , -0.05), 0.05)
                    self.__pub_vel.publish(vel_msg)
                    self.__rate.sleep()
            while (math.fabs(self.__rot_dock_ex) > 1) and not rospy.is_shutdown():
                    vel_msg.angular.z = min(max(0.01 * self.__rot_dock_ex , -0.02), 0.02)
                    self.__pub_vel.publish(vel_msg)
                    self.__rate.sleep()
            while (math.fabs(self.__gap_dock_ex) > 0.03 ) and not rospy.is_shutdown():
                    vel_msg.linear.y  = min(max(0.02 * self.__gap_dock_ex , -0.05), 0.05)
                    self.__pub_vel.publish(vel_msg)
                    self.__rate.sleep()
            rospy.loginfo("Door docking finished")
            return True
        finally:
            linesub.unregister()
            vel_end = Twist()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                self.__pub_vel.publish(vel_end)
                self.__rate.sleep()

    def hatrack_docking(self):
        print("TODO")
        #TODO

    def _cart_docking(self,cart_name,forward_dist):
        cart_dock   = cart_docking() # NO slash
        cart_dock.start(cart_name,forward_dist)
        rospy.sleep(1)
        self.__client_cart_dock.wait_for_server()
        goal = DockingGoal()
        self.__client_cart_dock.send_goal(goal)
        self.__client_cart_dock.wait_for_result()
        res = self.__client_cart_dock.get_result()
        cart_dock.stop()
        print("docking result : ",res)
        return res.result

    def _cart_attach_srv(self, cmd_type, currents = [2.0, 2.0] ):
        try:
            print( cmd_type == "open", cmd_type =="close", currents[0], currents[1])
            res = self.__cart_attach(cmd_type == "open", cmd_type =="close", currents[0], currents[1])            
            print("Res", res)

            if res.ok:
                print("ciao:: ",cmd_type)
            return res.ok
        except :
            print (" Sweepe unexpected error while calling the %s attach service: %s"  % (cmd_type, sys.exc_info()[0], ))
            return False

    def shutdown(self):
        print("Closing sweepee")
        self.move_base_action.cancel_all_goals()
        self.__client_cart_dock.cancel_all_goals()
        self.line_both_caller.stop()
        vel_msg = Twist()
        t_end = time.time() + 0.5
        while time.time() < t_end:
            self.__pub_vel.publish(vel_msg)
            self.__rate.sleep()
        self.__pub_vel.unregister()
        print("Closed sweepee")


    def status_update(self, floor =  [], position = [], docked_obj = [] ):
        if (floor or floor==0):
            self.floor              = floor
        if position    :    self.position   = position
        if docked_obj  :    self.docked_obj = docked_obj
        rospy.set_param("/sweepee/status/floor",self.floor)
        rospy.set_param("/sweepee/status/position",self.position)
        rospy.set_param("/sweepee/status/docked_obj",self.docked_obj)
        
    def get_status( self ):
        return rospy.get_param("/sweepee/status" )

    def _get_sweepee_pos(self):
        rot   = []
        trans = []
        while rot == [] and not rospy.is_shutdown():
            self.__rate.sleep()
            try:
                (trans,rot) = self.listener.lookupTransform('/map_sweepee','/sweepee/base_footprint', rospy.Time(0))
            except:
                rospy.logerr("Cannot retrieve tf")
                continue 
            
        return [trans[0],trans[1],rot[2],rot[3]]
      
        
    def pub_no_vel(self):
        vel_msg = Twist()
        t_end = time.time() + 1
        while time.time() < t_end:
            vel_msg.linear.x = 0.0
            self.__pub_vel.publish(vel_msg)
            self.__rate.sleep()
    
    def _dock_here(self,cart_name = "popeye"):
        result = True
        cart_name = raw_input("insert cart name\n")
        if cart_name in ["popeyeup","oliviaup","blutoup"]:
            cart_name=cart_name[:-2]
            print("docking up!!  :",cart_name)
            result = self.usb_tag_dock.start(up=True)                                   if result else False
            
        else:
            print("docking down!!  :",cart_name)
            result = self.usb_tag_dock.start(up=False)                                   if result else False
        print("cart name: ",cart_name)
        rospy.sleep(2)
        for_dist = rospy.get_param("/" + cart_name + "/param/forward_dist")
        result = self._cart_docking(cart_name,for_dist)                      if result else False
        print ("Docking algorithm result res:",result)
        result = self.usb_tag_dock.stop()                                    if result else False
        currs = rospy.get_param("/" + cart_name + "/param/docking_currents")
        # result = self._cart_attach_srv("open",currs)                         if result else False
        rospy.sleep(1)
        return result
    
    def __level_zero_task_start_begin(self,event=[]):#TODO chiamala da qualche parte
        if self.floor != 0:
            return False
        result = True
        floor = rospy.get_param("/sweepee/status/floor")
        result = self.change_map_srv(floor)     if result else False
        result = self.initial_pose_sweepee_0()  if result else False
        result = self.relocalize_amcl_srv()     if result else False
        return result


    def __level_zero_task_dock(self,cart_name,pos):
        result = True
        result = self.usb_tag_dock.start()                                   if result else False
        rospy.sleep(4)
        result = self.sweepee_movebase_client(pos)                           if result else False
        for_dist = rospy.get_param("/" + cart_name + "/param/forward_dist")
        result = self._cart_docking(cart_name,for_dist)                      if result else False
        result = self.usb_tag_dock.stop()                                    if result else False
        currs = rospy.get_param("/" + cart_name + "/param/docking_currents")
        result = self._cart_attach_srv("open",currs)                         if result else False
        self.status_update(docked_obj=cart_name)
        rospy.sleep(1)
        result = self.sweepee_move_forw(-2.5, 0.3)                           if result else False
        result = self.sweepee_move_yaw(math.pi/2.0, 0.3)                     if result else False
        return result

    def __level_zero_task_goelev_for1(self,event=[]):
        result = True

        pos_goal = rospy.get_param("/sweepee/goal/front_elev_0")
        result = self.sweepee_movebase_client(pos_goal)             if result else False
        # pos_goal = rospy.get_param("/sweepee/goal/elev_0")
        # result = self.sweepee_movebase_client(pos_goal)             if result else False
        result = self.sweepee_move_forw(1.6, 0.15)                 if result else False
        result = self.sweepee_move_yaw(-(math.pi/2.0), 0.3)         if result else False
        return result

    def __level_zero_task_goelev_for2(self,event=[]):
        result = True
        pos_goal = rospy.get_param("/sweepee/goal/front_elev_0")
        result = self.sweepee_movebase_client(pos_goal)                if result else False
        # pos_goal = rospy.get_param("/sweepee/goal/elev_0")
        # result = self.sweepee_movebase_client(pos_goal)                if result else False
        result = self.sweepee_move_forw(1.45, 0.15)                  if result else False
        result = self.sweepee_move_yaw(-(math.pi/2.0), 0.25)         if result else False
        return result

    def _level_zero_task_return(self,event=[]):
        result = True
        result = self.change_map_srv(0)                                    if result else False
        result = self.initial_pose_sweepee_0_return()                      if result else False  
        result = self.relocalize_amcl_srv()                                if result else False
        pos_goal = rospy.get_param("/sweepee/goal/elev_exit_0")
        result = self.sweepee_movebase_client(pos_goal)                    if result else False 
        result = self.sweepee_move_yaw((math.pi/2.0), 0.3)         if result else False
        pos_goal = rospy.get_param("/sweepee/goal/home_back_0")
        result = self.sweepee_movebase_client(pos_goal)                    if result else False
        self.status_update(position = self._get_sweepee_pos())
        return True

    def _level_one_task_enter(self,event=[]):
        result = True
        result = self.change_map_srv(1)                                         if result else False
        result = self.initial_pose_sweepee_1()                                  if result else False
        result = self.sweepee_move_forw(2.8, 0.3)                               if result else False
        rospy.set_param("/sweepee/cargo/safe",True)
        self.relocalize_amcl_srv()                                              if result else False
        result = self.sweepee_move_yaw((math.pi)/2, 0.2)                        if result else False
        self.relocalize_amcl_srv()                                              if result else False
        pos_goal = rospy.get_param("/sweepee/goal/cargo_mid_1")
        result = self.sweepee_movebase_client(pos_goal)                         if result else False
        self.relocalize_amcl_srv()                                              if result else False
        result = self.sweepee_move_yaw(-(math.pi)/2, 0.2)                       if result else False
        self.relocalize_amcl_srv()                                              if result else False
        pos_goal = rospy.get_param("/sweepee/goal/cargo_ass_2")    #TODO
        result = self.sweepee_movebase_client(pos_goal)                         if result else False
        result = self.sweepee_move_yaw(-(math.pi)/2, 0.2)                       if result else False
        rospy.sleep(0.5)
        # result = self.dock_cargo()                                              if result else False #TODO controlla pd
        if result:
            self.status_update(floor=1,position=self._get_sweepee_pos())
        return result

    def _level_one_task_exit(self,event=[]):
        result = True
        result = self.sweepee_move_yaw(-(math.pi/2.0), 0.2)                     if result else False
        self.relocalize_amcl_srv()                                              if result else False
        pos_goal = rospy.get_param("/sweepee/goal/cargo_mid_exit_1")
        result = self.sweepee_movebase_client(pos_goal)                         if result else False
        result = self.sweepee_move_yaw((math.pi)/2, 0.2)                        if result else False
        self.relocalize_amcl_srv()                                              if result else False
        try:
            self.dynrec.update_configuration({"xy_goal_tolerance":0.05})
        except Exception as e: print(e)
        pos_goal = rospy.get_param("/sweepee/goal/cargo_elev_exit_1")
        result = self.sweepee_movebase_client(pos_goal)                         if result else False
        try:
            self.dynrec.update_configuration({"xy_goal_tolerance":0.02})
        except Exception as e: print(e)
        result = self.sweepee_move_yaw(-(math.pi/2.0), 0.2)                     if result else False
        result = self.sweepee_move_forw(2.4, 0.3)                               if result else False
        result = self.sweepee_move_yaw((math.pi/2.0), 0.2)                      if result else False
        if result:
            self.status_update(position=self._get_sweepee_pos())
        return result


    def _level_two_enter(self,event=[]):
        result = True
        result = self.change_map_srv(2)
        self.initial_pose_sweepee_2()
        result = self.sweepee_move_yaw((math.pi/2.2), 0.25)               if result else False
        result = self.sweepee_move_forw(1.80, 0.3)                        if result else False
        result = self.sweepee_move_yaw(-(math.pi/2.0), 0.2)               if result else False

        self.relocalize_amcl_srv()
        pos_goal = rospy.get_param("/sweepee/goal/door_front_2")
        result = self.sweepee_movebase_client(pos_goal)                   if result else False
        result = self.sweepee_move_yaw((math.pi/2.0), 0.2)                if result else False
        self.relocalize_amcl_srv()                                        if result else False
        result = self.door_docking_entry()                                if result else False
        result = self.sweepee_move_forw(2.4, 0.2)                         if result else False
        if result:
            self.status_update(floor=2,position=self._get_sweepee_pos())
        return result
    
    def _level_two_leave(self,target_pos ,cart, release=False , go_back=False):
        result = True    
        self.__clearmap()
        result = self.sweepee_movebase_client(target_pos)                           if result else False
        result = self.sweepee_move_forw(-0.03,0.05)
        result = self.sweepee_move_lat(-0.1, 0.05)
        if release:
            result = self._cart_attach_srv("close")                                 if result else False
            self.status_update(docked_obj = "none")
        if go_back:
            result = self.sweepee_move_forw(-1.2,0.2)                               if result else False
            if cart=="bluto":
                result = self.sweepee_move_lat(0.8, 0.2)                               if result else False
            result = self.sweepee_move_yaw(math.pi,0.2)                         if result else False
            
            self.status_update(docked_obj="none")
        self.status_update(position=self._get_sweepee_pos())
        return result


    def _level_two_leave_popeye(self):
        result = True    
        result = self.sweepee_move_forw(0.2,0.2)                               if result else False
        result = self.sweepee_move_yaw(math.pi/2.0, 0.2)                        if result else False
        result = self.sweepee_move_forw(2.7,0.2)                                if result else False
        result = self.sweepee_move_yaw(-(math.pi/2.0), 0.2)                     if result else False
        result = self.dock_popeye_call.start()                                  if result else False
        rospy.set_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/status","none")
        time_watch = time.time()
        while True and not rospy.is_shutdown():
            rospy.sleep(0.5)
            if not rospy.has_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/status"):
                continue
            elif rospy.get_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/status") == "dock_popeye_done" :
                break
            elif (time.time() - time_watch) > 25.0 :
                print("Time exceedee for popeye docking")
                return False
            
        rospy.set_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/status","none")
        time_watch = time.time()
        while True and not rospy.is_shutdown():
            rospy.sleep(0.5)
            if not rospy.has_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/status"):
                continue
            elif rospy.get_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/status") == "dock_popeye_done" :
                break
            elif (time.time() - time_watch) > 25.0 :
                print("Time exceedee for popeye docking")
                return False
        result = self.dock_popeye_call.stop()                                     if result else False
        result = self._cart_attach_srv("close")                                   if result else False
        self.status_update(position=self._get_sweepee_pos())
        print(result)
        return result
        

    def __level_two_task_dock(self,cart_name, event=[]):#TODO ass 1 or 2
        result = True
        result = self.usb_tag_dock.start()                                     if result else False
        rospy.sleep(4)
        dock_pos = rospy.get_param("/" + cart_name + "/goal/fuse_1_dock")#TODO x ass 2
        for_dist = rospy.get_param("/" + cart_name + "/param/forward_dist")

        result = self.sweepee_movebase_client(dock_pos)                        if result else False
        result = self._cart_docking(cart_name,for_dist)                        if result else False
        result = self.usb_tag_dock.stop()                                               if result else False
        currs = rospy.get_param("/" + cart_name + "/param/docking_currents")
        result = self._cart_attach_srv("open",currs)                           if result else False
        self.status_update(docked_obj=cart_name)
        rospy.sleep(1)
        #TODO 2 volte per 2 posizioni
        return result
    

    def _level_two_exit_to_elev(self,event=[]):
        result = True
        # IDENTIFICA IL CARELLO
        
        # if event== "cartpanel1": #TODO se sotto carrello o carllello vicino sponda
        #     result = self.sweepee_move_forw(-1.0,0.2)                           if result else False
        #     result = self.sweepee_move_lat(0.5, 0.2)                            if result else False
        # result = self.sweepee_move_yaw(math.pi, 0.3)                            if result else False
        # self.relocalize_amcl_srv()
        pose_goal = rospy.get_param("/sweepee/goal/door_exit_2")
        result = self.sweepee_movebase_client(pose_goal)                        if result else False
        result = self.door_docking_exit()                                       if result else False
        result = self.sweepee_move_forw(2.5, 0.2)                               if result else False
        result = self.sweepee_move_yaw(-(math.pi/2.0), 0.3)                     if result else False
        pose_goal = rospy.get_param("/sweepee/goal/exit_2")
        result = self.sweepee_movebase_client(pose_goal)                        if result else False
        result = self.sweepee_move_yaw(math.pi/2.0, 0.2)                        if result else False
        result = self.sweepee_move_forw( 2.0, 0.2)                              if result else False
        return result


    def _task_dock(self,level, cart_name,pos,event=[]):
        if level == 0:
            result = self.__level_zero_task_dock(cart_name,pos)
        elif level == 2:
            result = self.__level_two_task_dock(cart_name,pos)
        else:
            rospy.logerr("The docking for level " + str(level) + "is not possible" )
            return False
        if result:
            self.status_update(position=self._get_sweepee_pos(),docked_obj=cart_name)
        return result
        
        
    def _level_zero_task_goelev(self,level, event=[]):
        if level == 1:
            result = self.__level_zero_task_goelev_for1()
        elif level == 2:
            result =  self.__level_zero_task_goelev_for2()
        else:
            rospy.logerr("Moving to elev " + str(level) + "is not possible" )
            return False
        if result :
            self.status_update(position=self._get_sweepee_pos())
        return result 
    


