import sys
import logging
import time

from opcua import Client
from opcua import ua

goLevel1='ns=3;s="Extern_Com"."Lift_level_1"."Request_OPEN"'
goLevel2='ns=3;s="Extern_Com"."Lift_level_2"."Request_OPEN"'
goLevel3='ns=3;s="Extern_Com"."Lift_level_3"."Request_OPEN"'
doorLevel1='ns=3;s="Extern_Com"."Lift_level_1"."IS_OPENED"'
doorLevel2='ns=3;s="Extern_Com"."Lift_level_2"."IS_OPENED"'
doorLevel3='ns=3;s="Extern_Com"."Lift_level_3"."IS_OPENED"'


if __name__ == "__main__":
    logging.basicConfig(level=logging.WARN)

    client = Client("opc.tcp://10.43.49.21:4840")
    # client = Client("opc.tcp://admin@localhost:4840/freeopcua/server/") #connect using a user
    try:
        client.connect()
        print("Connected")
        #client.load_type_definitions()  # load definition of server specific structures/extension objects

        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        root = client.get_root_node()
        print("Root node is: ", root)
        objects = client.get_objects_node()
        print("Objects node is: ", objects)

        # Node objects have methods to read and write node attributes as well as browse or populate address space
        print("Children of root are: ", root.get_children())

        # get a specific node knowing its node id
        #var = client.get_node(ua.NodeId(1002, 2))
        var = client.get_node(goLevel3)
        print("Var is: ",var)
        #var.get_data_value() # get value of node as a DataValue object
        val=var.get_value() # get value of node as a python builtin
        print("Val is: ",val)

        #var.set_attribute(ua.AttributeIds.Value, ua.DataValue(True))
        var.set_data_value(ua.DataValue(ua.Variant(True, ua.VariantType.Boolean)))# set node value using implicit data type
        print("Val changed to: True")


    finally:
        client.disconnect()
        print("Disconnected")
