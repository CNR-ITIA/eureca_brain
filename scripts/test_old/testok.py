#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  5 14:36:31 2019

@author: kolmogorov
"""



import roslaunch
import rospy
from roslaunch import remoteprocess,remote
import os

os.environ["ROSLAUNCH_SSH_UNKNOWN"] = "1"

rospy.init_node('test')
rate = rospy.Rate(50) # 10hz
uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)        

master_re = roslaunch.core.Master(type_="rosmaster",uri="http://10.42.0.1:11311/")


sweepee_machine = roslaunch.core.Machine("pi", "10.42.0.62",env_loader="/home/pi/env.sh" , user="pi", password="pi")
node1 = roslaunch.core.Node("rostopic","rostopic",name="rostopic_pi",machine_name="pi",output="screen",args=str('pub -r 10') + str(' /cmd_vel std_msgs/String ') + str('ciao'))

launch = roslaunch.scriptapi.ROSLaunch()
parent = roslaunch.parent.ROSLaunchParent(uuid, [], is_core=False)

base_conf = roslaunch.config.ROSLaunchConfig();
base_conf.add_machine(sweepee_machine)
base_conf.add_node(node1)
base_conf.assign_machines()
base_conf.set_master(master_re)


# print(base_conf.summary())
parent.config = base_conf
launch.parent = parent

launch.start()
rospy.sleep(15)
launch.stop()