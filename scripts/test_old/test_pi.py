#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 27 17:46:11 2019

@author: kolmogorov
"""

import roslaunch
import rospy
from roslaunch import remoteprocess,remote
import os

os.environ["ROSLAUNCH_SSH_UNKNOWN"] = "1"

rospy.init_node('test')
rate = rospy.Rate(50) # 10hz
uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)        

master_re = roslaunch.core.Master(type_="rosmaster",uri="http://10.42.0.1:11311/")


sweepee_machine = roslaunch.core.Machine("pi", "10.42.0.62",env_loader="/home/pi/env.sh" , user="pi", password="pi")
node1 = roslaunch.core.Node("rostopic","rostopic",name="rostopic_pi",machine_name="pi",output="screen",args=str('pub -r 10') + str(' /cmd_vel std_msgs/String ') + str('ciao'))

launch = roslaunch.scriptapi.ROSLaunch()

# launch.start()

# Start another node

listener = roslaunch.pmon.ProcessListener()

base_pm   = roslaunch.pmon.ProcessMonitor();
base_pm.add_process_listener(listener)

base_conf = roslaunch.config.ROSLaunchConfig();
base_conf.add_machine(sweepee_machine)
base_conf.add_node(node1)
base_conf.assign_machines()
base_conf.set_master(master_re)

base_ser  = roslaunch.server.ROSLaunchParentNode(base_conf,base_pm)
base_ser.start()

pd = roslaunch.remote.ROSRemoteRunner(run_id=uuid, rosconfig=base_conf, pm=base_pm ,server=base_ser)
pd.add_process_listener(listener)

launch.parent = base_ser# roslaunch.parent.ROSLaunchParent(uuid, [])

# launch.parent.remote_runner = pd
launch.start()
# launch.launch(node1)
launch.spin()
# parent = roslaunch.parent.ROSLaunchParent(uuid, [], is_core=False)


# parent.start()
# pd.start_children()
# pd.launch_remote_nodes()
print("launched")
rospy.sleep(10)


# base_pm.shutdown()
# base_ser.shutdown("stop")

# base_pm.kill_process("rostopic_pi")
print("stopped")
rospy.sleep(10)
# parent.shutdown()
# base_ser.shutdown("closing")
rospy.sleep(10)
# child.start()
# sweepee_remote = roslaunch.remoteprocess.SSHChildROSLaunchProcess(run_id=uuid, name="test_remote", server_uri="http://marie:45549/", machine=sweepee_machine,master_uri="http://172.31.1.102:11311/")
# sweepee_remote.start();


    # launch = roslaunch.parent.ROSLaunchParent
    # print("PD2")
    # node1 = roslaunch.core.Node("turtlebot3_teleop","turtlebot3_teleop_key",name="remote_try",machine_name="eureca_nuc",output="screen")
    # print("PD3")
    # base_conf = roslaunch.config.ROSLaunchConfig();
    # base_pm   = roslaunch.pmon.ProcessMonitor();
    # base_ser  = roslaunch.server.ROSLaunchParentNode(base_conf,base_pm)
    # pd = roslaunch.remote.ROSRemoteRunner(run_id=uuid, rosconfig=base_conf, pm=base_pm ,server=sweepee_remote)
    # pd.launch_remote_nodes()
    # pd.start_children()
# print("PD4")
# rospy.spin()



# launch = roslaunch.scriptapi.ROSLaunch()
# launch.start()

# process = launch.launch(node1)
# print process.is_alive()
# rospy.spin()
# process.stop()

# base_conf = roslaunch.config.ROSLaunchConfig();
# base_pm   = roslaunch.pmon.ProcessMonitor();
# base_ser  = roslaunch.server.ROSLaunchParentNode(base_conf,base_p 0m)
# pd = roslaunch.remote.ROSRemoteRunner(run_id=uuid, rosconfig=base_conf, pm=base_pm ,server=base_ser)

# launch = roslaunch.parent.ROSLaunchParent(uuid, ["/home/abaco/eureca/esim_ws/src/eureca_task_manager/launch/vision_solo.launch"])
# launch.start()

# roslaunch.core.Node(package, node_type, name=None, namespace='/', 
#                     machine_name=None, args='', 
#                     respawn=False, respawn_delay=0.0, 
#                     remap_args=None, env_args=None, output=None, cwd=None, 
#                     launch_prefix=None, required=False, filename='<unknown>')


# launch = roslaunch.scriptapi.ROSLaunch()
# launch.start()

# process = launch.launch(node)
# print process.is_alive()
# process.stop()
        