#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 27 17:46:11 2019

@author: kolmogorov
"""

import roslaunch
import rospy
from roslaunch import remoteprocess,remote
import os

os.environ["ROSLAUNCH_SSH_UNKNOWN"] = "1"

rospy.init_node('test')
rate = rospy.Rate(50) # 10hz
uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)        
roslaunch.configure_logging(uuid)

launch = roslaunch.parent.ROSLaunchParent(uuid, ["/home/stade/brain/src/eureca_brain/launch/remotes_lauch/docking.launch"])
launch.start()
rospy.loginfo("started")

rospy.sleep(30)
# 3 seconds later
launch.shutdown()

