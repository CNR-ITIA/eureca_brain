#!/usr/bin/env python

import rospy
import actionlib
import math
import sys
import logging
import time
import roslaunch
import signal
import os
from subprocess import Popen, PIPE, STDOUT
from roslaunch import remoteprocess,remote
from opcua import Client
from opcua import ua
from std_msgs.msg import Bool
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from interface.msg import DockingAction, DockingGoal
from interface.srv import *
from std_srvs.srv import Empty, EmptyRequest
from laser_line_extraction.msg import LineSegmentList, LineSegment
from sensor_msgs.msg import LaserScan
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point,PoseWithCovarianceStamped
# from tf.transformations import quaternion_from_euler
from threading import Thread, Lock
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from map_server.srv import LoadMap

os.environ["ROSLAUNCH_SSH_UNKNOWN"] = "1"

mutex   = Lock()
mutex_L = Lock()

midPx          = 0.0
midPy          = 0.0
rot_dock_ex    = 0.0
gap_dock_ex    = 0.0
coeffM         = 0.0
coeffM_cargo   = 0.0
coeffM_cargo_t = 0.0
laser_msg = LineSegmentList()

w_vel_sweepee  = 0.0
w_vel_turtle   = 0.0
x_odom_sweepee = 0.0
y_odom_sweepee = 0.0
x_odom_turtle  = 0.0

goLevel0='ns=3;s="Extern_Com"."Lift_level_1"."Request_OPEN"'
goLevel1='ns=3;s="Extern_Com"."Lift_level_2"."Request_OPEN"'
goLevel2='ns=3;s="Extern_Com"."Lift_level_3"."Request_OPEN"'
doorLevel0='ns=3;s="Extern_Com"."Lift_level_1"."IS_OPENED"'
doorLevel1='ns=3;s="Extern_Com"."Lift_level_2"."IS_OPENED"'
doorLevel2='ns=3;s="Extern_Com"."Lift_level_3"."IS_OPENED"'

# POSITIONS
#(pose.x,pose.y,quat.z,quat.w)
Elev_turtle         = (6.37, -0.0, 0.0  , 1.0)
turtle_home_1       = (0.29, 0.11, 0.0  , 1.0)
pos_mid_turtle      = (1.18, 5.3, 0.707, 0.707)
pos_mid_turtle_turn = (1.48, 5.3,  1.0,   1.0)
Fin_pos_turtle      = (3.71, 5.34, 0.0  , 1.0)

sweepee_elev_0     = (  6.10,  0    , -0.707, 0.707)
sweepee_home_1     = (  0.00,  0    ,  0.0  , 1.0  )
sweepee_mid_cargo  = (  0.90,  5.35 ,  0.0  , 1.0  )
sweepee_ass_cargo  = (  3.88,  5.35 , -0.707, 0.707)
sweepee_mid_exitcTODO  = (  0.90,  5.35 ,  0.0  , 1.0  )
sweepee_home_2     = (-2.194, -3.426,  0.0  , 1.0  )
sweepee_door_front = ( 3.142, -3.528,  0.707, 0.707)
sweepee_kuka_fuse  = ( 0.005,  0.102,  0.707, 0.707)
sweepee_panel_fuse = ( 0.905,  0.102,  0.707, 0.707)
sweepee_door_exit  = ( 3.037, -1.324, -0.707, 0.707)
sweepee_door_out   = ( 3.119, -3.467, -0.707, 0.707)

sweepee_final_2    = (-2.194, -3.426,  1.0  , 0.0  )

Door_front        = (-1.897, -3.098,  0.668, 0.745)
Panel_pose        = (-3.639, -0.040,  0.616, 0.787)
Home              = (-4.001, -3.038,  0    , 1)


def signal_handler(signal, frame):
  sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

#INITIAL POSES FOR AMCL

def initial_pose_turtle_0():
    rospy.set_param('/amcl/initial_pose_x',  0.652)
    rospy.set_param('/amcl/initial_pose_y', -0.278)
    rospy.set_param('/amcl/initial_pose_a',  0.0)

def initial_pose_turtle_1():#controlla uscita acensore e fai in modo arrivi qui
    rospy.set_param('/amcl/initial_pose_x',  0.29)
    rospy.set_param('/amcl/initial_pose_y',  0.11)
    rospy.set_param('/amcl/initial_pose_a',  0.0)

def initial_pose_sweepee_0():
    rospy.set_param('/sweepee/amcl/initial_pose_x',  0.0)
    rospy.set_param('/sweepee/amcl/initial_pose_y',  0.0)
    rospy.set_param('/sweepee/amcl/initial_pose_a',  0.0)

def initial_pose_sweepee_1():
    rospy.set_param('/sweepee/amcl/initial_pose_x',  0.0)
    rospy.set_param('/sweepee/amcl/initial_pose_y',  0.0)
    rospy.set_param('/sweepee/amcl/initial_pose_a',  0.0)

# MOVEMENTS

def turtle_move_forw(dist, vel_pub, vel,rate):
    dist_made = 0.0
    vel_msg = Twist()
    if dist < 0.0:
        vel = -1.0 * vel
    vel_msg.linear.x = vel
    time_now = time.time()
    try:
        while math.fabs(dist_made) < math.fabs(dist):
            vel_pub.publish(vel_msg)
            dist_made = dist_made + x_odom_turtle * (time.time() - time_now)
            time_now = time.time()
            rate.sleep()
        t_end = time.time() + 1
        while time.time() < t_end:
            vel_msg.linear.x = 0.0
            vel_pub.publish(vel_msg)
            rate.sleep()
    except KeyboardInterrupt:
        t_end = time.time() + 1
        while time.time() < t_end:
            vel_msg.linear.x = 0.0
            vel_pub.publish(vel_msg)
            rate.sleep()
        return True
    return True

def turtle_move_yaw(dist, vel_pub, vel,rate):
    dist_made = 0.0
    vel_msg = Twist()
    if dist < 0.0:
        vel = -1.0 * vel
    vel_msg.angular.z = vel
    time_now = time.time()
    try:
        while math.fabs(dist_made) < math.fabs(dist):
            vel_pub.publish(vel_msg)
            dist_made = dist_made + w_vel_turtle * (time.time() - time_now)
            time_now = time.time()
            rate.sleep()
        t_end = time.time() + 1
        while time.time() < t_end:
            vel_msg.angular.z = 0.0
            vel_pub.publish(vel_msg)
            rate.sleep()
    except KeyboardInterrupt:
        t_end = time.time() + 1
        while time.time() < t_end:
            vel_msg.angular.z = 0.0
            vel_pub.publish(vel_msg)
            rate.sleep()
        return True
    return True

def sweepee_move_forw(dist, vel_pub, vel,rate):
    dist_made = 0.0
    vel_msg = Twist()
    if dist < 0.0:
        vel = -1.0 * vel
    vel_msg.linear.x = vel
    time_now = time.time()
    try:
        while math.fabs(dist_made) < math.fabs(dist):
            vel_pub.publish(vel_msg)
            dist_made = dist_made + x_odom_sweepee * (time.time() - time_now)
            time_now = time.time()
            rate.sleep()
        t_end = time.time() + 0.2
        while time.time() < t_end:
            vel_msg.linear.x = 0.0
            vel_pub.publish(vel_msg)
            rate.sleep()
    except KeyboardInterrupt:
        t_end = time.time() + 1
        while time.time() < t_end:
            vel_msg.linear.x = 0.0
            vel_pub.publish(vel_msg)
            rate.sleep()
        return True
    return True

def sweepee_move_lat(dist, vel_pub, vel,rate):
    dist_made = 0.0
    vel_msg = Twist()
    if dist < 0.0:
        vel = -1.0 * vel
    vel_msg.linear.y = vel
    time_now = time.time()
    try:
        while math.fabs(dist_made) < math.fabs(dist):
            vel_pub.publish(vel_msg)
            dist_made = dist_made + y_odom_sweepee * (time.time() - time_now)
            time_now = time.time()
            rate.sleep()
        t_end = time.time() + 0.2
        while time.time() < t_end:
            vel_msg.linear.y = 0.0
            vel_pub.publish(vel_msg)
            rate.sleep()
    except KeyboardInterrupt:
        t_end = time.time() + 1
        while time.time() < t_end:
            vel_msg.linear.y = 0.0
            vel_pub.publish(vel_msg)
            rate.sleep()
        return True
    return True

def sweepee_move_yaw(dist, vel_pub, vel,rate):
    dist_made = 0.0
    vel_msg = Twist()
    if dist < 0.0:
        vel = -1.0 * vel
    vel_msg.angular.z = vel
    time_now = time.time()
    try:
        while math.fabs(dist_made) < math.fabs(dist):
            vel_pub.publish(vel_msg)
            dist_made = dist_made + w_vel_sweepee * (time.time() - time_now)
            time_now = time.time()
            rate.sleep()
        t_end = time.time() + 0.2
        while time.time() < t_end:
            vel_msg.angular.z = 0.0
            vel_pub.publish(vel_msg)
            rate.sleep()
    except KeyboardInterrupt:
        t_end = time.time() + 1
        while time.time() < t_end:
            vel_msg.angular.z = 0.0
            vel_pub.publish(vel_msg)
            rate.sleep()
        return True
    return True

def sweepee_movebase_client(client_a, pos_in):
    try:
        while True:
            client_a.wait_for_server()
            goal = MoveBaseGoal()
            goal.target_pose.header.frame_id = "map_sweepee"
            print("moving to : ",pos_in[0], pos_in[1], pos_in[2], pos_in[3])
            goal.target_pose.header.stamp = rospy.Time.now()
            goal.target_pose.pose.position.x = pos_in[0]
            goal.target_pose.pose.position.y = pos_in[1]
            goal.target_pose.pose.orientation.z = pos_in[2]
            goal.target_pose.pose.orientation.w = pos_in[3]

            client_a.send_goal(goal)
            wait = client_a.wait_for_result()
            if not wait:
                rospy.logerr("Action server not available!")
                rospy.signal_shutdown("Action server not available!")
            else:
                return client_a.get_result()
    except KeyboardInterrupt:
        return True

def turtle_movebase_client(client_a, pos_in):
    try:
        while True:
            client_a.wait_for_server()
            goal = MoveBaseGoal()
            goal.target_pose.header.frame_id = "map_turtle"
            print("moving to : ",pos_in[0], pos_in[1], pos_in[2], pos_in[3])
            goal.target_pose.header.stamp = rospy.Time.now()
            goal.target_pose.pose.position.x = pos_in[0]
            goal.target_pose.pose.position.y = pos_in[1]
            goal.target_pose.pose.orientation.z = pos_in[2]
            goal.target_pose.pose.orientation.w = pos_in[3]

            client_a.send_goal(goal)
            wait = client_a.wait_for_result()
            if not wait:
                rospy.logerr("Action server not available!")
                rospy.signal_shutdown("Action server not available!")
            else:
                return client_a.get_result()
    except KeyboardInterrupt:
        return True


def Lascallback_door(data):
    global midPx
    global midPy
    global coeffM
    global coeffM_cargo
    Points = []
    Plen = []
    for lines in data.line_segments:
        if abs(lines.angle) > 0.3:
            continue
        Points.append(lines.start)
        Points.append(lines.end)
        Plen.append(math.sqrt(lines.start[0] * lines.start[0] + lines.start[1] * lines.start[1]))
        Plen.append(math.sqrt(lines.end[0] * lines.end[0] + lines.end[1] * lines.end[1]))
    max1, max2 = range_lis(Plen)
    point1 = Points[Plen.index(max1)]
    point2 = Points[Plen.index(max2)]
    mutex.acquire()
    midPx = (point2[0] + point1[0])/2.0
    midPy = (point2[1] + point1[1])/2.0
    coeffM = -1/((point2[1] - point1[1])/(point2[0] - point1[0]))
    coeffM_cargo = -1/coeffM
    mutex.release()

def Lascallback_cargo(data):
    global midPx
    global midPy
    global coeffM
    global coeffM_cargo
    Points = []
    Plen = []
    for lines in data.line_segments:
        Points.append(lines.start)
        Points.append(lines.end)
        Plen.append(math.sqrt(lines.start[0] * lines.start[0] + lines.start[1] * lines.start[1]))
        Plen.append(math.sqrt(lines.end[0] * lines.end[0] + lines.end[1] * lines.end[1]))
    max1, max2 = range_lis(Plen)
    point1 = Points[Plen.index(max1)]
    point2 = Points[Plen.index(max2)]
    mutex.acquire()
    midPx = (point2[0] + point1[0])/2.0
    midPy = (point2[1] + point1[1])/2.0
    coeffM_cargo = (point2[1] - point1[1])/(point2[0] - point1[0])
    mutex.release()

def Lascallback_turtle(data):
    global coeffM_cargo_t
    Points = []
    Plen = []
    for lines in data.line_segments:
        Points.append(lines.start)
        Points.append(lines.end)
        # Plen.append(math.sqrt(lines.start[0] * lines.start[0] + lines.start[1] * lines.start[1]))
        # Plen.append(math.sqrt(lines.end[0] * lines.end[0] + lines.end[1] * lines.end[1]))
        Plen.append(lines.start[0])
        Plen.append(lines.start[1])
        Plen.append(lines.end[0])
        Plen.append(lines.end[1])
    print Plen
    # max1= range_lis(Plen)
    # point1 = Points[Plen.index(max1)]
    # mutex.acquire()
    # midPx = (point2[0] + point1[0])/2.0
    # midPy = (point2[1] + point1[1])/2.0
    # coeffM = -1/((point2[1] - point1[1])/(point2[0] - point1[0]))
    # coeffM_cargo = -1/coeffM
    # mutex.release()

def turtle_callback_odom(data):
    global w_vel_turtle
    global x_odom_turtle
    x_odom_turtle = data.twist.twist.linear.x
    w_vel_turtle  = data.twist.twist.angular.z

def sweepee_callback_odom(data):
    global w_vel_sweepee
    global x_odom_sweepee
    global y_odom_sweepee
    x_odom_sweepee = data.twist.twist.linear.x
    y_odom_sweepee = data.twist.twist.linear.y
    w_vel_sweepee = data.twist.twist.angular.z

def ScanBack_door(data):
    global rot_dock_ex
    global gap_dock_ex
    aperture = 0.174533 * 4 # 10 deg * tot
    mid_p = int(((data.angle_max * 2)/data.angle_increment)/2)
    p_low = int(mid_p - aperture/data.angle_increment)
    p_hi  = int(mid_p + aperture/data.angle_increment)
    new_data   = data.ranges[p_low:p_hi]
    new_data_b = [1 if x < 2.0 else 0 for x in new_data  ]
    new_data_d = [x if x < 2.0 else 0 for x in new_data  ]
    left  = sum(new_data_b[:len(new_data_b)//2]) #25  porta non simmetrica , valore stimato
    right = sum(new_data_b[len(new_data_b)//2:])
    left_d  = sum(new_data_d[:len(new_data_d)//2])/left
    right_d = sum(new_data_d[len(new_data_d)//2:])/right
    # print(left,right)
    rot_dock_ex = (left - right) * 0.1
    gap_dock_ex = (left_d - right_d) 
    # print(left_d,right_d,gap_dock_ex)
    print(left,right,rot_dock_ex)
    
def change_map_srv(change_map,map_number):
    if map_number==0:
        path = "/home/stade/brain/src/dispatcher/maps/zero.yaml"
    elif map_number==1:
        path = "/home/stade/brain/src/dispatcher/maps/first.yaml"
    elif map_number==3:
        path = "/home/stade/brain/src/dispatcher/maps/first_t.yaml"
    elif map_number==2:
        path = "/home/stade/brain/src/dispatcher/maps/second.yaml"
    else:
        return False
    
    try:
        resp1 = change_map(path)
        return resp1.success
    except rospy.ROSInterruptException:
        rospy.loginfo("Maps switch failure.")
        return False

def move_elevator(goLevel_in):
    logging.basicConfig(level=logging.WARN)
    client = Client("opc.tcp://10.43.49.21:4840")
    # client = Client("opc.tcp://admin@localhost:4840/freeopcua/server/") #connect using a user
    try:
        client.connect()
        print("Connected")
        #client.load_type_definitions()  # load definition of server specific structures/extension objects

        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        root = client.get_root_node()
        print("Root node is: ", root)
        objects = client.get_objects_node()
        print("Objects node is: ", objects)

        # Node objects have methods to read and write node attributes as well as browse or populate address space
        print("Children of root are: ", root.get_children())

        # get a specific node knowing its node id
        #var = client.get_node(ua.NodeId(1002, 2))
        var = client.get_node(goLevel_in)
        print("Var is: ",var)
        #var.get_data_value() # get value of node as a DataValue object
        val=var.get_value() # get value of node as a python builtin
        print("Val is: ",val)

        #var.set_attribute(ua.AttributeIds.Value, ua.DataValue(True))
        var.set_data_value(ua.DataValue(ua.Variant(True, ua.VariantType.Boolean)))# set node value using implicit data type
        print("Val changed to: True")
        


    finally:
        client.disconnect()
        print("Disconnected")

def take_pic_turtle():
    p = Popen(['ssh', 'pi@pi'], stdout=PIPE, stdin=PIPE, stderr=STDOUT)    
    grep_stdout = p.communicate(input="python take_pic.py")[0]
    
def take_pic_sweepee():
    p = Popen(['ssh', 'abaco@tx2'], stdout=PIPE, stdin=PIPE, stderr=STDOUT)    
    grep_stdout = p.communicate(input="python DATI_STADE_LUGLIO/snap.py")[0]

# DOCKING

def range_lis(list1):
    lowest = list1[0]
    lowest2 = None
    for item in list1[1:]:
        if item < lowest:
            lowest2 = lowest
            lowest = item
        elif lowest2 == None or lowest2 > item:
            lowest2 = item
    return lowest, lowest2

def docking_client():
    client = actionlib.SimpleActionClient('docking', DockingAction)
    client.wait_for_server()
    goal = DockingGoal()
    client.send_goal(goal)
    client.wait_for_result()
    return client.get_result()

def cart_attach_client(cart_attach_in, open_c, close_c):
    try:
        resp1 = cart_attach_in(open_c, close_c)
        return resp1.ok
    except rospy.ROSInterruptException:
        rospy.loginfo("cart attach failure.")
        return False

def dock_cargo_sweepee(vel_pub):
    #TODO mutex
    try:
        sub_line = rospy.Subscriber("/line_segments", LineSegmentList, Lascallback_cargo, queue_size=1)
        rospy.wait_for_message("/line_segments",LineSegmentList)
        vel_msg = Twist()
        print coeffM_cargo
        while math.fabs(coeffM_cargo) > 0.005:
            print coeffM_cargo
            # vel_msg.linear.x = min(max(0.25 * (midPx - 0.8), -0.25), 0.25)
            # vel_msg.linear.y = min(max(0.25 * midPy , -0.25), 0.25)
            # vel_msg.angular.z = min(max(0.25 * coeffM_cargo, -0.25), 0.25)
            vel_msg.angular.z = min(max(0.4 * coeffM_cargo, -0.2), 0.2)
            vel_pub.publish(vel_msg)
            rate.sleep()
        return True
    finally:
        sub_line.unregister()
        vel_end = Twist()
        t_end = time.time() + 0.5
        while time.time() < t_end:
            vel_pub.publish(vel_end)
            rate.sleep()
        
def dock_cargo_turtle(vel_pub):
    #TODO mutex
    try:
        sub_line = rospy.Subscriber("/line_segments", LineSegmentList, Lascallback_turtle, queue_size=1)
        rospy.wait_for_message("/line_segments",LineSegmentList)
        vel_msg = Twist()
        print coeffM_cargo_t
        # while math.fabs(coeffM_cargo) > 0.005:
        while True:
            # print coeffM_cargo_t
            # vel_msg.linear.x = min(max(0.25 * (midPx - 0.8), -0.25), 0.25)
            # vel_msg.linear.y = min(max(0.25 * midPy , -0.25), 0.25)
            # vel_msg.angular.z = min(max(0.25 * coeffM_cargo, -0.25), 0.25)
            # vel_msg.angular.z = coeffM_cargo
            # vel_pub.publish(vel_msg)
            rate.sleep()
        return True
    finally:
        sub_line.unregister()
        vel_end = Twist()
        t_end = time.time() + 0.5
        while time.time() < t_end:
            vel_pub.publish(vel_end)
            rate.sleep()
        
def door_docking(vel_pub):
    global midPx
    global midPy
    global coeffM
    # lin_err = 10
    # ang_err = 10
    linesub = rospy.Subscriber("/line_segments", LineSegmentList, Lascallback_door)
    rospy.loginfo("Sub line extractor initialized.")
    rospy.wait_for_message("/line_segments", LineSegmentList)

    rate = rospy.Rate(30)
    vel_msg = Twist()
    while math.fabs(midPy) > 0.01 or math.fabs(coeffM) > 0.02:
        vel_msg.linear.x = min(max(0.25 * (midPx - 0.8), -0.25), 0.25)
        vel_msg.linear.y = min(max(0.25 * midPy , -0.25), 0.25)
        vel_msg.angular.z = min(max(0.25 * coeffM, -0.25), 0.25)
        vel_pub.publish(vel_msg)
        rate.sleep()
    rospy.loginfo("Door docking finished")
    linesub.unregister()
    return True

def door_docking_exit(vel_pub):
    global rot_dock_ex
    global gap_dock_ex
    linesub = rospy.Subscriber("sweepee/scan/front", LaserScan, ScanBack_door)
    rospy.wait_for_message("sweepee/scan/front", LaserScan)

    rate = rospy.Rate(30)
    vel_msg = Twist()
    while math.fabs(gap_dock_ex) > 0.03 or math.fabs(rot_dock_ex) > 0.02:
            vel_msg.angular.z = min(max(0.1 * rot_dock_ex, -0.02), 0.02)
            vel_msg.linear.y = min(max(0.2 * (gap_dock_ex) , -0.02), 0.02)
            vel_pub.publish(vel_msg)
            rate.sleep()

    rospy.loginfo("Door docking finished")
    linesub.unregister()
    return True        


if __name__ == '__main__':
    try:
        rospy.init_node('demo')
        rate = rospy.Rate(50) # 10hz
        rate_fast = rospy.Rate(500) # 10hz
        
        # OPCUA
        logging.basicConfig(level=logging.WARN)
        client = Client("opc.tcp://10.43.49.21:4840")
        
        #MOVE BASE ACTION CLIENT
        turtle_move_base = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
        sweepee_move_base = actionlib.SimpleActionClient('/sweepee/move_base', MoveBaseAction)
        rospy.loginfo("Move base client initialized.")
        
        #COMMAND VEL PUBLISHER
        pub_vel_turt    = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        # pub_vel_sweepee = rospy.Publisher('/cmd_vel_mux/input/docking', Twist, queue_size=1)
        pub_vel_sweepee = rospy.Publisher('/agv/command/velocity', Twist, queue_size=1)
        rospy.loginfo("Twist publisher initialized.")

        #SUBSCRIBER
        # rospy.wait_for_service('/sweepee_control/cart_attach')
        rospy.Subscriber("odom"         , Odometry, turtle_callback_odom)
        rospy.Subscriber("/sweepee/odom", Odometry, sweepee_callback_odom) #TODO accendi solo quando necessario
        
        #Create services clients
        # cart_attach = rospy.ServiceProxy('/sweepee_control/cart_attach', CartAttach)
        turtle_r_map = rospy.ServiceProxy('/turtle/move_base/clear_costmaps', Empty)
        sweepee_clear_map = rospy.ServiceProxy('/sweepee/move_base/clear_costmaps', Empty)
        turtle_change_map = rospy.ServiceProxy('/turtle/change_map', LoadMap)
        sweepee_change_map = rospy.ServiceProxy('/sweepee/change_map', LoadMap)
        
        #AMCL
        # pub_amcl_init = rospy.Publisher('/initialpose', PoseWithCovarianceStamped, queue_size=1)
        # init_turtle = PoseWithCovarianceStamped()
        # init_turtle.header.frame_id = "map"
        # init_turtle.header.stamp    = rospy.Time.now()
        # init_turtle.pose.pose.position.x = 1.76
        # init_turtle.pose.pose.position.y = -0.153
        # init_turtle.pose.pose.position.z = 0.0
        # init_turtle.pose.pose.orientation.x = 0.0
        # init_turtle.pose.pose.orientation.y = 0.0
        # init_turtle.pose.pose.orientation.z = 0.0
        # init_turtle.pose.pose.orientation.w = 1.0
        # print(init_turtle)
        # t_end = time.time() + 1
        # while time.time() < t_end:
        #     pub_amcl_init.publish(init_turtle)
        #     rate.sleep()
    

        print("START")
        
        
        
        # TURTLE LVL 0
        
        # initial_pose_turtle_0():
        # change_map_srv(turtle_change_map,0)
        # rospy.sleep(2.0)
        # result = movebase_client(client_move_base,Elev_turtle)
        # rospy.sleep(2.0)
        # result = move_yaw(-1.57, pub_vel_turt, 0.6,rate)
        # rospy.sleep(2.0)
        
        # TURTLE LVL 1
        
        # change_map_srv(turtle_change_map,3) #3 mappa modificata per turtle
        # result = move_forw(2.5, pub_vel_turt, 0.18,rate)
        
        # initial_pose_turtle_1()
        # result = turtle_move_yaw(1.57 , pub_vel_turt, 0.6,rate)
        # result = turtle_movebase_client(turtle_move_base,pos_mid_turtle)
        # result = turtle_move_yaw(-1.57 , pub_vel_turt, 0.6 , rate)
        
        #TODO docking
        # dock_cargo_turtle(pub_vel_turt) # finire
        # result = turtle_move_forw(2.10 , pub_vel_turt, 0.18, rate)
        #TODO uscita
        
        
        
        
        
        #SWEEPEE LVL 0
        #TODO docking carrello
        # change_map_srv(sweepee_change_map,0)
        # initial_pose_sweepee_0()
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_elev) # centro ascensore
        # move_elevator(goLevel1)
        # TODO safety
        
        #SWEEPEE LVL 1
        
        # change_map_srv(sweepee_change_map,1)
        # result = sweepee_move_forw(2.0, pub_vel_sweepee, 0.4,rate) # uscita ascensore
        # initial_pose_sweepee_1()
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_home_1) # zona home
        # result = sweepee_move_yaw((math.pi/2.0),pub_vel_sweepee, 0.3,rate)
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_mid_cargo) # zona home
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_ass_cargo) # zona assemblaggio cargo
        # rospy.sleep(1)
        dock_cargo_sweepee(pub_vel_sweepee)#LASER LINE BOTH LAUNCH

        #EXIT CARGO SWEEPEE

        # result = sweepee_move_yaw(-(math.pi/2.0),pub_vel_sweepee, 0.3,rate)
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_mid_exitc)#TODO
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_home_1) # zona home
        
        #TODO risalire ascensore
        
        #SWEEPEE LVL 2 KUKA IIWA
        
        # change_map_srv(sweepee_change_map,2)
        #TODO uscire acensore
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_home_2)    #TODO valutare se si o no o tchuss
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_door_front)

        # result = door_docking(pub_vel_sweepee)
        # result = sweepee_move_forw(2.0, pub_vel_sweepee, 0.2,rate) 
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_kuka_fuse)
        
        
        
        #SWEEPEE LVL 2 PANEL CART
        
        # change_map_srv(sweepee_change_map,2)
        #TODO uscire acensore
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_home_2)    #TODO valutare se si o no o tchuss
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_door_front)

        # result = door_docking(pub_vel_sweepee)
        # result = sweepee_move_forw(2.0, pub_vel_sweepee, 0.2,rate) 
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_panel_fuse)
        
        #EXIT FUSELAGE SWEEPEE
        
        # result = sweepee_move_yaw(-(math.pi/2.0),pub_vel_sweepee, 0.3,rate)
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_door_exit)
        # # !result = sweepee_movebase_client(sweepee_move_base,sweepee_door_out) #in case of tchuss
        # result = door_docking_exit(pub_vel_sweepee)
        # result = sweepee_move_forw(2.0, pub_vel_sweepee, 0.2,rate) 
        # result = sweepee_move_yaw(-(math.pi/2.0),pub_vel_sweepee, 0.3,rate)
        # result = sweepee_movebase_client(sweepee_move_base,sweepee_final_2)
        # result = sweepee_move_yaw((math.pi/2.0),pub_vel_sweepee, 0.3,rate)
        #TODO risalire sull'ascensore
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        #TEST NAVIGAZIONE 
        #QUADRATO
        # sweepee_move_forw(1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # rospy.sleep(0.3)
        # sweepee_move_yaw((math.pi/2.0),pub_vel_sweepee, 0.3,rate_fast)
        # rospy.sleep(0.3)
        # sweepee_move_forw(1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # rospy.sleep(0.3)
        # sweepee_move_yaw((math.pi/2.0),pub_vel_sweepee, 0.3,rate_fast)
        # rospy.sleep(0.3)
        # sweepee_move_forw(1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # rospy.sleep(0.3)
        # sweepee_move_yaw((math.pi/2.0),pub_vel_sweepee, 0.3,rate_fast)
        # rospy.sleep(0.3)
        # sweepee_move_forw(1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # rospy.sleep(0.3)
        # sweepee_move_yaw((math.pi/2.0),pub_vel_sweepee, 0.3,rate_fast)
        # rospy.sleep(0.3)
        
        #QUADRATO LAT
        
        # sweepee_move_forw(1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # rospy.sleep(0.2)
        # sweepee_move_lat(1.0,pub_vel_sweepee, 0.3,rate_fast)
        # rospy.sleep(0.2)
        # sweepee_move_forw(-1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # rospy.sleep(0.2)
        # sweepee_move_lat(-1.0,pub_vel_sweepee, 0.3,rate_fast)
        # rospy.sleep(0.2)
        # sweepee_move_forw(1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # rospy.sleep(0.2)
        # sweepee_move_lat(1.0,pub_vel_sweepee, 0.3,rate_fast)
        # rospy.sleep(0.2)
        # sweepee_move_forw(-1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # rospy.sleep(0.2)
        # sweepee_move_lat(-1.0,pub_vel_sweepee, 0.3,rate_fast)
        # rospy.sleep(0.2)
        # sweepee_move_forw(1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # rospy.sleep(0.2)
        # sweepee_move_lat(1.0,pub_vel_sweepee, 0.3,rate_fast)
        # rospy.sleep(0.2)
        # sweepee_move_forw(-1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # rospy.sleep(0.2)
        # sweepee_move_lat(-1.0,pub_vel_sweepee, 0.3,rate_fast)
        # rospy.sleep(0.2)
        
        
        
        #BACK AND FORTH
        # sweepee_move_forw(1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # sweepee_move_yaw((math.pi),pub_vel_sweepee, 0.3,rate_fast)
        # sweepee_move_forw(1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # sweepee_move_yaw((math.pi),pub_vel_sweepee, 0.3,rate_fast)
        # sweepee_move_forw(1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # sweepee_move_yaw((math.pi),pub_vel_sweepee, 0.3,rate_fast)
        # sweepee_move_forw(1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # sweepee_move_yaw((math.pi),pub_vel_sweepee, 0.3,rate_fast)
        # sweepee_move_forw(1.0, pub_vel_sweepee, 0.3,rate_fast) 
        # sweepee_move_yaw((math.pi),pub_vel_sweepee, 0.3,rate_fast)
        
        # # turtle_move_forw(1.0, pub_vel_turt, 0.3,rate_fast) 
        # turtle_move_yaw((math.pi/2),pub_vel_turt, 0.6,rate_fast)
        # # turtle_move_forw(1.0, pub_vel_turt, 0.3,rate_fast) 
        # turtle_move_yaw((math.pi/2),pub_vel_turt, 0.6,rate_fast)
        # # turtle_move_forw(1.0, pub_vel_turt, 0.3,rate_fast) 
        # turtle_move_yaw((math.pi/2),pub_vel_turt, 0.6,rate_fast)
        # # turtle_move_forw(1.0, pub_vel_turt, 0.3,rate_fast) 
        # turtle_move_yaw((math.pi/2),pub_vel_turt, 0.6,rate_fast)
        # # turtle_move_forw(1.0, pub_vel_turt, 0.3,rate_fast) 
        # turtle_move_yaw((math.pi/2),pub_vel_turt, 0.6,rate_fast)
        
        
        #result = docking_client()
        # if not result:
        #     sys.exit()
        # rospy.loginfo("Cart Docking done")
        #
        # result = cart_attach_client(cart_attach, True, False)
        # if not result:
        #     sys.exit()
        # rospy.loginfo("Cart attached")

        # result = movebase_client_front_door(client,clear_map,Door_front)
        # if not result:
        #     sys.exit()
        # rospy.loginfo("Moved successfully")

        # # Line subscirber
        # linesub = rospy.Subscriber("/line_segments", LineSegmentList, Lascallback)
        # rospy.loginfo("Sub line extractor initialized.")
        # rospy.sleep(0.5)
        # rospy.wait_for_message("/line_segments", LineSegmentList)
        # result = door_docking(pub_vel)
        # rospy.loginfo("Door docking finished")
        # linesub.unregister()

        # result = move_forw(2.5, pub_vel, 0.25)

        # result = movebase_client_front_door(client, clear_map, Panel_pose)
        # if not result:
        #     sys.exit()
        # rospy.loginfo("Moved successfully")
        #
        # result = cart_attach_client(cart_attach, False, True)
        # if not result:
        #     sys.exit()
        # rospy.loginfo("Cart attached")

        # result = move_forw(-1.7, pub_vel, 0.25)
        # result = move_lat(-1.0, pub_vel, 0.25)
        # rospy.sleep(1.0)

        # result = move_forw(3.5, pub_vel_turt, 0.18,rate)
        # result = move_yaw(1.57, pub_vel_turt, 0.6,rate)
        # result = move_forw(-1.0, pub_vel_turt, 0.1,rate)

        # result = movebase_client_front_door(client, clear_map, Home)
        # if not result:
        #     sys.exit()
        # rospy.loginfo("Moved successfully")


    except rospy.ROSInterruptException:
        rospy.loginfo("Navigation test finished.")
