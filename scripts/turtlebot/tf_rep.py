#!/usr/bin/env python
import tf
import rospy
from tf.msg import tfMessage

nh   = rospy.init_node('tf_repub', anonymous=True,disable_signals=False)    
rate = rospy.Rate(20)

br = tf.TransformBroadcaster()

def repub_tf(data):
    try:
        transV = data.transforms[0].transform.translation
        rotV   = data.transforms[0].transform.rotation
        trans  = (transV.x,transV.y,transV.x)
        rot    = (rotV.x,rotV.y,rotV.z,rotV.w)
        br.sendTransform(trans,
                     rot,
                     rospy.Time.now(),
                     "turtle/base_footprint",
                     "turtle/odom")
    except Exception as e: print(e)
         

list_top = rospy.Subscriber("/tf_turtle", tfMessage, repub_tf)


while not rospy.is_shutdown():
    print("tchus1")
    rospy.spin()
    print("tchus2")
    rate.sleep()    
