import rospy
import actionlib
import time, math
import os
import tf
from eureca_hosts import eureca_hosts
from laser_line_extraction.msg import LineSegmentList, LineSegment
from geometry_msgs.msg import Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from map_server.srv import LoadMap
from laser_line_extraction.msg import LineSegmentList, LineSegment
from threading import Lock
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
import roslaunch
from roslaunch import remoteprocess,remote
from geometry_msgs.msg import PoseWithCovarianceStamped
from std_srvs.srv import Empty
import numpy as np
import traceback
from std_msgs.msg import Bool

os.environ["ROSLAUNCH_SSH_UNKNOWN"] = "1"

class ProcessListener(roslaunch.pmon.ProcessListener):#TODO per tutti

    def process_died(self, name, exit_code):
        rospy.logwarn("%s died with code %s", name, exit_code)

class turtle_test_launch: #For cargo docking
    def __init__(self):
        self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
        roslaunch.configure_logging(self.uuid)
        pl_trt = ProcessListener()
        self.launch_remote = roslaunch.parent.ROSLaunchParent(self.uuid, ["/home/stade/dry_run_ws/src/eureca_brain/launch/remotes_lauch/turtlebot3_test.launch"],process_listeners=[pl_trt])

    def start(self):
        self.launch_remote.start()
        
    def stop(self):
        self.launch_remote.shutdown()

class turtle_startup_launch: #For cargo docking
    def __init__(self):
        self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
        roslaunch.configure_logging(self.uuid)
        self.launch        = roslaunch.parent.ROSLaunchParent(self.uuid, ["/home/stade/dry_run_ws/src/eureca_wimpy/launch/turtlebot3_navigation.launch"])
        # pl_trt = ProcessListener()
        # self.launch_remote = roslaunch.parent.ROSLaunchParent(self.uuid, ["/home/stade/dry_run_ws/src/eureca_brain/launch/remotes_lauch/turtlebot3_remote.launch"],process_listeners=[pl_trt])

    def start(self):
        self.launch.start()
        # self.launch_remote.start()
        
    def stop(self):
        self.launch.shutdown()

class laser_line_turtle: #For cargo docking
    def __init__(self):
        try:
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(self.uuid)
            self.launch = roslaunch.parent.ROSLaunchParent(self.uuid, ["/home/stade/dry_run_ws/src/eureca_brain/launch/include/laser_line_turtle.launch"])
        except Exception as e: print(e)

    def start(self):
        try:
            self.launch.start()
        except Exception as e: print(e)
        
    def stop(self):
        self.launch.shutdown()

class turtlebotClass:
    def __init__(self):
        rospy.logwarn("Creating turtlebot class")
        self._HOST_UP  = True if eureca_hosts.check_host(eureca_hosts.TURTLEBOT_IP) else False 
        if not self._HOST_UP:
            rospy.logerr("WIMPY IS NOT UP")
            return
        self.__delay    = eureca_hosts.check_time_host(eureca_hosts.TURTLEBOT_IP) 
        rospy.logwarn("Time wimpy wimpy %s:   ",str(self.__delay))

        
        self.emergency              = False

        self.__w_vel_turtlebot  = 0.0
        self.__x_odom_turtlebot = 0.0

        self.__rate             = rospy.Rate(50)
        self.move_base_action   = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
        self.__pub_vel          = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        self.__grasp_up         = rospy.Publisher('/simfal/upper_panel_grasp', Bool, queue_size=1,latch=True)
        self.__grasp_down       = rospy.Publisher('/simfal/lower_panel_grasp', Bool, queue_size=1,latch=True)
        self.__odom_sub         = None #rospy.Subscriber("/odom", Odometry, turtle_callback_odom)
        self.__change_map       = rospy.ServiceProxy('/turtle/change_map', LoadMap)
        self._relocalize_amcl   = rospy.ServiceProxy("/request_nomotion_update", Empty)
        
        self.listener = tf.TransformListener()
        
        
        self._laser_line_launch = laser_line_turtle()
        #TODO
        # self.__launcher = turtle_launch()
        # self.__launcher.start()

        self.__mutex   = Lock()
        
        self.__dist      = 0.0
        self.__angM      = 0.0
        self.__angM_fin  = 0.0      
        self.__cnt_dock  = 0
        self.__u_right   = np.zeros([3,50])
        self.__p_mid     = np.zeros([3,50])
        self.__dock_GO   = False
        
        self.position  = rospy.get_param("/wimpy/status/position"   ) if rospy.has_param("/wimpy/status/position"  ) else [0,0,0,0]
        self.ready     = rospy.get_param("/wimpy/status/ready"      ) if rospy.has_param("/wimpy/status/ready"     ) else False
        self.floor     = rospy.get_param("/wimpy/status/floor"      ) if rospy.has_param("/wimpy/status/floor"     ) else 0
        self.empty     = rospy.get_param("/wimpy/status/empty"      ) if rospy.has_param("/wimpy/status/empty"     ) else False
        self.is_moving = rospy.get_param("/wimpy/status/is_moving"  ) if rospy.has_param("/wimpy/status/is_moving" ) else False
        self.emergency = rospy.get_param("/wimpy/status/emergency"  ) if rospy.has_param("/wimpy/status/emergency" ) else False
        
        self.status_update()
        
        rospy.logwarn("Finished creating turtlebot class")

    def pose_cargo(self):
        init_pose = PoseWithCovarianceStamped()
        init_pose.header.frame_id = "map_turtle"
        init_pose.header.stamp    =  rospy.Time.now()
        init_pose.pose.pose.position.x    = 4.8
        init_pose.pose.pose.position.y    = 5.06
        init_pose.pose.pose.orientation.z = 0
        init_pose.pose.pose.orientation.w = 1
        pub = rospy.Publisher('/initialpose', PoseWithCovarianceStamped, queue_size=1)
        t_end = time.time() + 1.5
        while time.time() < t_end:
            pub.publish(init_pose)
            self.__rate.sleep()
        pub.unregister()
        return True

    def initial_pose_turtlebot_0_init(self):
        init_pose = PoseWithCovarianceStamped()
        init_pose.header.frame_id = "map_turtle"
        init_pose.header.stamp    =  rospy.Time.now()
        pos_init = rospy.get_param("/wimpy/status/position_init_0")
        init_pose.pose.pose.position.x    = pos_init[0]
        init_pose.pose.pose.position.y    = pos_init[1]
        init_pose.pose.pose.orientation.z = pos_init[2]
        init_pose.pose.pose.orientation.w = pos_init[3]
        pub = rospy.Publisher('/initialpose', PoseWithCovarianceStamped, queue_size=1)
        t_end = time.time() + 1.5
        while time.time() < t_end:
            pub.publish(init_pose)
            self.__rate.sleep()
        pub.unregister()
        return True

    def initial_pose_turtlebot_0_end(self):
        init_pose = PoseWithCovarianceStamped()
        init_pose.header.frame_id = "map_turtle"
        init_pose.header.stamp    =  rospy.Time.now()
        pos_init = rospy.get_param("/wimpy/status/position_init_0_end")
        init_pose.pose.pose.position.x    = pos_init[0]
        init_pose.pose.pose.position.y    = pos_init[1]
        init_pose.pose.pose.orientation.z = pos_init[2]
        init_pose.pose.pose.orientation.w = pos_init[3]
        pub = rospy.Publisher('/initialpose', PoseWithCovarianceStamped, queue_size=1)
        t_end = time.time() + 1.5
        while time.time() < t_end:
            pub.publish(init_pose)
            self.__rate.sleep()
        pub.unregister()
        return True

    def initial_pose_turtlebot_1(self):    #controlla uscita acensore e fai in modo arrivi qui
        init_pose = PoseWithCovarianceStamped()
        init_pose.header.frame_id = "map_turtle"
        init_pose.header.stamp    =  rospy.Time.now()
        pos_init = rospy.get_param("/wimpy/status/position_init_1")
        print(pos_init)
        init_pose.pose.pose.position.x    = pos_init[0]
        init_pose.pose.pose.position.y    = pos_init[1]
        init_pose.pose.pose.orientation.z = pos_init[2]
        init_pose.pose.pose.orientation.w = pos_init[3]
        pub = rospy.Publisher('/initialpose', PoseWithCovarianceStamped, queue_size=1)
        t_end = time.time() + 2.5
        while time.time() < t_end:
            pub.publish(init_pose)
            self.__rate.sleep()
        pub.unregister()
        return True

    def _turtlebot_callback_odom(self,data):
            self.__x_odom_turtlebot = data.twist.twist.linear.x
            self.__w_vel_turtlebot  = data.twist.twist.angular.z

    def turtlebot_move_forw(self,dist, vel):
        self.__odom_sub = rospy.Subscriber("/odom", Odometry, self._turtlebot_callback_odom)
        try:
            rospy.wait_for_message("/odom", Odometry)
            dist_made = 0.0
            vel_msg = Twist()
            if dist < 0.0:
                vel = -1.0 * vel
            vel_msg.linear.x = vel
            time_now = time.time()
            while (math.fabs(dist_made) < math.fabs(dist)) and not rospy.is_shutdown() and not self.emergency:
                self.__pub_vel.publish(vel_msg)
                dist_made = dist_made + self.__x_odom_turtlebot * (time.time() - time_now)
                time_now = time.time()
                self.__rate.sleep()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                vel_msg.linear.x = 0.0
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
        except :
            print("Exception")
            vel_msg = Twist()
            t_end = time.time() + 0.5
            while time.time() < t_end:
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
            return False
        finally:
            t_end = time.time() + 0.2
            while time.time() < t_end:
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
            self.__odom_sub.unregister()
            self.__odom_sub = None
        return True

    def turtlebot_move_yaw(self,dist, vel):
        self.__odom_sub = rospy.Subscriber("/odom", Odometry, self._turtlebot_callback_odom)
        try:
            rospy.wait_for_message("/odom", Odometry)
            dist_made = 0.0
            vel_msg = Twist()
            if dist < 0.0:
                vel = -1.0 * vel
            vel_msg.angular.z = vel
            time_now = time.time()
            while (math.fabs(dist_made) < math.fabs(dist)) and not rospy.is_shutdown() and not self.emergency:
                self.__pub_vel.publish(vel_msg)
                dist_made = dist_made + self.__w_vel_turtlebot * (time.time() - time_now)
                time_now = time.time()
                self.__rate.sleep()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                vel_msg.angular.z = 0.0
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
        except :
            print("Exception")
            vel_msg = Twist()
            t_end = time.time() + 0.5
            while time.time() < t_end:
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
            return False
        finally:
            t_end = time.time() + 0.2
            while time.time() < t_end:
                self.__pub_vel.publish(vel_msg)
                self.__rate.sleep()
            self.__odom_sub.unregister()
            self.__odom_sub = None
        return True

    def pos_err(self,pos_in,trans):
        return math.sqrt(((pos_in[0]-trans[0])**2)+((pos_in[1]-trans[1])**2))

    def turtlebot_movebase_client(self, pos_in):
        try:
            while not rospy.is_shutdown() and not self.emergency:
                self.move_base_action.wait_for_server()
                goal = MoveBaseGoal()
                goal.target_pose.header.frame_id = "map_turtle"
                print("moving to : ",pos_in[0], pos_in[1], pos_in[2], pos_in[3])
                goal.target_pose.header.stamp = rospy.Time.now()
                goal.target_pose.pose.position.x    = pos_in[0]
                goal.target_pose.pose.position.y    = pos_in[1]
                goal.target_pose.pose.orientation.z = pos_in[2]
                goal.target_pose.pose.orientation.w = pos_in[3]
    
                self.move_base_action.send_goal(goal)
                wait = self.move_base_action.wait_for_result()
                print(wait)
                listener = tf.TransformListener()
                rot   = []
                trans = []
                while rot == [] and not rospy.is_shutdown():
                    self.__rate.sleep()
                    try:
                        (trans,rot) = listener.lookupTransform('/map_turtle','/turtle/base_footprint', rospy.Time(0))
                    except:
                        continue  
                err_move = self.pos_err(pos_in,trans)
                print("pos_err:",err_move)
                if not wait:
                    rospy.logerr("Action server error!")
                    return wait
                else:
                    return self.move_base_action.get_result()
        except Exception as e: print(e)
        finally:
            self.move_base_action.cancel_all_goals()
            return self.move_base_action.get_result()
            
    def change_map_srv(self,map_number):
        if map_number==0:
            path = "/home/stade/dry_run_ws/src/eureca_brain/maps/zero.yaml"
        elif map_number==1:
            path = "/home/stade/dry_run_ws/src/eureca_brain/maps/first_t.yaml"
        elif map_number==2:
            path = "/home/stade/dry_run_ws/src/eureca_brain/maps/second.yaml"
        else:
            return False
        
        try:
            rospy.logwarn("jp1")
            resp1 = self.__change_map(path)
            rospy.logwarn("jp2")
            return resp1.success
        except Exception as e: 
            print(e)
            return False

    def _range_lis(self,list1):
        lowest = list1[0]
        lowest2 = None
        for item in list1[1:]:
            if item < lowest:
                lowest2 = lowest
                lowest = item
            elif lowest2 == None or lowest2 > item:
                lowest2 = item
        return lowest, lowest2

    def _las_dock_call(self,data):
        filt = []
        for lines in data.line_segments:
            if (abs(lines.angle) > 2.7 or abs(lines.angle) < 0.4) and (lines.start[0] > 0.0) and (lines.start[0] < 3.0) and abs((lines.start[1] < 1.0)) and abs((lines.end[1] < 1.0)) :
                filt.append(lines)

        try:
            ciao = [node.radius for node in filt].index(min(node.radius for node in filt))
            chk = filt[ciao]
        except:
            return

        
        u_v = np.asarray([chk.start[0], chk.start[1], 0]) - np.asarray([chk.end[0],chk.end[1],0])
        u_v = u_v / np.linalg.norm(u_v)
        u_w = np.asarray([0,0,0]) - np.asarray([chk.end[0],chk.end[1],0])
        u_w = u_w / np.linalg.norm(u_w)
        if self.__cnt_dock < self.__u_right.shape[1]:
            self.__u_right[:,self.__cnt_dock] = np.cross(np.cross(u_v,u_w)/np.linalg.norm(np.cross(u_v,u_w)),u_v)
            self.__p_mid[:,self.__cnt_dock]   =  (np.asarray([chk.start[0], chk.start[1], 0]) + np.asarray([chk.end[0],chk.end[1],0]))/2.0
            self.__cnt_dock = self.__cnt_dock + 1
            return
        else:     
            p_mid   = np.mean(self.__p_mid,axis=1).reshape(u_v.shape)
            u_right = np.mean(self.__u_right,axis=1).reshape(u_v.shape)
            v_mid = -p_mid
            # print("u_v : ",u_v,"u_w : ",u_w,"u_right : ",u_right,"p_mid : ",p_mid)
            self.p_goal = (np.dot(u_right,v_mid)/2.0) * u_right + p_mid
            # print("goal: ",self.p_goal)
            self.__dock_GO = True
        
        
        
        

        
        self.__mutex.acquire()
        # self.__midP = (np.asarray(chk.start) + np.asarray(chk.end))/2.0
        self.__dist  = np.linalg.norm(self.p_goal) 
        self.__angM  = np.arctan2(self.p_goal[1],self.p_goal[0])
        self.__angM_fin  = chk.angle
        self.__mutex.release()

    def _dock_cargo_iiwa(self):
        self._laser_line_launch.start()
        rospy.sleep(3)
        try:
            linesub = rospy.Subscriber("/line_segments_turtle", LineSegmentList, self._las_dock_call)
            rospy.wait_for_message("/line_segments_turtle", LineSegmentList)
            vel_msg = Twist()
            cnt = 0
            rospy.sleep(1)
            rate_dock = rospy.Rate(5)
            while not self.__dock_GO and not rospy.is_shutdown():
                rospy.sleep(0.2)
            self.turtlebot_move_yaw(self.__angM,0.1)
            self.turtlebot_move_forw(self.__dist,0.2)
            while not rospy.is_shutdown() and cnt < 5:
                self.__mutex.acquire()
                vel_msg.angular.z =  min(max(0.5 * self.__angM_fin, -0.05), 0.05)
                self.__mutex.release()
                self.__pub_vel.publish(vel_msg)
                if math.fabs(self.__angM_fin) > 3.12 or math.fabs(self.__angM_fin) < 0.02:
                    cnt = cnt + 1
                rate_dock.sleep()

            self.turtlebot_move_forw(1.2,0.1)
            return True
        except KeyboardInterrupt:
            print("Stop key pressed")
            vel_msg = Twist()
            t_end = time.time() + 0.5
            while time.time() < t_end:
                self.__pub_vel.publish(vel_msg)
                rate_dock.sleep()
            return False
        finally:
            print("turtlecart cargo dock finished")
            self._laser_line_launch.stop()
            linesub.unregister()
            vel_end = Twist()
            t_end = time.time() + 0.2
            while time.time() < t_end:
                self.__pub_vel.publish(vel_end)
                self.__rate.sleep()

    def relocalize_amcl_srv(self):
        try:
            resp = self._relocalize_amcl()
            rospy.sleep(0.5)
            return resp
        except:
            print("Service call failed")
        finally:
            print (resp)

    def pub_no_vel(self):
        vel_msg = Twist()
        t_end = time.time() + 2
        while time.time() < t_end:
            vel_msg.linear.x = 0.0
            self.__pub_vel.publish(vel_msg)
            self.__rate.sleep()

    def status_update(self, floor =  [], position = [], ready = [], empty = []):
        if (floor or floor==0):
            self.floor              = floor
        if position :    self.position = position
        if ready    :    self.ready    = ready
        if empty    :    self.empty    = empty
        rospy.set_param("/wimpy/status/floor",self.floor)
        rospy.set_param("/wimpy/status/position",self.position)
        rospy.set_param("/wimpy/status/ready",self.ready)
        rospy.set_param("/wimpy/status/empty",self.empty)

    def get_status( self ):
        return rospy.get_param("/wimpy/status" )            
                           
    def _get_wimpy_pos(self):
        rot   = []
        trans = []
        while rot == [] and not rospy.is_shutdown():
            try:
                (trans,rot) = self.listener.lookupTransform('/map_turtle','turtle/base_footprint', rospy.Time(0))
            except:
                rospy.logerr("Cannot retrieve tf")
                continue 
        return [trans[0],trans[1],rot[2],rot[3]]
    

    def level_zero_task_init(self):
        result = True
        result = self.change_map_srv(0)                          if result else False
        self.initial_pose_turtlebot_0_init()                     if result else False
        self.turtlebot_move_forw(1.1,0.5)                        if result else False
        self.turtlebot_move_yaw(math.pi/2.0,0.5)                 if result else False
        self.relocalize_amcl_srv()                               if result else False
        pos_goal = rospy.get_param("/wimpy/goal/on_elev_0")      if result else False
        result = self.turtlebot_movebase_client(pos_goal)        if result else False
        result = self.turtlebot_move_yaw(-(math.pi/2.0),0.5)     if result else False
        listener = tf.TransformListener()
        rot   = []
        trans = []
        while rot == [] and not rospy.is_shutdown():
            try:
                (trans,rot) = listener.lookupTransform('/map_turtle','/turtle/base_footprint', rospy.Time(0))
            except:
                continue      
        result = self.turtlebot_movebase_client((trans[0],trans[1] - 0.14,-0.707, 0.707))       if result else False
        if result:
            self.status_update(position = self._get_wimpy_pos())
        return result

    def level_zero_task_end(self):
        rospy.logwarn("pd0")
        result = self.change_map_srv(0)
        rospy.logwarn("pd1")
        self.initial_pose_turtlebot_0_end()                                  if result else False#TODO prendi pos init
        rospy.logwarn("pd2")
        pos_goal = rospy.get_param("/wimpy/goal/home_back_0")                if result else False
        result = self.turtlebot_movebase_client(pos_goal)                    if result else False
        if result:
            self.status_update(position = self._get_wimpy_pos())
        return result

    def level_one_task_enter(self):
        result = True
        result = self.change_map_srv(1)                                      if result else False
        result = self.initial_pose_turtlebot_1()                             if result else False
        self.relocalize_amcl_srv()                                           if result else False
        pos_goal = rospy.get_param("/wimpy/goal/elev_exit_1")                if result else False
        result = self.turtlebot_movebase_client(pos_goal)                    if result else False
        result = self.turtlebot_move_yaw((math.pi/2.5),0.5)                  if result else False
        self.relocalize_amcl_srv()                                           if result else False
        pos_goal = rospy.get_param("/wimpy/goal/cargo_mid_1")                if result else False
        result = self.turtlebot_movebase_client(pos_goal)                    if result else False
        result = self.turtlebot_move_yaw((-math.pi/2.0),0.5)                 if result else False
        listener = tf.TransformListener()
        rot   = []
        trans = []
        while rot == [] and not rospy.is_shutdown():
            try:
                (trans,rot) = listener.lookupTransform('/map_turtle','/turtle/base_footprint', rospy.Time(0))
            except:
                continue      
        result = self.turtlebot_movebase_client((trans[0],trans[1],0, 1))       if result else False
        result = self.turtlebot_move_forw(1.5,0.18)                          if result else False
        # result = self._dock_cargo_iiwa()                                     if result else False
        self.pose_cargo()
        if result:
            self.status_update(position = self._get_wimpy_pos())
        return result
    
    def level_one_move_for_assembly(self):
        #TODO GG
        pass
    
    def level_one_task_exit(self):
        result = True
        self.relocalize_amcl_srv()                                       if result else False
        # rospy.set_param("/move_base/GlobalPlanner/orientation_mode", 1)  if result else False
        # pos_goal = rospy.get_param("/wimpy/goal/cargo_mid_back_1")       if result else False
        # result = self.turtlebot_movebase_client(pos_goal)                if result else False
        self.relocalize_amcl_srv()                                       if result else False
        result = self.turtlebot_move_yaw(-(math.pi/2.0),0.5)             if result else False
        self.relocalize_amcl_srv()                                       if result else False
        pos_goal = rospy.get_param("/wimpy/goal/cargo_home_back_1")      if result else False
        result = self.turtlebot_movebase_client(pos_goal)                if result else False 
        result = self.turtlebot_move_yaw((math.pi/2.0),0.5)              if result else False
        result = self.turtlebot_move_forw(2,0.18)                      if result else False

        #TODO raddrizzarsi
        if result:
            self.status_update(position = self._get_wimpy_pos())
        return result

    def cargo_move_back(self,updown):
        while not rospy.get_param("/tasks/move_wimpy") and not rospy.is_shutdown():
            rospy.sleep(0.25)
        msgs = Bool(True)
        if updown==1:
            self.__grasp_up.publish(msgs)
        else:
            self.__grasp_down.publish(msgs)
        result = self.turtlebot_move_forw(-1.2,0.18)
        return result

    def cargo_move_forw(self):
        while not rospy.get_param("/tasks/move_wimpy_forward") and not rospy.is_shutdown():
            rospy.sleep(0.25)
        result = self.turtlebot_move_forw(1.3,0.18)
        return result



    def shutdown(self):
        print("Closing turtlebot")
        self.move_base_action.cancel_all_goals()
        vel_msg = Twist()
        t_end = time.time() + 0.5
        while time.time() < t_end:
            self.__pub_vel.publish(vel_msg)
            self.__rate.sleep()
        self.__pub_vel.unregister()
        print("Closed turtlebot")
        
    



