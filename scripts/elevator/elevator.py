from opcua import Client
from opcua import ua
import eureca_hosts
import rospy
import os
import sys
import traceback
import time


class elevatorClass:
    def __init__(self):
        
        #rospy.logwarn("Creating elevator class")
        
        self.docked_obj  = rospy.get_param("/elev/status/docked_obj"  ) if rospy.has_param("/elev/status/docked_obj"  ) else "none"
        self.ready       = rospy.get_param("/elev/status/ready"       ) if rospy.has_param("/elev/status/ready"       ) else False
        self.empty       = rospy.get_param("/elev/status/empty"       ) if rospy.has_param("/elev/status/empty"       ) else True
        self.floor       = rospy.get_param("/elev/status/floor"       ) if rospy.has_param("/elev/status/floor"       ) else 0
        self.is_moving   = rospy.get_param("/elev/status/is_moving"   ) if rospy.has_param("/elev/status/is_moving"   ) else False
        self.emergency   = rospy.get_param("/elev/status/emergency"   ) if rospy.has_param("/elev/status/emergency"   ) else False
        
        self.status_update( )
        
        self.__goLevel0   = 'ns=3;s="Extern_Com"."Lift_level_1"."Request_OPEN"'
        self.__goLevel1   = 'ns=3;s="Extern_Com"."Lift_level_2"."Request_OPEN"'
        self.__goLevel2   = 'ns=3;s="Extern_Com"."Lift_level_3"."Request_OPEN"'
        self.__Level1C    = 'ns=3;s="Extern_Com"."Lift_level_2"."Request_CLOSE"'
        self.__Level2C    = 'ns=3;s="Extern_Com"."Lift_level_3"."Request_CLOSE"'
        self.__door0O     = 'ns=3;s="Extern_Com"."Lift_level_1"."IS_OPENED"'
        self.__door1O     = 'ns=3;s="Extern_Com"."Lift_level_2"."IS_OPENED"'
        self.__door2O     = 'ns=3;s="Extern_Com"."Lift_level_3"."IS_OPENED"'
        self.__door1C     = 'ns=3;s="Extern_Com"."Lift_level_2"."IS_CLOSED"'
        self.__door2C     = 'ns=3;s="Extern_Com"."Lift_level_3"."IS_CLOSED"'

        #logging.basicConfig(level=logging.WARN)

        self._HOST_UP    = True if eureca_hosts.check_host(eureca_hosts.OPCUA_MASTER_IP) else False
        if not self._HOST_UP:
            rospy.logerr("ELEVATOR IS NOT UP")
            return
        
        
        self.__client  = None

        try:
            if self._HOST_UP:
                self.__client = Client("opc.tcp://10.43.49.21:4840")
                rospy.logwarn("OPCUA_MASTER connected")
            else:
                rospy.logerr("OPCUA_MASTER not found")
    
            self.__client.connect()
        except:
            print (" elevator unexpected error:", sys.exc_info()[0])
            self._HOST_UP = None
            return
            
    rospy.logwarn("Finished creating elevator class")
    

    def shutdown(self):
        self.__client.disconnect()
        print("OPCUA_MASTER disconnected")

    def move_zero(self):
        try:
            print("MOVING TO GROUND FLOOR")
            root = self.__client.get_root_node()
            objects = self.__client.get_objects_node()
            var = self.__client.get_node(self.__goLevel0)
            val = var.get_value()
            var.set_data_value(ua.DataValue(ua.Variant(True, ua.VariantType.Boolean)))
            check_var = False
            while not check_var and not rospy.is_shutdown():
                rospy.logwarn_throttle(5,"Checking elevator")
                var_check = self.__client.get_node(self.__door0O)
                check_var = var_check.get_value()
                time.sleep(1)
            if check_var:
                self.status_update(floor = 0)
            return check_var
        except:
            rospy.logerr("elevator failure.")
            return False

    def move_first(self):
        try:
            print("MOVING TO FIRST FLOOR")
            root = self.__client.get_root_node()
            objects = self.__client.get_objects_node()
            var = self.__client.get_node(self.__goLevel1)
            val = var.get_value()
            var.set_data_value(ua.DataValue(ua.Variant(True, ua.VariantType.Boolean)))
            check_var = False
            while not check_var and not rospy.is_shutdown():
                rospy.logwarn_throttle(5,"Checking elevator")
                var_check = self.__client.get_node(self.__door1O)
                check_var = var_check.get_value()
                time.sleep(1)
            if check_var:
                self.status_update(floor = 1)
            return check_var
        except:
            rospy.logerr("elevator failure.")
            return False

    def move_second(self):
        try:
            print("MOVING TO SECOND FLOOR")
            root = self.__client.get_root_node()
            objects = self.__client.get_objects_node()
            var = self.__client.get_node(self.__goLevel2)
            val = var.get_value()
            var.set_data_value(ua.DataValue(ua.Variant(True, ua.VariantType.Boolean)))
            check_var = False
            while not check_var and not rospy.is_shutdown():
                rospy.logwarn_throttle(5,"Checking elevator")
                var_check = self.__client.get_node(self.__door2O)
                check_var = var_check.get_value()
                time.sleep(1)
            if check_var:
                self.status_update(floor = 2)
            return check_var
        except:
            rospy.logerr("elevator failure.")
            return False
        
    def move(self, target_floor):
        if target_floor == 0:
            return self.move_zero()
        elif target_floor == 1:
            return self.move_first()
        elif target_floor == 2:
            return self.move_second()
        else:
            rospy.logerr("Moving the elev to floor " + str(target_floor) + "is not possible" )
            return False

    def status_update(self, floor =  [], position = [], docked_obj = [], ready = [], empty = [], docking_position = []):
        if (floor or floor==0):
            self.floor              = floor
        if docked_obj :    self.docked_obj         = docked_obj
        if ready      :    self.ready              = ready
        if empty      :    self.empty              = empty
        rospy.set_param("/elev/status/floor",self.floor)
        rospy.set_param("/elev/status/ready",self.ready)
        rospy.set_param("/elev/status/empty",self.empty)
        rospy.set_param("/elev/status/docked_obj",self.docked_obj)
        rospy.set_param("/elev/status/is_moving", self.is_moving )
        rospy.set_param("/elev/status/emergency", self.emergency )
        
        
    def get_status( self ):
        return rospy.get_param("/elev/status" )
