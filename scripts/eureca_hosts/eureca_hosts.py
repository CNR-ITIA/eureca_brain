import subprocess
import ntplib
from time import ctime
import rospy

SIMFAL_IP        = "192.169.0.5"
SWEEPEE_IP       = "192.169.0.15"
TURTLEBOT_IP     = "192.169.0.25"
KOLMOGOROV_IP    = "192.169.0.66"
DISPATCHER_IP    = "192.169.0.88"
PAOLO_IP         = "192.169.0.107"
NUC_IIWA_IP      = "192.169.0.105"
CAMERA_IIWA_IP   = "192.169.0.110"
CAMERA_POPEYE_IP = "192.169.0.180"
NUC_POPEYE_IP    = "192.169.0.185"
OPCUA_MASTER_IP  = "10.43.49.21"


def check_host(host):
    ret = subprocess.call(['timeout','1.5','ping', '-c', '1', host], stdout=open('/dev/null', 'w'), stderr=open('/dev/null', 'w'))
    return ret == 0

def check_time_host(host):
    try:
        c = ntplib.NTPClient()
        response = c.request(host, version=3)
        chk = True if (abs(response.offset) < 0.5 and response.offset != 0.0 ) else False
        if chk:
            return float(response.offset * 1000)
    except:
        return False
