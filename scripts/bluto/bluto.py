import rospy
import actionlib
import time, math
import os
import numpy as np
from roslaunch import remoteprocess,remote
from eureca_hosts import eureca_hosts

class blutoClass:
    
    def __init__(self):
        rospy.logwarn(" creating bluto class")
        self.docking_position = rospy.get_param("/bluto/status/docking_position_init")
        self.position         = rospy.get_param("/bluto/status/position"    ) if rospy.has_param("/bluto/status/position"    ) else rospy.get_param("/bluto/status/position_init")
        self.docked           = rospy.get_param("/bluto/status/docked"      ) if rospy.has_param("/bluto/status/docked"      ) else False
        self.ready            = rospy.get_param("/bluto/status/ready"       ) if rospy.has_param("/bluto/status/ready"       ) else False
        self.empty            = rospy.get_param("/bluto/status/empty"       ) if rospy.has_param("/bluto/status/empty"       ) else True
        self.floor            = rospy.get_param("/bluto/status/floor"       ) if rospy.has_param("/bluto/status/floor"       ) else 0
        self.is_moving        = rospy.get_param("/bluto/status/is_moving"   ) if rospy.has_param("/bluto/status/is_moving"   ) else False
        self.emergency        = rospy.get_param("/bluto/status/emergency"   ) if rospy.has_param("/bluto/status/emergency"   ) else False
        self.status_update( )
        rospy.logwarn("Finished creating bluto class")
        
        
    def status_update(self, floor =  [], position = [], docked = [], ready = [], empty = [], docking_position = []):
        if (floor or floor==0):
            self.floor              = floor
        if position :    self.position            = position
        if docked   :    self.docked              = docked
        if docking_position     :    self.docking_position    = docking_position
        if ready    :    self.ready  = ready
        if empty    :    self.empty  = empty
        rospy.set_param("/bluto/status/floor",self.floor)
        rospy.set_param("/bluto/status/position",self.position)
        rospy.set_param("/bluto/status/ready",self.ready)
        rospy.set_param("/bluto/status/empty",self.empty)
        rospy.set_param("/bluto/status/docked",self.docked)
        rospy.set_param("/bluto/status/is_moving", self.is_moving )
        rospy.set_param("/bluto/status/emergency", self.emergency)
        rospy.set_param("/bluto/status/docking_position", self.docking_position )
        
    def get_status( self ):
        return rospy.get_param("/bluto/status" )