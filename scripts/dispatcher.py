import os
import time
import math
import signal
import sys
from enum import Enum
import select
from threading import Thread
from configuration_msgs.srv import StartConfiguration
import datetime
import rospy
import logging
from popeye_assembly_msgs.srv import configure_planning_scene 
from std_srvs.srv import Trigger,TriggerRequest,TriggerResponse,Empty,EmptyRequest
from geometry_msgs.msg import Pose
import traceback
import socket
import easygui

# logging.basicConfig(level=logging.WARN)

logging.basicConfig(level=logging.INFO)

from std_msgs.msg import Bool,String,Int8
from sensor_msgs.msg import Joy

from sweepee import sweepee as sweepeeMod
from popeye import popeye as popeyeMod
from popeye import remote_popeye
from olivia import olivia as oliviaMod
from bluto import bluto as blutoMod
from turtlebot import turtlebot as turtlebotMod
from elevator import elevator as elevatorMod

def listToString(s):  
    str1 = ""   
    for ele in s:  
        str1 +=( ele + ",")     
    return str1[:-1] 

class AutoNumber(Enum):
     def __new__(cls):
         value = len(cls.__members__) + 1
         obj = object.__new__(cls)
         obj._value_ = value
         return obj

class TaskState(Enum):
    IDLE        = 0
    RUNNING     = 1
    DONE        = 2
    REPEAT      = 3
    SYS_EXIT    = 4

class TaskCode(AutoNumber):
    ELEVATOR_FROM_0_TO_1                                = ( )
    ELEVATOR_FROM_1_TO_2                                = ( )
    ELEVATOR_FROM_0_TO_2                                = ( )
    ELEVATOR_FROM_1_TO_0                                = ( )
    ELEVATOR_FROM_2_TO_0                                = ( )
    ELEVATOR_FROM_2_TO_1                                = ( )
    
    SWEEPEE_CLAMP_OPEN                                  = ( )
    SWEEPEE_CLAMP_CLOSE                                 = ( )
    SWEEPEE_DOCK_HERE                                   = ( )
    
    SWEEPEE_FROM_HOME_TO_OLIVIA                         = ( )
    SWEEPEE_FROM_HOME_TO_POPEYE                         = ( )
    SWEEPEE_FROM_HOME_TO_BLUTO                          = ( )
    SWEEPEE_FROM_ELEVATOR_TO_HOME                       = ( )
    
    CARGO_ALL_PARALLEL                                  = ( )
    
    SWEEPEE_OLIVIA_FROM_HOME_TO_ELEVATOR_1              = ( )
    SWEEPEE_OLIVIA_FROM_HOME_TO_ELEVATOR_2              = ( )
    SWEEPEE_POPEYE_FROM_HOME_TO_ELEVATOR                = ( )
    SWEEPEE_BLUTO_FROM_HOME_TO_ELEVATOR                 = ( )

    SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_HOME                = ( )
    SWEEPEE_POPEYE_FROM_ELEVATOR_TO_HOME                = ( )
    SWEEPEE_BLUTO_FROM_ELEVATOR_TO_HOME                 = ( )
    
    SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_CARGO_ASSEMBLY      = ( )
    SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_CARGO_PARALLEL      = ( )
    SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY   = ( )
    SWEEPEE_OLIVIA_FROM_CARGO_ASSEMBLY_TO_ELEVATOR      = ( )
    SWEEPEE_OLIVIA_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR   = ( )
    SWEEPEE_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR          = ( )
    #TODO sweepee from cargo to elevator
    SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY   = ( )
    SWEEPEE_POPEYE_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR   = ( )

    SWEEPEE_BLUTO_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY    = ( )
    SWEEPEE_BLUTO_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR    = ( )
    
    WIMPY_FROM_HOME_TO_ELEVATOR                         = ( )
    WIMPY_FROM_ELEVATOR_TO_CARGO_ASSEMBLY               = ( )
    WIMPY_FROM_ELEVATOR_TO_HOME                         = ( )
    WIMPY_FROM_CARGO_ASSEMBLY_TO_ELEVATOR               = ( )
    
    OLIVIA_GO_HOME                                      = ( )
    OLIVIA_CARGO_ASSEMBLY_UP                            = ( )
    OLIVIA_CARGO_ASSEMBLY_DOWN                          = ( )
    OLIVIA_FUSELAGE_ASSEMBLY_1                          = ( )
    OLIVIA_FUSELAGE_ASSEMBLY_2                          = ( )
    MOVE_OLIVIA_FROM_ASS_1_TO_2                         = ( )
    
    POPEYE_FUSELAGE_ASSEMBLY                            = ( )
    SWEEPEE_FROM_HOME_TO_ELEVATOR                       = ( )

robot_ids =  [ 'elev'
             , 'wimpy'
             , 'sweepee'
             , 'popeye'
             , 'olivia'
             , 'bluto'   ]

ROBOT_IDX = { v : c * len( robot_ids )  for c, v in enumerate( robot_ids )}

DOCKED  = 0
READY   = 1
EMPTY  = 2
FLOOR  = 3
ISMOV  = 4
ISEME  = 5

CARGO_PNL_UP   = 36 + 0
CARGO_PNL_LOW  = 36 + 1
SIDE_PNL_1     = 36 + 2
SIDE_PNL_2     = 36 + 3
HATRACK        = 36 + 4

#popeye
#  docked
#  ready
#  empty 
#  floor
#  is moving
#  is emergency
#olivia
#bluto
#sweepee
#wimpy
#elev

# cargopanel_assembled_state_up
# cargopanel_assembled_state_down
# sidepanel_assembled_state
# hatrack_assembled_state
# waiting_for_elevator


class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs, Verbose)
        self._return = None
    def run(self):
        if self._Thread__target is not None:
            self._return = self._Thread__target(*self._Thread__args,**self._Thread__kwargs)
    def join(self):
        Thread.join(self)
        return self._return

    
class FSM:
    
    def __init__(self):
        self.id =   { TaskCode.ELEVATOR_FROM_0_TO_1                                : "ELEVATOR_FROM_0_TO_1"
                    , TaskCode.ELEVATOR_FROM_1_TO_2                                : "ELEVATOR_FROM_1_TO_2"
                    , TaskCode.ELEVATOR_FROM_0_TO_2                                : "ELEVATOR_FROM_0_TO_2"
                    , TaskCode.ELEVATOR_FROM_1_TO_0                                : "ELEVATOR_FROM_1_TO_0"
                    , TaskCode.ELEVATOR_FROM_2_TO_0                                : "ELEVATOR_FROM_2_TO_0"
                    , TaskCode.ELEVATOR_FROM_2_TO_1                                : "ELEVATOR_FROM_2_TO_1"
                    , TaskCode.CARGO_ALL_PARALLEL                                  : "CARGO_ALL_PARALLEL"
                    , TaskCode.SWEEPEE_FROM_HOME_TO_OLIVIA                         : "SWEEPEE_FROM_HOME_TO_OLIVIA"
                    , TaskCode.SWEEPEE_FROM_HOME_TO_POPEYE                         : "SWEEPEE_FROM_HOME_TO_POPEYE"
                    , TaskCode.SWEEPEE_FROM_HOME_TO_BLUTO                          : "SWEEPEE_FROM_HOME_TO_BLUTO"
                    , TaskCode.SWEEPEE_FROM_ELEVATOR_TO_HOME                       : "SWEEPEE_FROM_ELEVATOR_TO_HOME"
                    , TaskCode.SWEEPEE_FROM_HOME_TO_ELEVATOR                       : "SWEEPEE_FROM_HOME_TO_ELEVATOR"
                    , TaskCode.SWEEPEE_OLIVIA_FROM_HOME_TO_ELEVATOR_1              : "SWEEPEE_OLIVIA_FROM_HOME_TO_ELEVATOR_1"
                    , TaskCode.SWEEPEE_OLIVIA_FROM_HOME_TO_ELEVATOR_2              : "SWEEPEE_OLIVIA_FROM_HOME_TO_ELEVATOR_2"
                    , TaskCode.SWEEPEE_POPEYE_FROM_HOME_TO_ELEVATOR                : "SWEEPEE_POPEYE_FROM_HOME_TO_ELEVATOR"
                    , TaskCode.SWEEPEE_BLUTO_FROM_HOME_TO_ELEVATOR                 : "SWEEPEE_BLUTO_FROM_HOME_TO_ELEVATOR"
                    , TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_HOME                : "SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_HOME"
                    , TaskCode.SWEEPEE_POPEYE_FROM_ELEVATOR_TO_HOME                : "SWEEPEE_POPEYE_FROM_ELEVATOR_TO_HOME"
                    , TaskCode.SWEEPEE_BLUTO_FROM_ELEVATOR_TO_HOME                 : "SWEEPEE_BLUTO_FROM_ELEVATOR_TO_HOME"
                    , TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_CARGO_ASSEMBLY      : "SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_CARGO_ASSEMBLY"
                    , TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_CARGO_PARALLEL      : "SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_CARGO_PARALLEL"
                    , TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY   : "SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY"
                    , TaskCode.SWEEPEE_OLIVIA_FROM_CARGO_ASSEMBLY_TO_ELEVATOR      : "SWEEPEE_OLIVIA_FROM_CARGO_ASSEMBLY_TO_ELEVATOR"
                    , TaskCode.SWEEPEE_OLIVIA_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR   : "SWEEPEE_OLIVIA_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR"
                    , TaskCode.SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY   : "SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY"
                    , TaskCode.SWEEPEE_POPEYE_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR   : "SWEEPEE_POPEYE_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR"
                    , TaskCode.SWEEPEE_BLUTO_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY    : "SWEEPEE_BLUTO_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY"
                    , TaskCode.SWEEPEE_BLUTO_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR    : "SWEEPEE_BLUTO_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR"
                    , TaskCode.SWEEPEE_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR          : "SWEEPEE_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR"
                    , TaskCode.WIMPY_FROM_HOME_TO_ELEVATOR                         : "WIMPY_FROM_HOME_TO_ELEVATOR"
                    , TaskCode.WIMPY_FROM_ELEVATOR_TO_CARGO_ASSEMBLY               : "WIMPY_FROM_ELEVATOR_TO_CARGO_ASSEMBLY"
                    , TaskCode.WIMPY_FROM_ELEVATOR_TO_HOME                         : "WIMPY_FROM_ELEVATOR_TO_HOME"
                    , TaskCode.WIMPY_FROM_CARGO_ASSEMBLY_TO_ELEVATOR               : "WIMPY_FROM_CARGO_ASSEMBLY_TO_ELEVATOR"
                    , TaskCode.OLIVIA_GO_HOME                                      : "OLIVIA_GO_HOME"
                    , TaskCode.OLIVIA_CARGO_ASSEMBLY_UP                            : "OLIVIA_CARGO_ASSEMBLY_UP"
                    , TaskCode.OLIVIA_CARGO_ASSEMBLY_DOWN                          : "OLIVIA_CARGO_ASSEMBLY_DOWN"
                    , TaskCode.OLIVIA_FUSELAGE_ASSEMBLY_1                          : "OLIVIA_FUSELAGE_ASSEMBLY_1"
                    , TaskCode.OLIVIA_FUSELAGE_ASSEMBLY_2                          : "OLIVIA_FUSELAGE_ASSEMBLY_2"
                    , TaskCode.POPEYE_FUSELAGE_ASSEMBLY                            : "POPEYE_FUSELAGE_ASSEMBLY"
                    , TaskCode.SWEEPEE_CLAMP_CLOSE                                 : "SWEEPEE_CLAMP_CLOSE"
                    , TaskCode.SWEEPEE_CLAMP_OPEN                                  : "SWEEPEE_CLAMP_OPEN"
                    , TaskCode.SWEEPEE_DOCK_HERE                                   : "SWEEPEE_DOCK_HERE"}
   

        self.resources = \
                    { TaskCode.ELEVATOR_FROM_0_TO_1                               : [ 'elev' ]
                    , TaskCode.ELEVATOR_FROM_1_TO_2                               : [ 'elev' ]
                    , TaskCode.ELEVATOR_FROM_0_TO_2                               : [ 'elev' ]
                    , TaskCode.ELEVATOR_FROM_1_TO_0                               : [ 'elev' ]
                    , TaskCode.ELEVATOR_FROM_2_TO_0                               : [ 'elev' ]
                    , TaskCode.ELEVATOR_FROM_2_TO_1                               : [ 'elev' ]
                    , TaskCode.CARGO_ALL_PARALLEL                                 : [ 'elev','sweepee','wimpy']
                    , TaskCode.SWEEPEE_FROM_HOME_TO_OLIVIA                        : [ 'sweepee' ]
                    , TaskCode.SWEEPEE_FROM_HOME_TO_POPEYE                        : [ 'sweepee' ]
                    , TaskCode.SWEEPEE_FROM_HOME_TO_BLUTO                         : [ 'sweepee' ]
                    , TaskCode.SWEEPEE_FROM_ELEVATOR_TO_HOME                      : [ 'sweepee' ]
                    , TaskCode.SWEEPEE_FROM_HOME_TO_ELEVATOR                      : [ 'sweepee' ]
                    , TaskCode.SWEEPEE_OLIVIA_FROM_HOME_TO_ELEVATOR_1             : [ 'sweepee', 'olivia' ]
                    , TaskCode.SWEEPEE_OLIVIA_FROM_HOME_TO_ELEVATOR_2             : [ 'sweepee', 'olivia' ]
                    , TaskCode.SWEEPEE_POPEYE_FROM_HOME_TO_ELEVATOR               : [ 'sweepee', 'popeye' ]
                    , TaskCode.SWEEPEE_BLUTO_FROM_HOME_TO_ELEVATOR                : [ 'sweepee', 'bluto'  ]
                    , TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_HOME               : [ 'sweepee', 'olivia' ]
                    , TaskCode.SWEEPEE_POPEYE_FROM_ELEVATOR_TO_HOME               : [ 'sweepee', 'popeye' ]
                    , TaskCode.SWEEPEE_BLUTO_FROM_ELEVATOR_TO_HOME                : [ 'sweepee', 'bluto'  ]
                    , TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_CARGO_ASSEMBLY     : [ 'sweepee', 'olivia' ]
                    , TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_CARGO_PARALLEL     : [ 'sweepee', 'olivia', 'elev']
                    , TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY  : [ 'sweepee', 'olivia' ]
                    , TaskCode.SWEEPEE_OLIVIA_FROM_CARGO_ASSEMBLY_TO_ELEVATOR     : [ 'sweepee', 'olivia' ]
                    , TaskCode.SWEEPEE_OLIVIA_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR  : [ 'sweepee', 'olivia' ]
                    , TaskCode.SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY  : [ 'sweepee', 'popeye' ]
                    , TaskCode.SWEEPEE_POPEYE_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR  : [ 'sweepee', 'popeye' ]
                    , TaskCode.SWEEPEE_BLUTO_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY   : [ 'sweepee', 'bluto'  ]
                    , TaskCode.SWEEPEE_BLUTO_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR   : [ 'sweepee', 'bluto'  ]
                    , TaskCode.SWEEPEE_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR         : [ 'sweepee']
                    , TaskCode.WIMPY_FROM_HOME_TO_ELEVATOR                        : [ 'wimpy' ]
                    , TaskCode.WIMPY_FROM_ELEVATOR_TO_CARGO_ASSEMBLY              : [ 'wimpy' ]
                    , TaskCode.WIMPY_FROM_ELEVATOR_TO_HOME                        : [ 'wimpy' ]
                    , TaskCode.WIMPY_FROM_CARGO_ASSEMBLY_TO_ELEVATOR              : [ 'wimpy' ]
                    , TaskCode.OLIVIA_GO_HOME                                     : [ 'olivia' ]
                    , TaskCode.OLIVIA_CARGO_ASSEMBLY_UP                           : [ 'olivia' ]
                    , TaskCode.OLIVIA_CARGO_ASSEMBLY_DOWN                         : [ 'olivia' ]
                    , TaskCode.OLIVIA_FUSELAGE_ASSEMBLY_1                         : [ 'olivia' ]
                    , TaskCode.OLIVIA_FUSELAGE_ASSEMBLY_2                         : [ 'olivia' ]
                    , TaskCode.POPEYE_FUSELAGE_ASSEMBLY                           : [ 'popeye' ] 
                    , TaskCode.SWEEPEE_CLAMP_CLOSE                                : [ 'sweepee' ]
                    , TaskCode.SWEEPEE_CLAMP_OPEN                                 : [ 'sweepee' ] 
                    , TaskCode.SWEEPEE_DOCK_HERE                                  : [ 'sweepee' ] }
        
        self.actual_mask = [None] * (36 + 5)
        

    def update_actual_mask( self ):
        for i, robot_id in enumerate( robot_ids ):
            docked = False
            if robot_id in ["sweepee", "elev" ]:
                if rospy.has_param("/"+str(robot_id)+"/status/docked_obj"    ):
                    docked = False if rospy.get_param("/"+str(robot_id)+"/status/docked_obj" ) == "none" else True
                else:
                    docked = False                                                                    
            else:
                docked = rospy.get_param("/"+str(robot_id)+"/status/docked"    ) if rospy.has_param("/"+str(robot_id)+"/status/docked"    ) else False
                
            self.actual_mask[ i * 6 + 0 ] = docked
            self.actual_mask[ i * 6 + 1 ] = rospy.get_param("/"+str(robot_id)+"/status/ready"     ) if rospy.has_param("/"+str(robot_id)+"/status/ready"     ) else False
            self.actual_mask[ i * 6 + 2 ] = rospy.get_param("/"+str(robot_id)+"/status/empty"     ) if rospy.has_param("/"+str(robot_id)+"/status/empty"     ) else False
            self.actual_mask[ i * 6 + 3 ] = rospy.get_param("/"+str(robot_id)+"/status/floor"     ) if rospy.has_param("/"+str(robot_id)+"/status/floor"     ) else 0
            self.actual_mask[ i * 6 + 4 ] = rospy.get_param("/"+str(robot_id)+"/status/is_moving" ) if rospy.has_param("/"+str(robot_id)+"/status/is_moving" ) else False
            self.actual_mask[ i * 6 + 5 ] = rospy.get_param("/"+str(robot_id)+"/status/emergency" ) if rospy.has_param("/"+str(robot_id)+"/status/emergency" ) else False

        self.actual_mask[ CARGO_PNL_UP ]  = rospy.get_param("/tasks/cargo_upper_panel_1/done" ) if rospy.has_param("/"+str(robot_id)+"/tasks/cargo_upper_panel_1/done"  ) else False
        self.actual_mask[ CARGO_PNL_LOW ] = rospy.get_param("/tasks/cargo_lower_panel_1/done" ) if rospy.has_param("/"+str(robot_id)+"/tasks/cargo_lower_panel_1/done"  ) else False
        self.actual_mask[ SIDE_PNL_1 ]    = rospy.get_param("/side_panel/status_1"            ) if rospy.has_param("/"+str(robot_id)+"/side_panel/status_1"             ) else False
        self.actual_mask[ SIDE_PNL_2 ]    = rospy.get_param("/side_panel/status_2"            ) if rospy.has_param("/"+str(robot_id)+"/side_panel/status_2"             ) else False
        self.actual_mask[ HATRACK ]       = rospy.get_param("/hatrack_panel/status"           ) if rospy.has_param("/"+str(robot_id)+"/hatrack_panel/status"            ) else False
        
    def print_mask(self, verbose = True )     :
        if verbose:
            message = "Mask ["
            for i, robot_id in enumerate( robot_ids ):
                message = message + "|" + robot_id[0]
                for j in range(6):
                    message = message + ( str(1) if self.actual_mask[ i * 6 + j ] else str(0) )
            
            message = message + "|"
            for j in range(5):
                message = message + ( str(1) if self.actual_mask[ 36 + j ] else str(0) )
              
            print(message)
                
        else:  
            print(self.actual_mask)
        
    def is_task_available( self, task_id ):
        
        if   task_id == TaskCode.ELEVATOR_FROM_0_TO_1: 
            return  ( self.actual_mask[ ROBOT_IDX['elev' ] + FLOOR ] == 0 ) 
                    
        elif task_id == TaskCode.ELEVATOR_FROM_1_TO_2:
            return  ( self.actual_mask[ ROBOT_IDX['elev' ] + FLOOR ] == 1 ) 
        
        elif task_id == TaskCode.ELEVATOR_FROM_0_TO_2:                           
            return  ( self.actual_mask[ ROBOT_IDX['elev' ] + FLOOR ] == 0 ) 
            
        elif task_id == TaskCode.ELEVATOR_FROM_1_TO_0:
            return  ( self.actual_mask[ ROBOT_IDX['elev' ] + FLOOR ] == 1 ) 
            
        elif task_id == TaskCode.ELEVATOR_FROM_2_TO_0: 
            return  ( self.actual_mask[ ROBOT_IDX['elev' ] + FLOOR ] == 2 ) 
        
        elif task_id == TaskCode.ELEVATOR_FROM_2_TO_1: 
            return  ( self.actual_mask[ ROBOT_IDX['elev' ] + FLOOR ] == 2 ) 
        
        elif task_id == TaskCode.CARGO_ALL_PARALLEL: 
            return  ( self.actual_mask[ ROBOT_IDX['olivia' ] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['olivia' ] + DOCKED ] )\
              and   ( self.actual_mask[ ROBOT_IDX['sweepee'] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['sweepee'] + DOCKED ] )\
              and   ( self.actual_mask[ ROBOT_IDX['elev' ]   + FLOOR ] == 0)
        
        elif task_id == TaskCode.SWEEPEE_FROM_HOME_TO_OLIVIA: 
            return  ( self.actual_mask[ ROBOT_IDX['olivia' ] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['olivia' ] + DOCKED ] )\
              and   ( self.actual_mask[ ROBOT_IDX['sweepee'] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['sweepee'] + DOCKED ] )

        elif task_id == TaskCode.SWEEPEE_FROM_HOME_TO_POPEYE: 
            return  ( self.actual_mask[ ROBOT_IDX['popeye' ] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['popeye' ] + DOCKED ] )\
              and   ( self.actual_mask[ ROBOT_IDX['sweepee'] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['sweepee'] + DOCKED ] )
              
        elif task_id == TaskCode.SWEEPEE_FROM_HOME_TO_BLUTO:
            return  ( self.actual_mask[ ROBOT_IDX['bluto'  ] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['bluto'  ] + DOCKED ] )\
              and   ( self.actual_mask[ ROBOT_IDX['sweepee'] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['sweepee'] + DOCKED ] )
            
        elif task_id == TaskCode.SWEEPEE_FROM_ELEVATOR_TO_HOME: 
            return ( self.actual_mask[ ROBOT_IDX['sweepee'] + FLOOR  ] == 0 and not self.actual_mask[ ROBOT_IDX['sweepee'] + DOCKED ] )\
               and ( self.actual_mask[ ROBOT_IDX['elev'   ] + FLOOR  ] == 0 and     self.actual_mask[ ROBOT_IDX['elev'   ] + DOCKED ] )
            
        elif task_id == TaskCode.SWEEPEE_FROM_HOME_TO_ELEVATOR: 
            return ( self.actual_mask[ ROBOT_IDX['sweepee'] + FLOOR  ] == 0 and not self.actual_mask[ ROBOT_IDX['sweepee'] + DOCKED ]  )\
               and ( self.actual_mask[ ROBOT_IDX['elev'   ] + FLOOR  ] == 0 )
        
        elif task_id == TaskCode.SWEEPEE_OLIVIA_FROM_HOME_TO_ELEVATOR_1:
            return  ( self.actual_mask[ ROBOT_IDX['sweepee'] + FLOOR ] == 0 and     self.actual_mask[ ROBOT_IDX['sweepee'] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['olivia' ] + FLOOR ] == 0 and     self.actual_mask[ ROBOT_IDX['olivia' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'   ] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['elev'   ] + DOCKED ] )
               
        elif task_id == TaskCode.SWEEPEE_OLIVIA_FROM_HOME_TO_ELEVATOR_2:
            return  ( self.actual_mask[ ROBOT_IDX['sweepee'] + FLOOR ] == 0 and     self.actual_mask[ ROBOT_IDX['sweepee'] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['olivia' ] + FLOOR ] == 0 and     self.actual_mask[ ROBOT_IDX['olivia' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'   ] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['elev'   ] + DOCKED ] )
            
        elif task_id == TaskCode.SWEEPEE_POPEYE_FROM_HOME_TO_ELEVATOR:
            return  ( self.actual_mask[ ROBOT_IDX['sweepee'] + FLOOR ] == 0 and     self.actual_mask[ ROBOT_IDX['sweepee'] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['popeye' ] + FLOOR ] == 0 and     self.actual_mask[ ROBOT_IDX['popeye' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'   ] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['elev'   ] + DOCKED ] )
        
        elif task_id == TaskCode.SWEEPEE_BLUTO_FROM_HOME_TO_ELEVATOR: 
            return  ( self.actual_mask[ ROBOT_IDX['sweepee'] + FLOOR ] == 0 and     self.actual_mask[ ROBOT_IDX['sweepee'] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['bluto'  ] + FLOOR ] == 0 and     self.actual_mask[ ROBOT_IDX['bluto'  ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'   ] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['elev'   ] + DOCKED ] )
               
        elif task_id == TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_HOME: 
            return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 0 and self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['olivia'  ] + FLOOR ] == 0 and self.actual_mask[ ROBOT_IDX['olivia'  ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'    ] + FLOOR ] == 0 and self.actual_mask[ ROBOT_IDX['elev'    ] + DOCKED ] )
               
        elif task_id == TaskCode.SWEEPEE_POPEYE_FROM_ELEVATOR_TO_HOME: 
            return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 0 and self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['popeye'  ] + FLOOR ] == 0 and self.actual_mask[ ROBOT_IDX['popeye'  ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'    ] + FLOOR ] == 0 and self.actual_mask[ ROBOT_IDX['elev'    ] + DOCKED ] )

        elif task_id == TaskCode.SWEEPEE_BLUTO_FROM_ELEVATOR_TO_HOME: 
            return  ( self.actual_mask[ ROBOT_IDX['sweepee'] + FLOOR ] == 0 and self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['bluto'  ] + FLOOR ] == 0 and self.actual_mask[ ROBOT_IDX['bluto'   ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'   ] + FLOOR ] == 0 and self.actual_mask[ ROBOT_IDX['elev'    ] + DOCKED ] )
               
        elif task_id == TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_CARGO_ASSEMBLY: 
            return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['olivia'  ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['olivia'  ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'    ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['elev'    ] + DOCKED ] )
       
        elif task_id == TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_CARGO_PARALLEL: 
            return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['olivia'  ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['olivia'  ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'    ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['elev'    ] + DOCKED ] )
                
            
        elif task_id == TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY: 
            return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['olivia'  ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['olivia'  ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'    ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['elev'    ] + DOCKED ] )

        elif task_id == TaskCode.SWEEPEE_OLIVIA_FROM_CARGO_ASSEMBLY_TO_ELEVATOR: 
            return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 1 and     self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['olivia'  ] + FLOOR ] == 1 and     self.actual_mask[ ROBOT_IDX['olivia'  ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'    ] + FLOOR ] == 1 and not self.actual_mask[ ROBOT_IDX['elev'    ] + DOCKED ] )
               
        elif task_id == TaskCode.SWEEPEE_OLIVIA_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR: 
            return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 2 and     self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['olivia'  ] + FLOOR ] == 2 and     self.actual_mask[ ROBOT_IDX['olivia'  ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'    ] + FLOOR ] == 2 and not self.actual_mask[ ROBOT_IDX['elev'    ] + DOCKED ] )
               
        elif task_id == TaskCode.SWEEPEE_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR:
            return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 2 and  self.actual_mask[ ROBOT_IDX['elev'    ] + FLOOR ] == 2)
        
        elif task_id == TaskCode.SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY: 
            return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['popeye'  ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['popeye'  ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'    ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['elev'    ] + DOCKED ] )
               
        elif task_id == TaskCode.SWEEPEE_POPEYE_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR: 
            return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 2 and     self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['popeye'  ] + FLOOR ] == 2 and     self.actual_mask[ ROBOT_IDX['popeye'  ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'    ] + FLOOR ] == 2 and not self.actual_mask[ ROBOT_IDX['elev'    ] + DOCKED ] )
               
        elif task_id == TaskCode.SWEEPEE_BLUTO_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY: 
            return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['bluto'   ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['bluto'   ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'    ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['elev'    ] + DOCKED ] )

        elif task_id == TaskCode.SWEEPEE_BLUTO_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR:
            return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 2 and     self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['bluto'   ] + FLOOR ] == 2 and     self.actual_mask[ ROBOT_IDX['bluto'   ] + DOCKED ] )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'    ] + FLOOR ] == 2 and not self.actual_mask[ ROBOT_IDX['elev'    ] + DOCKED ] )
               
        elif task_id == TaskCode.WIMPY_FROM_HOME_TO_ELEVATOR:
            return  ( self.actual_mask[ ROBOT_IDX['wimpy' ] + FLOOR ] == 0 )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'  ] + FLOOR ] == 0 and not self.actual_mask[ ROBOT_IDX['elev'  ] + DOCKED ] )
            
        elif task_id == TaskCode.WIMPY_FROM_ELEVATOR_TO_CARGO_ASSEMBLY: 
            return  ( self.actual_mask[ ROBOT_IDX['wimpy' ] + FLOOR ] == 1 )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'  ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['elev'  ] + DOCKED ] )
               
        elif task_id == TaskCode.WIMPY_FROM_ELEVATOR_TO_HOME: 
            return  ( self.actual_mask[ ROBOT_IDX['wimpy' ] + FLOOR ] == 0 )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'  ] + FLOOR ] == 0 and self.actual_mask[ ROBOT_IDX['elev'  ] + DOCKED ] )
        
        elif task_id == TaskCode.WIMPY_FROM_CARGO_ASSEMBLY_TO_ELEVATOR: 
            return  ( self.actual_mask[ ROBOT_IDX['wimpy' ] + FLOOR ] == 1 )\
               and  ( self.actual_mask[ ROBOT_IDX['elev'  ] + FLOOR ] == 1 and not self.actual_mask[ ROBOT_IDX['elev'  ] + DOCKED ] )       

        elif task_id == TaskCode.OLIVIA_GO_HOME: 
            return True

        elif task_id == TaskCode.OLIVIA_CARGO_ASSEMBLY_UP: 
            return True
            # return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
            #    and  ( self.actual_mask[ ROBOT_IDX['olivia'  ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['olivia'  ] + DOCKED ] )\
            #    and  ( self.actual_mask[ ROBOT_IDX['wimpy'   ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['wimpy'   ] + DOCKED ] )#\
               # and not ( self.actual_mask[ CARGO_PNL_UP ]  == 1 )
               
        elif task_id == TaskCode.OLIVIA_CARGO_ASSEMBLY_DOWN: 
            return True
            # return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
            #    and  ( self.actual_mask[ ROBOT_IDX['olivia'  ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['olivia'  ] + DOCKED ] )\
            #    and  ( self.actual_mask[ ROBOT_IDX['wimpy'   ] + FLOOR ] == 1 and self.actual_mask[ ROBOT_IDX['wimpy'   ] + DOCKED ] )#\
               # and  ( self.actual_mask[ CARGO_PNL_UP ]  == 1 )
               
        elif task_id == TaskCode.OLIVIA_FUSELAGE_ASSEMBLY_1:
            return True
            # return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
            #    and  ( self.actual_mask[ ROBOT_IDX['olivia'  ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['olivia'  ] + READY ] )\
            #    and  ( self.actual_mask[ ROBOT_IDX['bluto'   ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['bluto'   ] + READY ] )
               
        elif task_id == TaskCode.OLIVIA_FUSELAGE_ASSEMBLY_2:
            return True
            # return  ( self.actual_mask[ ROBOT_IDX['sweepee' ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['sweepee' ] + DOCKED ] )\
               # and  ( self.actual_mask[ ROBOT_IDX['olivia'  ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['olivia'  ] + READY ] )\
               # and  ( self.actual_mask[ ROBOT_IDX['bluto'   ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['bluto'   ] + READY ] )
            
        elif task_id == TaskCode.POPEYE_FUSELAGE_ASSEMBLY: 
            return  ( self.actual_mask[ ROBOT_IDX['popeye'  ] + FLOOR ] == 2 and self.actual_mask[ ROBOT_IDX['popeye'  ] + READY ] ) #TODO check ready
        
        elif task_id == TaskCode.SWEEPEE_CLAMP_CLOSE:
            return  ( self.actual_mask[ROBOT_IDX['sweepee' ] + DOCKED ])
        
        elif task_id == TaskCode.SWEEPEE_CLAMP_OPEN:
            return  (not self.actual_mask[ROBOT_IDX['sweepee' ] + DOCKED ])
        
        elif task_id == TaskCode.SWEEPEE_DOCK_HERE:
            return  (not self.actual_mask[ROBOT_IDX['sweepee' ] + DOCKED ])
    
    
    def task_availables( self, verbose = False ):
        self.update_actual_mask()
        if verbose: 
            self.print_mask( True )
        
        tasks = [ TaskCode(i) for i in sorted([ int(k.value) for k,v in self.id.items() if self.is_task_available( k ) ]) ]
        if verbose:
            print( "task_availables", tasks )
            
        return tasks
        
    def used_resources( self, tasks, verbose = False ):
        resources = []
        for task in tasks:
            resources.extend( self.resources[ task ]  )

        resources = list(set(resources))            
        if verbose:
            print( "used_resources", resources )
        return resources



class dispatcherClass:
    
    def __init__(self):

        self.emergency = False
        self.elev      = elevatorMod.elevatorClass()
        self.wimpy     = turtlebotMod.turtlebotClass() 
        self.sweepee   = sweepeeMod.sweepeeClass()
        self.popeye    = popeyeMod.popeyeClass()
        
        if len(sys.argv) == 2:
            self.olivia    = oliviaMod.oliviaClass(int(sys.argv[1])) 
        else:
            self.olivia    = oliviaMod.oliviaClass() 
        self.bluto     = blutoMod.blutoClass() 

        
        self.R      = { 'elev'    : self.elev
                      , 'wimpy'   : self.wimpy 
                      , 'sweepee' : self.sweepee
                      , 'popeye'  : self.popeye
                      , 'olivia'  : self.olivia
                      , 'bluto'   : self.bluto }
        try:
            self.task = { TaskCode.ELEVATOR_FROM_0_TO_1                               : self.move_elevator_from_0_to_1                                 
                        , TaskCode.ELEVATOR_FROM_1_TO_2                               : self.move_elevator_from_1_to_2                                 
                        , TaskCode.ELEVATOR_FROM_0_TO_2                               : self.move_elevator_from_0_to_2                                 
                        , TaskCode.ELEVATOR_FROM_1_TO_0                               : self.move_elevator_from_1_to_0                                 
                        , TaskCode.ELEVATOR_FROM_2_TO_0                               : self.move_elevator_from_2_to_0                        
                        , TaskCode.ELEVATOR_FROM_2_TO_1                               : self.move_elevator_from_2_to_1 
                        , TaskCode.CARGO_ALL_PARALLEL                                 : self.cargo_all_parallel                        
                        , TaskCode.SWEEPEE_FROM_HOME_TO_OLIVIA                        : self.move_sweepee_from_home_to_olivia                          
                        , TaskCode.SWEEPEE_FROM_HOME_TO_POPEYE                        : self.move_sweepee_from_home_to_popeye                          
                        , TaskCode.SWEEPEE_FROM_HOME_TO_BLUTO                         : self.move_sweepee_from_home_to_bluto                           
                        , TaskCode.SWEEPEE_FROM_ELEVATOR_TO_HOME                      : self.move_sweepee_from_elevator_to_home 
                        , TaskCode.SWEEPEE_FROM_HOME_TO_ELEVATOR                      : self.move_sweepee_from_home_to_elevator  
                        , TaskCode.SWEEPEE_OLIVIA_FROM_HOME_TO_ELEVATOR_1             : self.move_sweepee_olivia_from_home_to_elevator_1               
                        , TaskCode.SWEEPEE_OLIVIA_FROM_HOME_TO_ELEVATOR_2             : self.move_sweepee_olivia_from_home_to_elevator_2               
                        , TaskCode.SWEEPEE_POPEYE_FROM_HOME_TO_ELEVATOR               : self.move_sweepee_popeye_from_home_to_elevator                
                        , TaskCode.SWEEPEE_BLUTO_FROM_HOME_TO_ELEVATOR                : self.move_sweepee_bluto_from_home_to_elevator                  
                        , TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_HOME               : self.move_sweepee_olivia_from_elevator_to_home                 
                        , TaskCode.SWEEPEE_POPEYE_FROM_ELEVATOR_TO_HOME               : self.move_sweepee_popeye_from_elevator_to_home                 
                        , TaskCode.SWEEPEE_BLUTO_FROM_ELEVATOR_TO_HOME                : self.move_sweepee_bluto_from_elevator_to_home                  
                        , TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_CARGO_ASSEMBLY     : self.move_sweepee_olivia_from_elevator_to_cargo_assembly    
                        , TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_CARGO_PARALLEL     : self.move_sweepee_olivia_from_elevator_to_cargo_parallel 
                        , TaskCode.SWEEPEE_OLIVIA_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY  : self.move_sweepee_olivia_from_elevator_to_fuselage_assembly    
                        , TaskCode.SWEEPEE_OLIVIA_FROM_CARGO_ASSEMBLY_TO_ELEVATOR     : self.move_sweepee_olivia_from_cargo_assembly_to_elevator       
                        , TaskCode.SWEEPEE_OLIVIA_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR  : self.move_sweepee_olivia_from_fuselage_assembly_to_elevator    
                        , TaskCode.SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY  : self.move_sweepee_popeye_from_elevator_to_fuselage_assembly    
                        , TaskCode.SWEEPEE_POPEYE_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR  : self.move_sweepee_popeye_from_fuselage_assembly_to_elevator    
                        , TaskCode.SWEEPEE_BLUTO_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY   : self.move_sweepee_bluto_from_elevator_to_fuselage_assembly     
                        , TaskCode.SWEEPEE_BLUTO_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR   : self.move_sweepee_bluto_from_fuselage_assembly_to_elevator   
                        , TaskCode.SWEEPEE_FROM_FUSELAGE_ASSEMBLY_TO_ELEVATOR         : self.move_sweepee_from_fuselage_assembly_to_elevator
                        , TaskCode.WIMPY_FROM_HOME_TO_ELEVATOR                        : self.move_wimpy_from_home_to_elevator                          
                        , TaskCode.WIMPY_FROM_ELEVATOR_TO_CARGO_ASSEMBLY              : self.move_wimpy_from_elevator_to_cargo_assembly                
                        , TaskCode.WIMPY_FROM_ELEVATOR_TO_HOME                        : self.move_wimpy_from_elevator_to_home                          
                        , TaskCode.WIMPY_FROM_CARGO_ASSEMBLY_TO_ELEVATOR              : self.move_wimpy_from_cargo_assembly_to_elevator  
                        , TaskCode.OLIVIA_GO_HOME                                     : self.olivia_go_home
                        , TaskCode.OLIVIA_CARGO_ASSEMBLY_UP                           : self.olivia_cargo_assembly_up  
                        , TaskCode.OLIVIA_CARGO_ASSEMBLY_DOWN                         : self.olivia_cargo_assembly_down
                        , TaskCode.OLIVIA_FUSELAGE_ASSEMBLY_1                         : self.olivia_fuselage_assembly_1                             
                        , TaskCode.OLIVIA_FUSELAGE_ASSEMBLY_2                         : self.olivia_fuselage_assembly_2  
                        , TaskCode.POPEYE_FUSELAGE_ASSEMBLY                           : self.popeye_fuselage_assembly 
                        , TaskCode.SWEEPEE_CLAMP_CLOSE                                : self.sweepee_clamp_close
                        , TaskCode.SWEEPEE_CLAMP_OPEN                                 : self.sweepee_clamp_open
                        , TaskCode.SWEEPEE_DOCK_HERE                                  : self.sweepee_dock_here }
        except Exception as e: 
            print(e)
            sys.exit(-1)

            
        self.__task_state  = { k : TaskState.IDLE for k,v in self.task.items() }
        self.__task_thread = { k : [None,None]    for k,v in self.task.items() }
        self.emergency = False
        self.emer_sub = rospy.Subscriber("/joy", Joy, callback=self.emergency_call)
        
        ##SIMFAL Publishers
        self.simfal_start_time     = rospy.Publisher("/simfal/start_time"      ,String,latch = True,queue_size=1)
        self.simfal_start_time.publish(String(str(datetime.datetime.now())))
        self.simfal_task_start     = rospy.Publisher("/simfal/task_start_time" ,String,latch = True,queue_size=1)
        self.simfal_task_list      = rospy.Publisher("/simfal/possible_tasks"  ,String,latch = True,queue_size=1)
        self.simfal_task_active    = rospy.Publisher("/simfal/active_tasks"    ,String,latch = True,queue_size=1)
        self.simfal_popeye_status  = rospy.Publisher("/simfal/popeye_status"   ,Int8,latch = True,queue_size=1) 
        self.simfal_olivia_status  = rospy.Publisher("/simfal/olivia_status"   ,Int8,latch = True,queue_size=1) 
        self.simfal_sweepee_status = rospy.Publisher("/simfal/sweepee_status"  ,Int8,latch = True,queue_size=1) 
        self.simfal_wimpy_status   = rospy.Publisher("/simfal/wimpy_status"    ,Int8,latch = True,queue_size=1) 
        self.simfal_panel_reset    = rospy.Publisher("/simfal/panel_reset"     ,Int8,latch = True,queue_size=1) 
        
        rospy.set_param("/tasks/sidewall_panel_1/done" ,False)
        rospy.set_param("/tasks/sidewall_panel_2/done" ,False)
        rospy.logwarn("asdasdasd")

    def idfn( self, fcn_name ):
        fcn_map = { v.__name__ : k for k,v in self.task }
        return fcn_map[ fcn_name ]
    

        
    def emergency_call(self,data):
        self.emergency = data.buttons[6] or data.buttons[7]  or self.emergency
        if self.emergency:
            self.emer_sub.unregister()
            rospy.logerr("SAFETY BUTTON PRESSED")
            print(data.buttons)
            rospy.logerr("SAFETY BUTTON PRESSED")
            self.emergency = False
            if hasattr(self,"wimpy")   and hasattr(self,"wimpy.emergency"  ):  self.wimpy.emergency   = True
            if hasattr(self,"sweepee") and hasattr(self,"sweepee.emergency"):  self.sweepee.emergency = True 
            if hasattr(self,"elev")    and hasattr(self,"elev.emergency"   ):  self.elev.emergency    = True 
            if hasattr(self,"olivia")  and hasattr(self,"olivia.emergency" ):  self.olivia.emergency  = True 
            if hasattr(self,"popeye")  and hasattr(self,"popeye.emergency" ):  self.popeye.emergency  = True
            os.kill(os.getpid(),signal.SIGINT)
            # raise KeyboardInterrupt

    def tasks_running( self, verbose = False ):
        tasks = [ k for k,v in self.__task_state.items() if v == TaskState.RUNNING ]
        if verbose:
            print( tasks )
        
        return tasks 
    

    def purge_unavailable_tasks( self, fsm, tasks, busy_resources, verbose = False ):
        
        _busy_resources = []
        for busy_resource in busy_resources:
            _busy_resources.append(busy_resource)    
            if busy_resource == 'elev':
                if self.elev.docked_obj == "wimpy":  
                    _busy_resources.append( "wimpy" )        
                elif self.elev.docked_obj == "sweepee":  
                    _busy_resources.append( "sweepee" )      
                    if self.sweepee.docked_obj  != "none": 
                        _busy_resources.append( self.sweepee.docked_obj )        
        
        tasks = [ task for task in tasks if  (set(fsm.resources[task]) - set(_busy_resources) ) == set(fsm.resources[task]) ]
        if verbose:
            print("purge_unavailable_tasks", tasks)
        return tasks


            
    def catch_function_result( self, tc, tt ):
        self.__task_state[tc] = tt.join( )

    def exec_function( self, blocking, tc  ):
                    
        self.__task_state[ tc ] = TaskState.RUNNING
        if blocking:
            self.__task_state[ tc ] = self.task[ tc ]( )
        else:
            self.__task_thread[tc][0] = ThreadWithReturnValue(target = self.task[ tc ]  )
            self.__task_thread[tc][0].start()
            self.__task_thread[tc][1] = Thread(target = self.catch_function_result, args=(tc,self.__task_thread[tc][0], ) )
            self.__task_thread[tc][1].start()
            
        return self.__task_state[ tc ]


#############################################################
## move elevator 
#############################################################
    
    def  __move_elevator_from_to( self, from_floor, to_floor ):
        result = True
        if from_floor == 0 and to_floor == 1: result = self.elev.move_first()
        if from_floor == 2 and to_floor == 1: result = self.elev.move_first()
        if from_floor == 1 and to_floor == 2: result = self.elev.move_second()
        if from_floor == 0 and to_floor == 2: result = self.elev.move_second()
        if from_floor == 1 and to_floor == 0: result = self.elev.move_zero()
        if from_floor == 2 and to_floor == 0: result = self.elev.move_zero()
        try:
            if result:
                self.R['elev'].status_update(floor=to_floor)
                if (self.elev.docked_obj  != "none"):
                    self.R[self.elev.docked_obj].status_update(floor=to_floor) 
                    if self.elev.docked_obj  == "sweepee" and to_floor==0:
                        self.sweepee.change_map_srv(0)                                       
                        self.sweepee.initial_pose_sweepee_0_return()
                    elif self.elev.docked_obj  == "sweepee" and to_floor==1: 
                        self.sweepee.change_map_srv(1)                                       
                        self.sweepee.initial_pose_sweepee_1()    
                    elif self.elev.docked_obj  == "sweepee" and to_floor==2: 
                        self.sweepee.change_map_srv(2)                                       
                        self.sweepee.initial_pose_sweepee_2()    
                        
                if (self.elev.docked_obj  == "wimpy"):
                    self.wimpy.initial_pose_turtlebot_1
                    
                if (self.elev.docked_obj == "sweepee" and self.sweepee.docked_obj  != "none"):
                    self.R[self.sweepee.docked_obj].status_update(floor=to_floor) 
        except Exception as e: print(e)

        return TaskState.DONE if result else TaskState.SYS_EXIT
    
    def move_elevator_from_0_to_1( self ): 
        return self.__move_elevator_from_to( 0, 1 )   
    
    def move_elevator_from_1_to_2( self ): 
        return self.__move_elevator_from_to( 1, 2 )   
        
    def move_elevator_from_0_to_2( self ): 
        return self.__move_elevator_from_to( 0, 2 )          
        
    def move_elevator_from_1_to_0( self ): 
         return self.__move_elevator_from_to( 1, 0 )  
        
    def move_elevator_from_2_to_0( self ): 
         return self.__move_elevator_from_to( 2, 0 )  
     
    def move_elevator_from_2_to_1( self ): 
        return self.__move_elevator_from_to( 2, 1 )   
   
    
## end move elevator ########################################
#############################################################  
     
        
        

#############################################################
## move move_sweepee_from_home_to 
#############################################################
    def __dock_sweepee_from_home_to(self,cart): #TODO gestione pulsante emergenza
        self.simfal_sweepee_status.publish(Int8(1))
        status_sweepee = self.sweepee.get_status()
        rospy.logwarn("pd1")
        if status_sweepee["docked_obj"] != "none":
            rospy.logwarn("Already docked to '%s' !!!",status_sweepee["docked_obj"])
            return TaskState.REPEAT
        
        status_cart = self.R[cart].get_status() #TODO
        sweepee_pos = self.R['sweepee']._get_sweepee_pos()
        #TODO controlla distanza non esagerata
        if status_sweepee["floor"] != status_cart["floor"]:
            rospy.warn(" Sweepee floor and cart floor don't match !!!")
            return TaskState.REPEAT
        rospy.logwarn("pd4")
        result = self.sweepee._task_dock(status_cart["floor"], cart, status_cart["docking_position"])
        if result:
            sweepee_pos = self.R['sweepee']._get_sweepee_pos()
            self.R[cart].status_update(  floor=status_cart["floor"], position= sweepee_pos, docked = True, ready = False)
            self.simfal_sweepee_status.publish(Int8(0))
            return TaskState.DONE
        else:
            self.simfal_sweepee_status.publish(Int8(3))
            rospy.logerr("Failed in docking")
            return TaskState.REPEAT
        
    def move_sweepee_from_home_to_olivia                         ( self ): 
        rospy.logerr("CAMBIA TUBO\nCAMBIA TUBO\nCAMBIA TUBO\nCAMBIA TUBO\nCAMBIA TUBO\n")
        return self.__dock_sweepee_from_home_to("olivia")
        
    def move_sweepee_from_home_to_popeye                         ( self ): 
        return self.__dock_sweepee_from_home_to("popeye")
        
    def move_sweepee_from_home_to_bluto                          ( self ): 
        return self.__dock_sweepee_from_home_to("bluto")

## end move_sweepee_from_home_to ###########################
############################################################
        
############################################################    
## CARGO PARALLEL
#############################################################

    def cargo_all_parallel(self):
        pass

#############################################################


############################################################    
## __move_sweepee_from_home_to_elevator 
#############################################################
    def __move_sweepee_from_home_to_elevator( self, to_floor ): 
        self.simfal_sweepee_status.publish(Int8(1))
        result = self.sweepee._level_zero_task_goelev(to_floor)
        if result:
            self.elev.status_update(docked_obj="sweepee")
            if self.sweepee.get_status()['docked_obj'] != "none":
                self.R[self.sweepee.get_status()['docked_obj']].status_update(position= self.sweepee._get_sweepee_pos() )
                self.simfal_sweepee_status.publish(Int8(0))
        return TaskState.DONE if result else TaskState.REPEAT
    
    def move_sweepee_olivia_from_home_to_elevator_1 ( self ): 
        return self.__move_sweepee_from_home_to_elevator(1)
    
    def move_sweepee_olivia_from_home_to_elevator_2 ( self ): 
        return self.__move_sweepee_from_home_to_elevator(2)
    
    def move_sweepee_popeye_from_home_to_elevator ( self ): 
        return self.__move_sweepee_from_home_to_elevator(2)
        
    def move_sweepee_bluto_from_home_to_elevator ( self ): 
        return self.__move_sweepee_from_home_to_elevator(2)
    
    def move_sweepee_from_home_to_elevator ( self ):
        return self.__move_sweepee_from_home_to_elevator(2)
    
## end move_sweepee_from_home_to ###########################
############################################################
    
    
############################################################    
## move_sweepee_from_elevator_to_home 
#############################################################
    def move_sweepee_from_elevator_to_home                       ( self ): 
        self.simfal_sweepee_status.publish(Int8(1))
        result = self.sweepee._level_zero_task_return()
        if result:
            self.elev.status_update(docked_obj="none")
            self.simfal_sweepee_status.publish(Int8(0))
        return TaskState.DONE if result else TaskState.REPEAT
    
    def move_sweepee_olivia_from_elevator_to_home                ( self ): 
        return self.move_sweepee_from_elevator_to_home()
    
    def move_sweepee_popeye_from_elevator_to_home                ( self ): 
        return self.move_sweepee_from_elevator_to_home()
            
    def move_sweepee_bluto_from_elevator_to_home                 ( self ): 
        return self.move_sweepee_from_elevator_to_home()
## end - move_sweepee_from_elevator_to_home ################
############################################################ 
          

    
#############################################################
## from elevator to fuselage assembly 
#############################################################
    def __move_sweepee_from_elevator_to_fuselage_assembly( self, cart ): 
        result = True
        self.simfal_sweepee_status.publish(Int8(1))
        result = self.sweepee._level_two_enter( )
        if result:
            self.elev.status_update(docked_obj="none")
            self.R[cart].status_update(position= self.sweepee._get_sweepee_pos() )
        else:
            return TaskState.REPEAT
        result = True
        print("docked::",self.sweepee.docked_obj)
        
        if self.sweepee.docked_obj == "popeye" and result:
            result = self.sweepee._level_two_leave_popeye()
            self.R[cart].status_update(position= self.sweepee._get_sweepee_pos(), ready = True ) if result else False
            
        elif self.sweepee.docked_obj == "olivia" and result:
            if not rospy.get_param("/tasks/sidewall_panel_1/done"):
                target_pose = rospy.get_param("/"+cart+"/goal/fuse_1")
                result = self.sweepee.sweepee_move_forw(1.02,0.2)
                result = self.sweepee._cart_attach_srv("close") 
                self.sweepee.status_update(docked_obj="none")
                if result:
                    self.R[cart].status_update(position= self.sweepee.position, docked = False, ready = True,docking_position = self.sweepee._get_sweepee_pos() )
                    
        elif self.sweepee.docked_obj == "bluto" and result:
            if not rospy.get_param("/tasks/sidewall_panel_1/done"):
                target_pose = rospy.get_param("/"+cart+"/goal/fuse_1")
                result = self.sweepee._level_two_leave( target_pose, cart , release=True , go_back=True  )
                
                if result:
                    self.R[cart].status_update(position= self.sweepee._get_sweepee_pos(), docked = False, ready = True )
            else:
                target_pose = rospy.get_param("/"+cart+"/goal/fuse_2")
                result = self.sweepee._level_two_leave( target_pose, cart , release=True , go_back=False  )
                
                if result:
                    self.R[cart].status_update(position= self.sweepee._get_sweepee_pos(), docked = False, ready = True )

        self.simfal_sweepee_status.publish(Int8(0))
        return TaskState.DONE if result else TaskState.REPEAT
    
    def move_sweepee_olivia_from_elevator_to_fuselage_assembly   ( self ): 
        return self.__move_sweepee_from_elevator_to_fuselage_assembly( "olivia" )
        
    def move_sweepee_popeye_from_elevator_to_fuselage_assembly   ( self ): 
        ret = self.__move_sweepee_from_elevator_to_fuselage_assembly( "popeye" )
        return ret
            
    def move_sweepee_bluto_from_elevator_to_fuselage_assembly    ( self ): 
        return self.__move_sweepee_from_elevator_to_fuselage_assembly( "bluto" )
## end - from elevator to fuselage assembly #################
#############################################################
    
    
#############################################################
## from CARGO TO ELEVATOR 
#############################################################

    def move_sweepee_olivia_from_elevator_to_cargo_parallel      ( self ): 
        self.simfal_sweepee_status.publish(Int8(1))
        rospy.set_param("/sweepee/cargo/safe",False)
        par_th = Thread(target = self.sweepee._level_one_task_enter)
        rospy.logwarn("th1")
        par_th.start()
        rospy.logwarn("")
        while not rospy.get_param("/sweepee/cargo/safe"):
            rospy.logwarn("waiting for safe")
            rospy.sleep(1)
        self.elev.status_update(docked_obj='none')
        rospy.logwarn("calle elv")
        result = self.move_elevator_from_1_to_0()
        rospy.logwarn("after elev")
        par_th.join()
        if result:
            self.elev.status_update(docked_obj="none")
            self.olivia.status_update(position= self.sweepee._get_sweepee_pos(), ready = True )
            self.simfal_sweepee_status.publish(Int8(0))
        return TaskState.DONE if result else TaskState.REPEAT   
     
    def move_sweepee_olivia_from_elevator_to_cargo_assembly      ( self ): 
        self.simfal_sweepee_status.publish(Int8(1))
        result = self.sweepee._level_one_task_enter( )
        if result:
            self.elev.status_update(docked_obj="none")
            self.olivia.status_update(position= self.sweepee._get_sweepee_pos(), ready = True )
            self.simfal_sweepee_status.publish(Int8(0))
        return TaskState.DONE if result else TaskState.REPEAT

        
    def move_sweepee_olivia_from_cargo_assembly_to_elevator      ( self ):
        self.simfal_sweepee_status.publish(Int8(1))
        result = self.sweepee._level_one_task_exit( )
        if result:
            self.elev.status_update(docked_obj="sweepee")
            self.olivia.status_update(position= self.sweepee._get_sweepee_pos(), ready=False )
            self.simfal_sweepee_status.publish(Int8(0))
        return TaskState.DONE if result else TaskState.REPEAT
## END from CARGO TO ELEVATOR ###############################
#############################################################
    
    
#############################################################
## from fuselage assembly to elevator 
#############################################################
    def __move_sweepee_from_fuselage_assembly_to_elevator   ( self, cart ):
        self.simfal_sweepee_status.publish(Int8(1))
        result = self.sweepee._level_two_exit_to_elev( )
        if result:
            self.elev.status_update(docked_obj="sweepee")
            if cart != "none":
                self.R[cart].status_update(position= self.sweepee._get_sweepee_pos(), ready = False )
            self.simfal_sweepee_status.publish(Int8(0))
        return TaskState.DONE if result else TaskState.REPEAT

    def move_sweepee_from_fuselage_assembly_to_elevator(self):
        return self.__move_sweepee_from_fuselage_assembly_to_elevator("none")
   
    def move_sweepee_olivia_from_fuselage_assembly_to_elevator   ( self ):
        return self.__move_sweepee_from_fuselage_assembly_to_elevator("olivia")
          
    def move_sweepee_popeye_from_fuselage_assembly_to_elevator   ( self ): 
        return self.__move_sweepee_from_fuselage_assembly_to_elevator("popeye")
    
    def move_sweepee_bluto_from_fuselage_assembly_to_elevator    ( self ): 
        return self.__move_sweepee_from_fuselage_assembly_to_elevator("bluto")
## end - from fuselage assembly to elevator #################
#############################################################
        

        
#############################################################
## from cargo assembly to elevator 
#############################################################
    def move_wimpy_from_home_to_elevator                         ( self ): 
        self.simfal_wimpy_status.publish(Int8(1))
        result = self.wimpy.level_zero_task_init()
        if result:
            print("Setting wimpty on elev")
            self.elev.status_update(docked_obj="wimpy")
            self.simfal_wimpy_status.publish(Int8(0))
        return TaskState.DONE if result else TaskState.REPEAT
        
    def move_wimpy_from_elevator_to_cargo_assembly               ( self ): 
        self.simfal_wimpy_status.publish(Int8(1))
        result = self.wimpy.level_one_task_enter()
        if result:
            self.wimpy.status_update(ready = True)
            self.elev.status_update(docked_obj = "none")
            self.simfal_wimpy_status.publish(Int8(0))
        return TaskState.DONE if result else TaskState.REPEAT
        
    def move_wimpy_from_elevator_to_home                         ( self ): 
        self.simfal_wimpy_status.publish(Int8(1))
        result = self.wimpy.level_zero_task_end()
        if result:
            self.elev.status_update(docked_obj="none")
            self.simfal_wimpy_status.publish(Int8(0))
        return TaskState.DONE if result else TaskState.REPEAT
        
    def move_wimpy_from_cargo_assembly_to_elevator               ( self ): 
        self.simfal_wimpy_status.publish(Int8(1))
        result = self.wimpy.level_one_task_exit()
        if result:
            self.wimpy.status_update(ready = False)
            self.simfal_wimpy_status.publish(Int8(0))
        return TaskState.DONE if result else TaskState.REPEAT
        
## end fromcargo assembly to elevator 
#############################################################
            
            
            
            
#############################################################
## assembly calls
#############################################################           
        
    def olivia_go_home(self):
        result = True
        self.simfal_olivia_status.publish(Int8(1))
        rospy.set_param("/tasks/cargo_upper_panel_1/grasp",False)
        rospy.set_param("/tasks/cargo_upper_panel_1/done" ,False)
        rospy.set_param("/tasks/olivia/done" ,False)
        rospy.set_param("/tasks/move_wimpy",False)

        if self.olivia.floor == 1:
            rospy.logwarn("go home cargo")
            result = self.olivia.cargo_go_home.start()
        else:
            rospy.logwarn("go home fuselage")
            result = self.olivia.fuselage_go_home.start()
        rospy.logwarn("CARGO ASSEMBLY UP FINISHED")
        rospy.sleep(5)    
        while not rospy.is_shutdown() and not rospy.get_param("/tasks/olivia/done"):
            rospy.logwarn("going on")
            rospy.sleep(0.5)  
        rospy.sleep(2)   
        if self.olivia.floor == 1:
            result = self.olivia.cargo_go_home.stop()
        else:
            result = self.olivia.fuselage_go_home.stop()
        rospy.sleep(2)
        while not rospy.get_param("/tasks/olivia/done") and not rospy.is_shutdown():
            rospy.sleep(0.5)
            
        if result:
            self.simfal_olivia_status.publish(Int8(4))
        else:
            self.simfal_olivia_status.publish(Int8(3))
            
        return TaskState.DONE if result else TaskState.REPEAT
    
    
    def olivia_cargo_assembly_up                               ( self ): 
        result = True
        raw_input("lancia ghidini e rosclean")
        self.simfal_olivia_status.publish(Int8(1))
        self.simfal_panel_reset.publish(Int8(2))
        rospy.set_param("/tasks/cargo_upper_panel_1/grasp",False)
        rospy.set_param("/tasks/cargo_upper_panel_1/done" ,False)
        rospy.set_param("/tasks/olivia/done" ,False)
        rospy.set_param("/tasks/move_wimpy",False)
        rospy.logwarn("Turtle thread started")
        par_th = Thread(target = self.wimpy.cargo_move_back,args=(1,))
        par_th.start()
        rospy.logwarn("launching cargo assembly up")
        result = self.olivia.cargo_assembly_up()
        
        while not rospy.get_param("/tasks/olivia/done") and not rospy.is_shutdown():
            rospy.sleep(0.5)
            
        if result:
            self.simfal_olivia_status.publish(Int8(4))
        else:
            self.simfal_olivia_status.publish(Int8(3))
            
        return TaskState.DONE if result else TaskState.REPEAT
    
    def olivia_cargo_assembly_down                             ( self ): 
        result = True
        raw_input("lancia ghidini e rosclean")
        self.simfal_olivia_status.publish(Int8(1))
        self.simfal_panel_reset.publish(Int8(3))
        rospy.set_param("/tasks/cargo_lower_panel_1/grasp",False)
        rospy.set_param("/tasks/cargo_lower_panel_1/done" ,False)
        rospy.set_param("/tasks/olivia/done" ,False)
        rospy.set_param("/tasks/move_wimpy",False)
        rospy.set_param("/tasks/move_wimpy_forward",False)
        rospy.logwarn("turtle moving thread creatin")
        par_thf = Thread(target = self.wimpy.cargo_move_forw )
        par_thf.start()
        rospy.logwarn("cargo assembly down start ")
        par_th = Thread(target = self.wimpy.cargo_move_back,args=(2,) )
        par_th.start()
        result = self.olivia.cargo_assembly_down()
        
        while not rospy.get_param("/tasks/olivia/done") and not rospy.is_shutdown():
            rospy.sleep(0.5)
            
        if result:
            self.simfal_olivia_status.publish(Int8(4))
        else:
            self.simfal_olivia_status.publish(Int8(3))
            
        return TaskState.DONE if result else TaskState.REPEAT


    def olivia_fuselage_assembly_1                            ( self ): 
        result = True
        raw_input("lancia ghidini e rosclean")
        self.simfal_olivia_status.publish(Int8(1))
        self.simfal_panel_reset.publish(Int8(1))
        rospy.set_param("/tasks/sidewall_panel_1/grasp",False)
        rospy.set_param("/tasks/sidewall_panel_1/done" ,False)
        rospy.set_param("/tasks/olivia/done" ,False)

        result = self.olivia.fuselage_assembly_1()
        
        while not rospy.get_param("/tasks/olivia/done") and not rospy.is_shutdown():
            rospy.sleep(0.5)
            
        if result:
            self.simfal_olivia_status.publish(Int8(4))
        else:
            self.simfal_olivia_status.publish(Int8(3))

        
        return TaskState.DONE if result else TaskState.REPEAT

    def olivia_fuselage_assembly_2                            ( self ): 
        result = True
        raw_input("lancia ghidini e rosclean")
        self.simfal_olivia_status.publish(Int8(1))
        self.simfal_panel_reset.publish(Int8(1))
        rospy.set_param("/tasks/sidewall_panel_1/grasp",False)
        rospy.set_param("/tasks/sidewall_panel_1/done" ,False)
        rospy.set_param("/tasks/olivia/done" ,False)
        
        
        result = self.olivia.fuselage_assembly_2()
        
        while not rospy.get_param("/tasks/olivia/done") and not rospy.is_shutdown():
            rospy.sleep(0.5)
            
        if result:
            self.simfal_olivia_status.publish(Int8(4))
        else:
            self.simfal_olivia_status.publish(Int8(3))
            
        return TaskState.DONE if result else TaskState.REPEAT
    
    def popeye_fuselage_assembly                              ( self ):  
        
        self.simfal_popeye_status.publish(Int8(1))
        popeye_launcher = remote_popeye()

        
        try:    
            # print("waiting service ")
            # rospy.wait_for_service('/popeye_configuration_manager/start_configuration')
            raw_input("Press to start")
            popeye_launcher.start()
            
            
            # rospy.sleep(0.5)
            # print("Service Arrived")
            # popeye_configure_scene_srv = rospy.ServiceProxy('/popeye/popeye_context_manager/configure_scene', configure_planning_scene)
            # pose_1 = Pose()
            # pose_2 = Pose()
            # pose_1.position.x = 0.013
            # pose_1.position.y = -0.792
            # pose_1.position.z = 0.908
            # pose_1.orientation.x = 0.0
            # pose_1.orientation.y = -0.398
            # pose_1.orientation.z =  0.918
            # pose_1.orientation.w = 0.0
            # pose_2.position.x = 0.0
            # pose_2.position.y = -1.950
            # pose_2.position.z = 0.049
            # pose_2.orientation.x = 0.0
            # pose_2.orientation.y = -0.0
            # pose_2.orientation.z =  0.0
            # pose_2.orientation.w = 1.0
            # result = popeye_configure_scene_srv(pose_1,pose_2)
            # print("Service ruslt : ",result)2
            
            # raw_input("ICP DOCKING")
            # result = self.sweepee.dock_popeye_call.start()                                 
            # rospy.set_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/status","none")
            # time_watch = time.time()
            # while True:
            #     rospy.sleep(0.5)
            #     if not rospy.has_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/status"):
            #         continue
            #     elif rospy.get_param("/tasks/SWEEPEE_POPEYE_FROM_ELEVATOR_TO_FUSELAGE_ASSEMBLY/status") == "dock_popeye_done" :
            #         break
            #     elif (time.time() - time_watch) > 25.0 :
            #         print("Time exceedee for popeye docking")
            #         return False
            # result = self.sweepee.dock_popeye_call.stop()  
            

            # raw_input("clamp close")
            # self.sweepee_clamp_close()
            
            while not rospy.is_shutdown():
                rospy.sleep(1)
                rospy.logwarn("Assembling")
                
        except Exception as e:
            print("deleting popeye remote exception")
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            popeye_launcher.stop()
        finally:
            print("deleting popeye remote finish")
            popeye_launcher.stop()
        
        return TaskState.DONE


## end assembly calls
#############################################################               
    
    
#############################################################
## utility calls
#############################################################            
    def sweepee_clamp_open ( self ): 
        currents = rospy.get_param("/" + self.sweepee.docked_obj + "/param/docking_currents" ) if self.sweepee.docked_obj != "none" else [2.0, 2.0]
        result = self.sweepee._cart_attach_srv("open",  currents    )
        if not result:
            return TaskState.REPEAT  
        return TaskState.DONE
        
    def sweepee_clamp_close ( self ):
        result = self.sweepee._cart_attach_srv("close")
        if not result:
            return TaskState.REPEAT  
        return TaskState.DONE

    def sweepee_dock_here  (self):
        result = self.sweepee._dock_here()
        if not result:
            return TaskState.REPEAT  
        return TaskState.DONE
            

## end assembly calls
#############################################################    
    
    
    
    def run(self, fsm):
        self.simfal_task_list.publish(String(listToString(fsm.id.values())))
        
        while True and not self.emergency and not rospy.is_shutdown():
            # os.system('clear')
            
            print('\n\n\n\n\n\n\n\n')
            
            tasks_running   = self.tasks_running(verbose = False )
            busy_resources  = fsm.used_resources( tasks_running, verbose = False )
            tasks_available = list(set(fsm.task_availables( verbose = False  )) - set(tasks_running))
            tasks_available = self.purge_unavailable_tasks( fsm, tasks_available, busy_resources, verbose = False  )
            
            if len(tasks_available) == 0:
                 raw_input("Finished! press any key to continue")
                 break
            # task_available  = [elem for elem in tasks_running if elem not in task_available ]
            
            message = "Task Available: \n"
            for i, task in enumerate( tasks_available ):
                message = message + str( i ) + ". " + fsm.id[ task ] + "\n"
        
            message = message + ">>"
            print(message)
            while True and not self.emergency and not rospy.is_shutdown():
                try:
                    val = select.select([sys.stdin],[], [], 1)[0]
                    if val:
                        task_selected = int(sys.stdin.readline().rstrip())
                        print(task_selected)
                        break
                    else:
                        time.sleep(0.1)
                except :
                    print( "Error " )
                    continue
                    
            print("<< Task Selected: " + str( task_selected ) + ",\
                  Function: " + self.task[ tasks_available[ task_selected ] ].__name__ )
            self.simfal_task_start.publish(String(str(datetime.datetime.now())))
            commm = str(tasks_available[ task_selected ])[9:]
            print(commm)
            simstr = str(commm) + "," + str(datetime.datetime.now())
            self.simfal_task_active.publish(simstr)
            blocking = True
            #TODO gestire running
            while True and not self.emergency:
                result = self.exec_function(blocking, tasks_available[ task_selected ] )
                if result == TaskState.DONE:
                    asd = raw_input("Done, press any key to continue")
                    break
                elif result == TaskState.REPEAT:
                    asd = raw_input("Failed. Try to repeat, press any key to continue")
                elif result == TaskState.RUNNING:
                    break
                else:
                    asd = raw_input("Failed. Press any key to exit")
                    return -1
    
        return 0        
            
            


def on_shut():
    global dispatcher
    try:
        
        dispatcher.emergency = True
        try: # WIMPY!
            dispatcher.wimpy.emergency = True
            if dispatcher.wimpy._HOST_UP:
                dispatcher.wimpy.move_base_action.cancel_all_goals()
                dispatcher.wimpy.pub_no_vel()
                dispatcher.wimpy.shutdown()
        except:
            print (" Wimpy unexpected error:", sys.exc_info()[0])
        finally:
            if dispatcher.wimpy._HOST_UP:
                print("Del Wimpy")
                del dispatcher.wimpy
                
        try:# SWEEPEE!
            dispatcher.sweepee.emergency = True
            if dispatcher.sweepee._HOST_UP:
                dispatcher.sweepee.move_base_action.cancel_all_goals()
                dispatcher.sweepee.pub_no_vel()
                dispatcher.sweepee.shutdown()
        except:
            print (" Sweepee unexpected error:", sys.exc_info()[0])
        finally: 
            if dispatcher.sweepee._HOST_UP:                   
                dispatcher.sweepee.dock_popeye_call.stop()
                dispatcher.sweepee.line_both_caller.stop()
                dispatcher.sweepee.usb_tag_dock.stop()
                dispatcher.sweepee.door_dock_launch.stop()
                print("Del Sweepee")
                del dispatcher.sweepee
           
        if dispatcher.olivia.HOST_UP:
            try:# OLIVIA!
                dispatcher.olivia.emergency = True
            except:
                print (" Sweepee unexpected error:", sys.exc_info()[0])
            finally:       
                dispatcher.olivia.remote_olivia.stop()          
                dispatcher.olivia.fuselage_panel1.stop()                 
                dispatcher.olivia.fuselage_panel2.stop()                 
                dispatcher.olivia.cargo_down.stop()                     
                dispatcher.olivia.cargo_up.stop()   
                dispatcher.olivia.cargo_go_home.stop()
                dispatcher.olivia.fuselage_go_home.stop()
                print("Del Olivia")
                del dispatcher.olivia   
            
            
        try:
            dispatcher.elev.emergency = True
            if dispatcher.elev._HOST_UP:
                print("del elev")
                dispatcher.elev.shutdown()
        finally:
            print("Del Elev")
            del dispatcher.elev
            
    finally:
            
        print("Del Popeye")
        del dispatcher.popeye


        print("Del Bluto")
        del dispatcher.bluto

        print ("Del Dipatcher")
        del dispatcher
        print("Bye bye")
    


       
if __name__=="__main__" :
        
    nh = rospy.init_node('dispatcher', anonymous=True,disable_signals=False)    
    
    rospy.on_shutdown(on_shut)
    ret = 0
    try:
    
        fsm        = FSM()
        dispatcher = dispatcherClass()
        ret        =  dispatcher.run(fsm) 
        
    finally:
        ret = -99

        sys.exit(ret)
