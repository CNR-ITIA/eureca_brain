import rospy
import actionlib
import time, math
import os
import numpy as np
from roslaunch import remoteprocess,remote
from eureca_hosts import eureca_hosts
import roslaunch
from threading import Thread
from pexpect import pxssh
import socket
import traceback

os.environ["ROSLAUNCH_SSH_UNKNOWN"] = "1"

class remote_olivia_class:
    def __init__(self):
        self.s = pxssh.pxssh(maxread=100)
        self.s.SSH_OPTS = ("-X")
        hostname = "192.169.0.105"
        username = "eureca"
        password = "eureca"
        self.s.login (hostname, username, password)
        self.s.sendline ('export DISPLAY=localhost:10.0')
        self.s.sendline ("xterm -e 'python /home/eureca/eureca_ws/src/olivia_brain/scripts/olivia_brain_tcp_server.py 2>&1 | tee pd_log.txt' ")
        self.s.prompt()            
        print(self.s.before)
        time.sleep(1)

    def start(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        t_end = time.time() + 0.1
        while time.time() < t_end:
            try:
                self.sock.connect(('192.169.0.105', 10000))
                self.sock.sendall(str(1)) 
            except Exception as e: print(e)
        try:
            self.sock.sendall(str(1)) 
        except Exception as e: print(e)
        

    def stop(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        t_end = time.time() + 0.1
        while time.time() < t_end:
            try:
                self.sock.connect(('192.169.0.105', 10000))
            except Exception as e: print(e)
        try:
            self.sock.sendall(str(5))
        except Exception as e: print(e)
        time.sleep(1)
        
    def close(self):
        try:
            self.s.logout()
        except Exception as e: print(e)
       
class remote_blaise_class:
    def __init__(self):
        self.s = pxssh.pxssh(maxread=100)
        self.s.SSH_OPTS = ("-X")
        hostname = "192.169.0.110"
        username = "blaise"
        password = "blaise"
        self.s.login (hostname, username, password)
        self.s.sendline ('export DISPLAY=localhost:10.0')
        self.s.sendline ("xterm -e 'python /home/blaise/eureca_ws/src/blaise_brain/scripts/blaise_brain_tcp_server.py 2>&1 | tee pd_log.txt' ")
        self.s.prompt()            
        print(self.s.before)
        time.sleep(1)

    def start(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        t_end = time.time() + 0.2
        while time.time() < t_end:
            try:
                self.sock.connect(('192.169.0.110', 10000))
            except Exception as e: print(e)
        try:
            self.sock.sendall(str(1))
        except Exception as e: print(e)
        
        
    def start_actions(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        t_end = time.time() + 0.2
        while time.time() < t_end:
            try:
                self.sock.connect(('192.169.0.110', 10000))
            except Exception as e: print(e)
        try:
            self.sock.sendall(str(3))
        except Exception as e: print(e)
        
        
    def stop_actions(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        t_end = time.time() + 0.2
        while time.time() < t_end:
            try:
                self.sock.connect(('192.169.0.110', 10000))
            except Exception as e: print(e)
        try:
            self.sock.sendall(str(4))
        except Exception as e: print(e)


    def stop(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        t_end = time.time() + 0.2
        while time.time() < t_end:
            try:
                self.sock.connect(('192.169.0.110', 10000))
            except Exception as e: print(e)
        try:
            self.sock.sendall(str(5))
        except Exception as e: print(e)
        time.sleep(1)
        
    def close(self):
        try:
            self.s.logout()
        except Exception as e: print(e)
        
class olivia_fuse_1_class:
    def __init__(self):
        try:
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(self.uuid)
            cli_args = ['/home/stade/dry_run_ws/src/eureca_task_manager/launch/minimal_dispatcher.launch','cabina:=true','start_configuration:=virtual','task:=cabin','component:=sidewall_panel_1','rviz:=false','enter_stop:=false','upload_traj:=false']
            roslaunch_args = cli_args[1:]
            roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]
            self.launch = roslaunch.parent.ROSLaunchParent(self.uuid, roslaunch_file)
        except Exception as e: print(e)

    def start(self):
        try:
            self.launch.start()
        except Exception as e: print(e)
        return True

    def stop(self):
        try:
            self.launch.shutdown()
        except Exception as e: print(e)
        return True

class olivia_fuse_2_class:
    def __init__(self):
        try:
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(self.uuid)
            cli_args = ['/home/stade/dry_run_ws/src/eureca_task_manager/launch/minimal_dispatcher.launch','cabina:=true','start_configuration:=virtual','task:=cabin','component:=sidewall_panel_2','rviz:=false','enter_stop:=false','upload_traj:=false']
            roslaunch_args = cli_args[1:]
            roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]
            self.launch = roslaunch.parent.ROSLaunchParent(self.uuid, roslaunch_file)
        except Exception as e: print(e)   
    def start(self):
        try:
            self.launch.start()
        except Exception as e: print(e)
        return True

    def stop(self):
        self.launch.shutdown()
        return True

class olivia_cargo_up_class:
    def __init__(self):
        try:
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(self.uuid)
            cli_args = ['/home/stade/dry_run_ws/src/eureca_task_manager/launch/minimal_dispatcher.launch','cabina:=false','start_configuration:=virtual_cargo','task:=cargo_upper','component:=cargo_upper_panel_1','rviz:=false','enter_stop:=false','upload_traj:=true']
            roslaunch_args = cli_args[1:]
            roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]
            self.launch = roslaunch.parent.ROSLaunchParent(self.uuid, roslaunch_file)
        except Exception as e: print(e)

    def start(self):
        try:
            self.launch.start()
        except Exception as e: print(e)
        return True

    def stop(self):
        try:
            self.launch.shutdown()
        except Exception as e: print(e)
        return True

class olivia_cargo_bot_class:
    def __init__(self):
        try:
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(self.uuid)
            cli_args = ['/home/stade/dry_run_ws/src/eureca_task_manager/launch/minimal_dispatcher.launch','cabina:=false','start_configuration:=virtual_cargo','task:=cargo_lower','component:=cargo_lower_panel_1','rviz:=false','enter_stop:=false','upload_traj:=true']
            roslaunch_args = cli_args[1:]
            roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]
            self.launch = roslaunch.parent.ROSLaunchParent(self.uuid, roslaunch_file)
        except Exception as e: print(e)

    def start(self):
        try:
            self.launch.start()
        except Exception as e: print(e)
        return True

    def stop(self):
        try:
            self.launch.shutdown()
        except Exception as e: print(e)
        return True

class olivia_cargo_home_class:
    def __init__(self):
        try:
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(self.uuid)
            cli_args = ['/home/stade/dry_run_ws/src/eureca_task_manager/launch/minimal_dispatcher.launch','cabina:=false','start_configuration:=virtual_cargo','task:=go_home_cargo','component:=cargo_lower_panel_1','vision:=false','rviz:=false','enter_stop:=false']
            roslaunch_args = cli_args[1:]
            roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]
            self.launch = roslaunch.parent.ROSLaunchParent(self.uuid, roslaunch_file)
            print(roslaunch_file)
        except Exception as e: print(e)

    def start(self):
        try:
            self.launch.start()
        except Exception as e: print(e)
        return True

    def stop(self):
        try:
            self.launch.shutdown()
        except Exception as e: print(e)
        return True

class olivia_fuse_home_class:
    def __init__(self):
        try:
            self.uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(self.uuid)
            cli_args = ['/home/stade/dry_run_ws/src/eureca_task_manager/launch/minimal_dispatcher.launch','cabina:=true','start_configuration:=virtual','task:=go_home','component:=sidewall_panel_1','vision:=false','rviz:=false','enter_stop:=false']
            roslaunch_args = cli_args[1:]
            roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]
            self.launch = roslaunch.parent.ROSLaunchParent(self.uuid, roslaunch_file)
        except Exception as e: print(e)
        
    def start(self):
        try:
            self.launch.start()
        except Exception as e: print(e)
        return True

    def stop(self):
        try:
            self.launch.shutdown()  
        except Exception as e: print(e)
        return True


class oliviaClass:

    def __init__(self,init=0):
        rospy.logwarn("Creating olivia class")
        
        self.HOST_UP = False
        if init==1:
            self.HOST_UP  = eureca_hosts.check_host(eureca_hosts.NUC_IIWA_IP) and eureca_hosts.check_host(eureca_hosts.CAMERA_IIWA_IP) and init==1
            print("Nuc is up :",eureca_hosts.check_host(eureca_hosts.NUC_IIWA_IP))
            print("Blaise is up :",eureca_hosts.check_host(eureca_hosts.CAMERA_IIWA_IP))
        else:
            rospy.logwarn("NOT CREATING OLIVIA, NO INPUT")

        # self.__delay = eureca_hosts.check_time_host(eureca_hosts.NUC_IIWA_IP) and eureca_hosts.check_time_host(eureca_hosts.CAMERA_IIWA_IP)
        
        self.docking_position = rospy.get_param("/olivia/status/docking_position_init")        
        self.position         = rospy.get_param("/olivia/status/position"    ) if rospy.has_param("/olivia/status/position"    ) else [rospy.get_param("/olivia/status/position_init")]
        self.docked           = rospy.get_param("/olivia/status/docked"      ) if rospy.has_param("/olivia/status/docked"      ) else False
        self.ready            = rospy.get_param("/olivia/status/ready"       ) if rospy.has_param("/olivia/status/ready"       ) else False
        self.empty            = rospy.get_param("/olivia/status/empty"       ) if rospy.has_param("/olivia/status/empty"       ) else True
        self.floor            = rospy.get_param("/olivia/status/floor"       ) if rospy.has_param("/olivia/status/floor"       ) else 0
        self.is_moving        = rospy.get_param("/olivia/status/is_moving"   ) if rospy.has_param("/olivia/status/is_moving"   ) else False
        self.emergency        = rospy.get_param("/olivia/status/emergency"   ) if rospy.has_param("/olivia/status/emergency"   ) else False
        self.status_update( )
        
        # self.ISHOST_UP = False
        if self.HOST_UP and init==1:
        # self.ISHOST_UP = True
             
            self.remote_olivia            = remote_olivia_class()
            # self.remote_blaise            = remote_blaise_class()
        
            
            self.fuselage_panel1          = olivia_fuse_1_class()
            self.fuselage_panel2          = olivia_fuse_2_class()
            self.cargo_down               = olivia_cargo_bot_class()
            self.cargo_up                 = olivia_cargo_up_class()
            self.cargo_go_home            = olivia_cargo_home_class()
            self.fuselage_go_home         = olivia_fuse_home_class()
    
            
            self.emergency                = False
            
            rospy.sleep(6)
            # rospy.logwarn("starting blaise")
            # self.remote_blaise.start()
            # rospy.logwarn("starting blaise actions")
            # self.remote_blaise.start_actions()
            rospy.logwarn("starting olivia")
            self.remote_olivia.start()
   
        rospy.logwarn("Finished creating olivia class")
        
    def status_update(self, floor =  [], position = [], docked = [], ready = [] , docking_position = []):
        if (floor or floor==0):
            self.floor              = floor
        if position             :    self.position            = position
        if docked               :    self.docked              = docked
        if docking_position     :    self.docking_position    = docking_position
        if ready   :    self.ready  = ready
        rospy.set_param("/olivia/status/floor",self.floor)
        rospy.set_param("/olivia/status/position",self.position)
        rospy.set_param("/olivia/status/ready",self.ready)
        rospy.set_param("/olivia/status/empty",self.empty)
        rospy.set_param("/olivia/status/docked",self.docked)
        rospy.set_param("/olivia/status/is_moving", self.is_moving )
        rospy.set_param("/olivia/status/emergency", self.emergency )
        rospy.set_param("/olivia/status/docking_position", self.docking_position )
        
        
    def get_status( self ):
        return rospy.get_param("/olivia/status" )
    
    def cargo_assembly_up(self):
        result = True
        self.cargo_up.start()
        rospy.logwarn("cargo up started")
        rospy.sleep(5)
        while not rospy.get_param("/tasks/olivia/done") and not rospy.is_shutdown():
            rospy.logwarn("Waiting assembly")
            rospy.sleep(2)
        self.cargo_up.stop()
        return result
            
            
    def cargo_assembly_down(self):
        result = True
        self.cargo_down.start()
        rospy.sleep(5)
        while not rospy.get_param("/tasks/olivia/done") and not rospy.is_shutdown():
            rospy.logwarn("Waiting assembly")
            rospy.sleep(2)
        self.cargo_down.stop()
        return result
        
            
    def fuselage_assembly_1(self):
        result = True
        rospy.sleep(2)
        self.fuselage_panel1.start()
        rospy.sleep(5)
        while not rospy.get_param("/tasks/olivia/done") and not rospy.is_shutdown():
            rospy.logwarn("Waiting assembly in dispatcher")
            rospy.sleep(2)
        self.fuselage_assembly_1.stop()
        return result
        
        
    def fuselage_assembly_2(self):
        result = True
        self.fuselage_panel2.start()
        rospy.sleep(5)
        while not rospy.get_param("/tasks/olivia/done") and not rospy.is_shutdown():
            rospy.logwarn("Waiting assembly")
            rospy.sleep(2)
        self.fuselage_assembly_1.stop()
        return result
            
            
            