#!/bin/bash

sudo echo >/dev/null || exit 1

ps_codes=$(ps -aux | grep -i "python dispatcher.py" | grep -v "grep" | awk  '{print $2}'| tr -s '\n ', ' ' )
IFS=', ' read -r -a ps_array <<< "$ps_codes"

for i in "${ps_array[@]}"
do
   echo "$i"
   sudo kill -9 $i
done



if [[ $(ps -aux | grep -i "python dispatcher.py" | grep -v "grep") ]]; then
    echo "could not close all the process"
else
    echo "OK OK OK OK OK"
fi

rosnode cleanup 
