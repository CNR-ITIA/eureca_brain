#!/usr/bin/env python
import rospy
import tf
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose,Point,Quaternion
from pyquaternion import Quaternion as pyqt
#import quaternion  as pyqt
import numpy as np

rospy.init_node('turtle_odom_simfal', anonymous=True,disable_signals=False)  
pub_odom        = rospy.Publisher('/turtle/odom_simfal', Odometry, queue_size=1)  
 
listener = tf.TransformListener()
rate_sim = rospy.Rate(6) 

v = np.array([0., 1., 0.])

rotw   = []
transw = []

docked_obj = "none"
odom_sim = Odometry()
odom_sim.header.frame_id = "map_turtle"
x_shift = [0, 6]
y_shift = [0.3,-0.7]

while True and not rospy.is_shutdown():
    
    try:
        (transw,rotw) = listener.lookupTransform('/map_turtle','turtle/base_footprint', rospy.Time(0))
    except Exception as e:
        rospy.logwarn_throttle(5,"exc 32")
        continue
    
    floor = rospy.get_param("/wimpy/status/floor")
        
    odom_sim.header.stamp    =  rospy.Time.now()
    if floor == 1:
        try:
            odom_sim.pose.pose       =  Pose(Point(transw[1] + x_shift[floor] ,-transw[0] + y_shift[floor],floor * 1.7),Quaternion(rotw[0],rotw[1],rotw[2],rotw[3]))    
        except Exception as e:
            rospy.logwarn_throttle(1,"exc")
            continue
    else:
        try:
            pd = pyqt(rotw[0],rotw[1],rotw[3],rotw[2])
            pd.rotate(v)
            odom_sim.pose.pose       =  Pose(Point(transw[0] + y_shift[floor],transw[1] + x_shift[floor] ,floor * 1.7),Quaternion(rotw[0],rotw[1],rotw[2],rotw[3]))    
        except Exception as e:
            rospy.logwarn_throttle(1,"exc")
            continue
    pub_odom.publish(odom_sim)
    
#    if rospy.has_param("/sweepee/status/docked_obj"):
#        docked_obj = rospy.get_param("/sweepee/status/docked_obj")
    
    rate_sim.sleep()
